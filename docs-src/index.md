## Remal Gradle plugins

The main purpose of these plugins is to simplify Gradle configuration.

A simple Gradle `build.gradle` file for a Java application or library can look like this:

```groovy tab="Groovy DSL"
buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath "name.remal:gradle-plugins:@version@"
    }
}

apply plugin: 'java'
apply plugin: 'name.remal.default-plugins'

// If you want to publish Maven artifacts to Maven Central:
apply plugin: 'maven-publish'
publishing.repositories.ossrh()
```

&nbsp;

{!docs-generated/plugins-index.md!}
