The plugin applies [`name.remal.common-settings`](name.remal.common-settings.md) plugin.

&nbsp;

This plugin configures all [`AbstractCopyTask`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/AbstractCopyTask.html) to merge files with the same [`RelativePath`](https://docs.gradle.org/current/javadoc/org/gradle/api/file/RelativePath.html). Currently the plugin merges these files:

* `META-INF/services/org.codehaus.groovy.runtime.ExtensionModule` - Groovy extension modules
* `META-INF/services/*` - services for [ServiceLoader mechanism](https://docs.oracle.com/javase/8/docs/api/java/util/ServiceLoader.html)
* `module-info.class` - Jigsaw module info (useful along with [`name.remal.fat-jar`](name.remal.fat-jar.md) plugin)
