**This plugin works only if [`maven-publish`](https://docs.gradle.org/current/userguide/publishing_maven.html) plugin is applied.**

The plugin applies [`name.remal.maven-publish-settings`](name.remal.maven-publish-settings.md) and [`name.remal.environment-variables`](name.remal.environment-variables.md) plugins.

&nbsp;

This plugin adds `publishing.repositories.bintray` method. This method adds Bintray Maven repository to publish Maven artifacts to.

Publishing SNAPSHOT artifacts will be skipped, as Bintray doesn't accept SNATSHOT versions.

Usage:

```groovy tab="Groovy"
publishing.repositories.bintray {
    owner = "owner-name"
    repositoryName = "repository-name"
    packageName = "package-name"

    credentials.username = "user" // Optional. By default 'BINTRAY_USER' environment variable is used
    credentials.password = "password" // Optional. By default 'BINTRAY_API_KEY' environment variable is used
}
```

```kotlin tab="Kotlin"
import name.remal.gradle_plugins.plugins.publish.bintray.RepositoryHandlerBintrayExtension
import name.remal.gradle_plugins.dsl.extensions.*

publishing.repositories.convention[RepositoryHandlerBintrayExtension::class.java].bintray {
    owner = "owner-name"
    repositoryName = "repository-name"
    packageName = "package-name"

    credentials.username = "user" // Optional. By default 'BINTRAY_USER' environment variable is used
    credentials.password = "password" // Optional. By default 'BINTRAY_API_KEY' environment variable is used
}
```
