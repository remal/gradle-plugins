**This plugin works only if `GITLAB_CI` environment variable equals to `true`.**

The plugin applies [`name.remal.common-ci`](name.remal.common-ci.md) plugin.

Current build is treated as executed on a CI server if `GITLAB_CI` environment variable equals to `true`.

&nbsp;

The plugin sets [`ci`](name.remal.common-ci.md#nameremalgradle_pluginspluginsciciextension) extension values:

* `pipelineId` to `null`
* `buildId` to `CI_PIPELINE_ID` environment variable
* `stageName` to `CI_JOB_STAGE` environment variable
* `jobName` to `CI_JOB_NAME` environment variable

&nbsp;

## If [`name.remal.vcs-operations`](name.remal.vcs-operations.md) plugin is applied, this plugin:

* Sets `vcsOperations.overwriteCurrentBranch` to `CI_COMMIT_BRANCH` environment variable if it's available, if it's not available then to `CI_COMMIT_REF_NAME` environment variable (only if `CI_COMMIT_TAG` environment variable is empty)
* Configures remote URI to `CI_REPOSITORY_URL` environment variable
* Uses `SSH_PRIVATE_KEY_FILE` environment variable's content as remote authorization's SSH private key file path
* Uses `SSH_PRIVATE_KEY` environment variable's content as remote authorization's SSH private key
* Uses `SSH_PRIVATE_KEY_PASSWORD` environment variable's content as remote authorization's SSH private key password
* Configures `vcsOperations.commitAuthor` to use `GITLAB_USER_NAME` or `GITLAB_USER_LOGIN` environment variables as commit author name and `GITLAB_USER_EMAIL` environment variable as commit author email.
