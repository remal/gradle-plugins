This plugin adds filtering (see [Maven filtering](https://maven.apache.org/plugins/maven-resources-plugin/examples/filter.html)) functionality to all tasks of [`CopySpec`](https://docs.gradle.org/current/javadoc/org/gradle/api/file/CopySpec.html) type.

The functionality can be configured using created `filteringSettings` extension of type [`FilteringSettings`](#nameremalgradle_pluginspluginscommonfilteringsettings).

&nbsp;

### `name.remal.gradle_plugins.plugins.common.FilteringSettings`
| Property | Type | Description
| --- | :---: | --- |
| `fileExtensions` | <nobr>`MutableSet<String>`</nobr> | File extensions to apply filtering. Default value: `properties`, `xml`. |
| `filePatterns` | <nobr>`MutableSet<String>`</nobr> | ANT like patterns of resources to apply filtering. |
| `filteringCharset` | <nobr><code><a href="https://docs.oracle.com/javase/8/docs/api/java/nio/charset/Charset.html">Charset</a></code></nobr> | The charset used to read and write files when filtering. Default value: `UTF-8`. |
| `beginToken` | <nobr>`String`</nobr> | Begin token. Default value: `@`. |
| `endToken` | <nobr>`String`</nobr> | End token. Default value: `@`. |
