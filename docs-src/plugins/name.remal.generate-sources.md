## If [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) plugin is applied.

For every source-set corresponding `generateResources` task is created. Corresponding `processResources` task depends on it.

For every source-set corresponding `generateJava` task is created. Corresponding `compileJava` task depends on it.



## If [`kotlin`](https://kotlinlang.org/docs/reference/using-gradle.html) plugin is applied.

For every source-set corresponding `generateKotlin` task is created. Corresponding `compileKotlin` and `compileKotlin2Js` tasks depend on it.



## If [`groovy`](https://docs.gradle.org/current/userguide/groovy_plugin.html) plugin is applied.

For every source-set corresponding `generateGroovy` task is created. Corresponding `compileGroovy` task depends on it.
