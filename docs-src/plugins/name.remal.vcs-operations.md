This plugin allows to run different [VCS](https://en.wikipedia.org/wiki/Version_control) actions (like commit).

Currently these [VCS](https://en.wikipedia.org/wiki/Version_control) are supported:

* Git (using [JGit](https://www.eclipse.org/jgit/) project)

The plugin creates `vcsOperations` of type [`VcsOperationsExtension`](#nameremalgradle_pluginspluginsvcsvcsoperationsextension)

&nbsp;

### `name.remal.gradle_plugins.plugins.vcs.VcsOperationsExtension`
| Property | Type | Description
| --- | :---: | --- |
| `overwriteMasterBranch` | `String?` | This value is used as master branch name (if not `null`). Default value: `null`. |
| `overwriteCurrentBranch` | `String?` | This value is used as current branch name (if not `null`). Default value: `null`. |
| `commitAuthor` | <code><a href="#nameremalgradle_pluginspluginsvcscommitauthor">CommitAuthor</a></code>? | Use this info as commit author (if not `null`). Default value: `null`. |

| Method | Description
| --- | --- |
| `File getVcsRootDir()` | Returns root VCS dir for current project. |
| `String getMasterBranch()` | Returns master branch name. |
| `String? getCurrentBranch()` | Returns current branch name. Returns `null` if current branch name can't be retrieved. |
| `boolean isCommitted()` | Returns `true` if everything is committed. |
| `boolean isNotCommitted()` | Returns `true` if anything is not committed. |
| <code>Stream&lt;<a href="#nameremalgradle_pluginspluginsvcscommit">Commit</a>&gt; walkCommits()</code> | Returns a stream of commits. |
| <code><a href="#nameremalgradle_pluginspluginsvcscommit">Commit</a>? getCurrentCommit()</code> | Returns current commit if anything has been committed yet. |
| `void commitFiles(String message, Collection<File> files)` | Commits files with provided message. If `files` is empty, all modified files are modified. The commit will be pushed to a remote repository. |
| `void commitFiles(String message, File... files)` | Commits files with provided message. If `files` is empty, all modified files are modified. The commit will be pushed to a remote repository. |
| `void commit(String message, Collection<String> filePaths)` | Commits files with provided message. If `filePaths` is empty, all modified files are modified. The commit will be pushed to a remote repository. |
| `void commit(String message, String... filePaths)` | Commits files with provided message. If `filePaths` is empty, all modified files are modified. The commit will be pushed to a remote repository. |
| `void commit(String message)` | Commits modified files with provided message. The commit will be pushed to a remote repository. |
| `Set<String> getAllTagNames()` | Returns all tag names. |
| `void createTag(String commitId, String tagName, String message)` | Creates a tag that points on a commit of provided commit ID. If tag creation operation supports a message provision, provided message will be used. The tag will be pushed to a remote repository. |
| `void createTag(String commitId, String tagName)` | Creates a tag that points on a commit of provided commit ID. The tag will be pushed to a remote repository. |
| <code>void createTag(<a href="#nameremalgradle_pluginspluginsvcscommit">Commit</a> commit, String tagName, String message)</code> | Creates a tag that points on a commit of provided commit. If tag creation operation supports a message provision, provided message will be used. The tag will be pushed to a remote repository. |
| <code>void createTag(<a href="#nameremalgradle_pluginspluginsvcscommit">Commit</a> commit, String tagName)</code> | Creates a tag that points on a commit of provided commit. The tag will be pushed to a remote repository. |
| `void createTagForCurrentCommit(String tagName, String message)` | Creates a tag that points on current commit. If tag creation operation supports a message provision, provided message will be used. The tag will be pushed to a remote repository. |
| `void createTagForCurrentCommit(String tagName)` | Creates a tag that points on current commit. The tag will be pushed to a remote repository. |
| `void setUnauthorizedRemoteURI(String uri)` | Sets remote URI without authorization info. |
| `void setUnauthorizedRemoteURI(URI uri)` | Sets remote URI without authorization info. |
| `void setUsernamePasswordAuth(String username, char[] password)` | Sets user name and password as remote authorization info. |
| `void setUsernamePasswordAuth(String username, String password)` | Sets user name and password as remote authorization info. |
| `void setUsernamePasswordAuth(String username)` | Sets user name with empty password as remote authorization info. |
| `void setSSHAuth(File privateKeyFile, char[]? password)` | Sets SSH remote authorization info. `privateKeyFile` is used as private key file. `password` is used as a password (if not `null`). |
| `void setSSHAuth(File privateKeyFile, String? password)` | Sets SSH remote authorization info. `privateKeyFile` is used as private key file. `password` is used as a password (if not `null`). |
| `void setSSHAuth(File privateKeyFile)` | Sets SSH remote authorization info without password. `privateKeyFile` is used as private key file. |

### `name.remal.gradle_plugins.plugins.vcs.CommitAuthor`
| Constructor | Description
| --- | --- |
| `CommitAuthor(String name, String email)` | Construct `CommitAuthor` info with provided name and email. |

| Method | Description
| --- | --- |
| `String getName()` | Returns name. |
| `String getEmail()` | Returns email. |

### `name.remal.gradle_plugins.plugins.vcs.Commit`
| Method | Description
| --- | --- |
| `String getId()` | Returns commit ID. |
| <code><a href="https://docs.oracle.com/javase/8/docs/api/java/time/LocalDateTime.html">LocalDateTime</a> getLocalDateTime()</code> | Returns date and time of the commit. |
| `List<String> getTags()` | Returns tag names that point on the commit. |
| <code><a href="https://docs.oracle.com/javase/8/docs/api/java/util/Date.html">Date</a> getDate()</code> | Returns date and time of the commit. |
