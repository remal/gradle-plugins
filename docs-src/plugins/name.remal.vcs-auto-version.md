The plugin applies [`name.remal.vcs-operations`](name.remal.vcs-operations.md) plugin.

&nbsp;

This plugin sets the project's version based on VCS tags and commits.

The plugin operates with:

* <a id="version-tags"></a> Version tags - tags where versions are stored
* <a id="base-version-tags"></a> Base version tags - versions are calculated based on the latest base version, which is stored in base version tag
* <a id="version-increment"></a> Version increment - a number which is used to increment the latest version number by. Examples:
    * `1.1` increment by `5` equals to `1.6`
    * `1.1.1` increment by `10` equals to `1.1.11`


### Version calculation algorithm examples

<table class="gitgraph-examples">
    <thead><tr align="center">
        <th>Git log
        <th>Result
    </thead>
    <tr>
        <td>
            <script type="gitgraph" template="minimal">
            var master = gitgraph.branch("master");
            master.commit({ tag: "HEAD" });
            master.commit();
            master.commit({ tag: "ver-0.1" });
            </script>
        <td>
            Calculated version: <code>0.2</code>.
            <br>Latest version: <code>0.1</code>.
            <br>No other modifiers.
            <br><br>If <code>incrementVersionByCommitsCount = true</code>, then calculated version is <code>0.3</code>, as current commit is the second from the latest version commit.
    <tr>
        <td>
            <script type="gitgraph" template="minimal">
            var master = gitgraph.branch("master");
            master.commit({ tag: "HEAD" });
            master.commit();
            master.commit({ tag: "ver-base-0.2" });
            master.commit({ tag: "ver-0.1" });
            </script>
        <td>
            Calculated version: <code>0.2.0</code>.
            <br>Latest version: <code>0.1</code>.
            <br>Latest base version: <code>0.2</code>, which is greater then the latest version, so calculated version will be <code>&lt;base version&gt;.0</code>.
            <br><br>If <code>incrementVersionByCommitsCount = true</code>, then calculated version is <code>0.2.0</code>, as the latest base version is used.
    <tr>
        <td>
            <script type="gitgraph" template="minimal">
            var master = gitgraph.branch("master");
            master.commit({ tag: "HEAD" });
            var develop = master.branch("develop");
            develop.commit();
            develop.commit();
            develop.commit({ tag: "ver-1.0" });
            develop.commit();
            develop.merge(master, { tag: "ver-0.1" });
            //master.commit();
            </script>
        <td>
            Calculated version: <code>0.2</code>.
            <br>Latest version: <code>0.1</code>, as distance to version <code>0.1</code> is 1 commit and distance to version <code>1.0</code> is 3 commits.
            <br>Greatest version: <code>1.0</code>, <b>but</b> the latest is alwayes used.
            <br>No other modifiers.
            <br><br>If <code>incrementVersionByCommitsCount = true</code>, then calculated version is <code>0.2</code>.
</table>


### Additionally

The plugin creates `autoVcsVersion` of type [`AutoVcsVersionExtension`](#nameremalgradle_pluginspluginsvcsautovcsversionextension) to configure version calculation.

Also the plugin creates `createAutoVcsVersionTag` task, that creates a VCS tag and push it to a remote repository.

&nbsp;

### `name.remal.gradle_plugins.plugins.vcs.AutoVcsVersionExtension`
| Property | Type | Description
| --- | :---: | --- |
| `incrementVersionByCommitsCount` | `Boolean` | Is calculated version [increment](#version-increment) by commits since previous version enabled. Default value: `false`. |
| `incrementVersionBy` | `Int` | [Increment](#version-increment) calculated version additionally by this value. Default value: `0`. |
| `useLatestVersion` | `Boolean` | Use the latest version regardless how many commits there are since the version tag. Default value: `false`. |

| Method | Description
| --- | --- |
| `MutableList<String> getVersionTagPrefixes()` | Returns a list of [version tag](#version-tags) prefixes. Default value: `ver-`, `version-`. |
| `void setVersionTagPrefixes(Collection<String> versionTagPrefixes)` | Sets a list of [version tag](#version-tags) prefixes. |
| `void setVersionTagPrefixes(String... versionTagPrefixes)` | Sets a list of [version tag](#version-tags) prefixes. |
| `void versionTagPrefix(String versionTagPrefix)` | Adds [version tag](#version-tags) prefix. |
| `void versionTagPrefixes(Iterable<String> versionTagPrefixes)` | Adds [version tag](#version-tags) prefixes. |
| `void versionTagPrefixes(String... versionTagPrefixes)` | Adds [version tag](#version-tags) prefixes. |
| `MutableList<String> getBaseVersionTagPrefixes()` | Returns a list of [base version tag](#base-version-tags) prefixes. Default value: `ver-base-`, `version-base-`. |
| `void setBaseVersionTagPrefixes(Collection<String> baseVersionTagPrefixes)` | Sets a list of [base version tag](#base-version-tags) prefixes. |
| `void setBaseVersionTagPrefixes(String... baseVersionTagPrefixes)` | Sets a list of [base version tag](#base-version-tags) prefixes. |
| `void baseVersionTagPrefix(String baseVersionTagPrefix)` | Adds [base version tag](#base-version-tags) prefix. |
| `void baseVersionTagPrefixes(Iterable<String> baseVersionTagPrefixes)` | Adds [base version tag](#base-version-tags) prefixes. |
| `void baseVersionTagPrefixes(String... baseVersionTagPrefixes)` | Adds [base version tag](#base-version-tags) prefixes. |
