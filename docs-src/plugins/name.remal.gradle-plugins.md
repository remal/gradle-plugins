The plugin applies these plugins:

* [`java`](https://docs.gradle.org/current/userguide/java_plugin.html)
* [`name.remal.common-settings`](name.remal.common-settings.md)
* [`name.remal.test-source-sets`](https://github.com/remal-gradle-plugins/test-source-sets)
* [`name.remal.generate-sources`](name.remal.generate-sources.md)
* [`name.remal.cross-gradle-versions-checks`](name.remal.cross-gradle-versions-checks.md)

&nbsp;

This plugin helps to develop Gradle plugins. It:

* Adds `gradleApi` and [`embeddedKotlin`](name.remal.dependencies-extensions.md#embedded-kotlin) as `compileOnly` dependency
* Adds `gradleTestKit` as `testCompile` dependency
* Excludes these transitive dependencies:
    * `org.codehaus.groovy:*`
    * `ant:ant`
    * `org.apache.ant:ant`
    * `org.apache.ant:ant-launcher`
    * `org.slf4j:*`
    * `ch.qos.logback:*`
    * `org.apache.logging.log4j:*`
    * `log4j:*`
    * `commons-logging:*`
    * `org.springframework:spring-jcl`

Also the plugin [generates](name.remal.generate-sources.md) simple test for each plugin annotated by `name.remal.gradle_plugins.dsl.Plugin` annotation.
