**This plugin works only if [`maven-publish`](https://docs.gradle.org/current/userguide/publishing_maven.html) plugin is applied.**

&nbsp;

This plugin creates `releaseNexusRepositories` task that releases staging Nexus repositories. This task depends on all [`PublishToMavenRepository`](https://docs.gradle.org/current/javadoc/org/gradle/api/publish/maven/tasks/PublishToMavenRepository.html) tasks.

Also `release*NexusRepository` tasks are created for each staging Nexus repository.

**If release fails, the staging repository will be dropped.**
