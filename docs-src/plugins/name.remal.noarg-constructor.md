**This plugin works only if [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) plugin is applied.**

The plugin applies [`name.remal.classes-processing`](name.remal.classes-processing.md) plugin.

&nbsp;

This plugin processes all compiled classes. It creates no-arg protected synthetic constructor if it's possible:

* The class extends `java.lang.Object`
* Parent class has public or protected no-arg constructor

Created no-arg synthetic constructor does nothing and basically is needed for deserialization.
