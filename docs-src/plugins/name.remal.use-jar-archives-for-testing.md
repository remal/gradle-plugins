**This plugin works only if [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) plugin is applied.**

The plugin applies [`name.remal.test-source-sets`](https://github.com/remal-gradle-plugins/test-source-sets) plugin.

&nbsp;

By default [`Test`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/Test.html) tasks use `main` source-set output as a classpath. This plugin configures the tasks to use a jar archive created by `jar` task as a classpath.
