**This plugin works only if [`java`](https://docs.gradle.org/current/userguide/java_plugin.html) plugin is applied.**

The plugin applies [`name.remal.test-source-sets`](https://github.com/remal-gradle-plugins/test-source-sets) plugin.

&nbsp;

* It disables all [`AbstractTestTask`](https://docs.gradle.org/current/javadoc/org/gradle/api/tasks/testing/AbstractTestTask.html) tasks.
* It disables all [`AssertJGenerate`](name.remal.assertj-generator.md#assertj-generate) tasks.
* It disables `check` task.
* It disables all tasks that compiles/processes [`testSourceSets`](https://github.com/remal-gradle-plugins/test-source-sets).
