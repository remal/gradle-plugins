package name.remal.gradle_plugins.plugins.dependencies

import name.remal.findAll
import name.remal.gradle_plugins.api.BuildTimeConstants.getClassSimpleName
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.createWithUniqueName
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.notation
import name.remal.gradle_plugins.dsl.utils.matches
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import name.remal.gradle_plugins.testing.dsl.TEST_MAVEN_REPO_DEFAULT_GROUP_ID
import name.remal.gradle_plugins.testing.dsl.TEST_MAVEN_REPO_DEFAULT_VERSION
import name.remal.gradle_plugins.testing.dsl.testMavenRepository
import org.gradle.api.artifacts.ResolvedDependency
import org.gradle.internal.resolve.ModuleVersionNotFoundException
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Test

class DependencyVersionsPluginTest : BaseProjectTest() {

    lateinit var dependencyVersions: DependencyVersionsExtension

    @Before
    fun before() {
        project.applyPlugin(DependencyVersionsPlugin::class.java)
        dependencyVersions = project[DependencyVersionsExtension::class.java]

        project.testMavenRepository {
            val dependency = component(artifactId = "dependency", version = "1-alpha") { jar() }
            component(artifactId = "root", version = "1") { pom { dependency(dependency) }; jar() }
            component(artifactId = "root", version = "2-beta") { pom { dependency(dependency) }; jar() }
        }

        project.testMavenRepository {
            component("bouncycastle", "bcprov-jdk16") { jar() }
            component("org.bouncycastle", "bcprov-jdk16") { jar() }
        }
    }

    @Test
    fun resolveLatestVersion() {
        assertEquals("1", dependencyVersions.resolveLatestVersion("$TEST_MAVEN_REPO_DEFAULT_GROUP_ID:root"))

        dependencyVersions.allowAllVersionsFor.add("$TEST_MAVEN_REPO_DEFAULT_GROUP_ID:root")
        assertEquals("2-beta", dependencyVersions.resolveLatestVersion("$TEST_MAVEN_REPO_DEFAULT_GROUP_ID:root"))

        dependencyVersions.allowAllVersionsFor.clear()
        assertEquals("1", dependencyVersions.resolveLatestVersion("$TEST_MAVEN_REPO_DEFAULT_GROUP_ID:root"))

        dependencyVersions.invalidVersionTokens.clear()
        assertEquals("2-beta", dependencyVersions.resolveLatestVersion("$TEST_MAVEN_REPO_DEFAULT_GROUP_ID:root"))

        dependencyVersions.invalidVersionTokens.add("beta")
        assertEquals("1", dependencyVersions.resolveLatestVersion("$TEST_MAVEN_REPO_DEFAULT_GROUP_ID:root"))
    }

    @Test
    fun resolveAllVersions() {
        assertEquals(listOf("1"), dependencyVersions.resolveAllVersions("$TEST_MAVEN_REPO_DEFAULT_GROUP_ID:root"))

        dependencyVersions.allowAllVersionsFor.add("$TEST_MAVEN_REPO_DEFAULT_GROUP_ID:root")
        assertEquals(listOf("2-beta", "1"), dependencyVersions.resolveAllVersions("$TEST_MAVEN_REPO_DEFAULT_GROUP_ID:root"))

        dependencyVersions.allowAllVersionsFor.clear()
        assertEquals(listOf("1"), dependencyVersions.resolveAllVersions("$TEST_MAVEN_REPO_DEFAULT_GROUP_ID:root"))

        dependencyVersions.invalidVersionTokens.clear()
        assertEquals(listOf("2-beta", "1"), dependencyVersions.resolveAllVersions("$TEST_MAVEN_REPO_DEFAULT_GROUP_ID:root"))

        dependencyVersions.invalidVersionTokens.add("beta")
        assertEquals(listOf("1"), dependencyVersions.resolveAllVersions("$TEST_MAVEN_REPO_DEFAULT_GROUP_ID:root"))
    }

    @Test
    fun rejectDynamicFirstLevelDependencies() {
        val configuration = project.configurations.createWithUniqueName {
            it.dependencies.add(project.dependencies.create("$TEST_MAVEN_REPO_DEFAULT_GROUP_ID:root:+"))
        }

        val resolvedNotations = configuration.resolvedConfiguration.lenientConfiguration.allModuleDependencies.asSequence()
            .map(ResolvedDependency::notation)
            .sorted()
            .toList()
        assertEquals(2, resolvedNotations.size)
        assertTrue(resolvedNotations[0].matches("$TEST_MAVEN_REPO_DEFAULT_GROUP_ID:dependency:1-alpha"))
        assertTrue(resolvedNotations[1].matches("$TEST_MAVEN_REPO_DEFAULT_GROUP_ID:root:1"))
    }

    @Test
    fun rejectedVersions() {
        try {
            val rejectedConfiguration = project.configurations.createWithUniqueName {
                it.dependencies.add(project.dependencies.create("bouncycastle:bcprov-jdk16:$TEST_MAVEN_REPO_DEFAULT_VERSION"))
            }
            rejectedConfiguration.resolvedConfiguration.rethrowFailure()
            fail()
        } catch (e: Throwable) {
            val isOK = e.findAll(Throwable::class.java).any {
                it.javaClass.simpleName == getClassSimpleName(ModuleVersionNotFoundException::class.java)
            }
            if (isOK) {
                // OK
            } else {
                throw e
            }
        }

        val normalConfiguration = project.configurations.createWithUniqueName {
            it.dependencies.add(project.dependencies.create("org.bouncycastle:bcprov-jdk16:$TEST_MAVEN_REPO_DEFAULT_VERSION"))
        }
        assertEquals(1, normalConfiguration.files.size)
    }

}
