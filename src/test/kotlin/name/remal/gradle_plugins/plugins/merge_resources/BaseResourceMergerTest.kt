package name.remal.gradle_plugins.plugins.merge_resources

import name.remal.gradle_plugins.dsl.utils.matches
import org.gradle.api.file.RelativePath
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import java.io.File

class BaseResourceMergerTest {

    @Test
    fun getPatternMatcher() {
        val resourceMerger = object : BaseResourceMerger("dir/**/*.class") {
            override fun mergeFiles(relativePath: RelativePath, files: List<File>, mergedFilesDir: File) = TODO()
        }
        val patternMatcher = resourceMerger.getPatternMatcher(true)
        assertTrue(patternMatcher.matches(RelativePath.parse(true, "dir/TestClass.class")))
        assertTrue(patternMatcher.matches(RelativePath.parse(true, "dir/sub-dir/TestClass.class")))
        assertFalse(patternMatcher.matches(RelativePath.parse(true, "another-dir/TestClass.class")))
        assertFalse(patternMatcher.matches(RelativePath.parse(true, "dir/TestClass")))
        assertFalse(patternMatcher.matches(RelativePath.parse(true, "dir/TestClass.CLASS")))
    }

    @Test
    fun getPatternMatcherCaseInsensitive() {
        val resourceMerger = object : BaseResourceMerger("dir/**/*.class") {
            override fun mergeFiles(relativePath: RelativePath, files: List<File>, mergedFilesDir: File) = TODO()
        }
        val patternMatcher = resourceMerger.getPatternMatcher(false)
        assertTrue(patternMatcher.matches(RelativePath.parse(true, "dir/TestClass.class")))
        assertTrue(patternMatcher.matches(RelativePath.parse(true, "dir/sub-dir/TestClass.class")))
        assertFalse(patternMatcher.matches(RelativePath.parse(true, "another-dir/TestClass.class")))
        assertFalse(patternMatcher.matches(RelativePath.parse(true, "dir/TestClass")))
        assertTrue(patternMatcher.matches(RelativePath.parse(true, "dir/TestClass.CLASS")))
    }

}
