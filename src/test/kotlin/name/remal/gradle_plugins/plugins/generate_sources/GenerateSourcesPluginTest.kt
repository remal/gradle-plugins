package name.remal.gradle_plugins.plugins.generate_sources

import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.plugins.generate_sources.groovy.GenerateGroovy
import name.remal.gradle_plugins.plugins.generate_sources.java.GenerateJava
import name.remal.gradle_plugins.plugins.generate_sources.kotlin.GenerateKotlin
import name.remal.gradle_plugins.plugins.generate_sources.resources.GenerateResources
import name.remal.gradle_plugins.plugins.groovy.GroovyPluginId
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.plugins.kotlin.KotlinJvmPluginId
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Test

class GenerateSourcesPluginTest : BaseProjectTest() {

    @Test
    fun tasksAreCreated() {
        project.applyPlugin(GenerateSourcesPlugin::class.java)

        assertNull(project.tasks.withType(GenerateResources::class.java).findByName("generateResources"))
        assertNull(project.tasks.withType(GenerateResources::class.java).findByName("generateTestResources"))

        project.applyPlugin(JavaPluginId)
        assertNotNull(project.tasks.withType(GenerateResources::class.java).findByName("generateResources"))
        assertNotNull(project.tasks.withType(GenerateResources::class.java).findByName("generateTestResources"))
        assertNotNull(project.tasks.withType(GenerateJava::class.java).findByName("generateJava"))
        assertNotNull(project.tasks.withType(GenerateJava::class.java).findByName("generateTestJava"))

        project.applyPlugin(KotlinJvmPluginId)
        assertNotNull(project.tasks.withType(GenerateKotlin::class.java).findByName("generateKotlin"))
        assertNotNull(project.tasks.withType(GenerateKotlin::class.java).findByName("generateTestKotlin"))

        project.applyPlugin(GroovyPluginId)
        assertNotNull(project.tasks.withType(GenerateGroovy::class.java).findByName("generateGroovy"))
        assertNotNull(project.tasks.withType(GenerateGroovy::class.java).findByName("generateTestGroovy"))
    }

}
