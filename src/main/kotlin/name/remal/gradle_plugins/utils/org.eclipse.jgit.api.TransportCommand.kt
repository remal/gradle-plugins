package name.remal.gradle_plugins.utils

import com.jcraft.jsch.Session
import name.remal.gradle_plugins.dsl.DEFAULT_IO_TIMEOUT
import org.eclipse.jgit.api.TransportCommand
import org.eclipse.jgit.transport.JschConfigSessionFactory
import org.eclipse.jgit.transport.OpenSshConfig
import org.eclipse.jgit.transport.SshSessionFactory
import org.eclipse.jgit.transport.SshTransport
import java.time.Duration

fun TransportCommand<*, *>.setTimeout(duration: Duration) {
    setTimeout(duration.seconds.toInt())
}

fun TransportCommand<*, *>.setSshSessionFactory(sshSessionFactory: SshSessionFactory?) {
    setTransportConfigCallback { transport ->
        if (transport is SshTransport) {
            transport.sshSessionFactory = sshSessionFactory ?: DefaultSshSessionFactory
        }
    }
}

private object DefaultSshSessionFactory : JschConfigSessionFactory() {
    override fun configure(host: OpenSshConfig.Host, session: Session) {
        session.setConfig("StrictHostKeyChecking", "no")
        session.setConfig("ConnectTimeout", DEFAULT_IO_TIMEOUT.seconds.toString())
    }
}
