package name.remal.gradle_plugins.plugins.vcs

import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.logDebug
import name.remal.gradle_plugins.dsl.extensions.requirePlugin
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction

@BuildTask
class FailIfProjectVersionTagExists : DefaultTask() {

    private var _tagNamePrefix: String? = null

    @get:Input
    @get:Optional
    var tagNamePrefix: String
        get() = _tagNamePrefix ?: project.extensions.findByType(AutoVcsVersionExtension::class.java)?.versionTagPrefixes?.firstOrNull() ?: "version-"
        set(value) {
            _tagNamePrefix = value
        }


    @get:Input
    @get:Optional
    val tagName: String?
        get() = tagNamePrefix + project.version


    init {
        requirePlugin(VcsOperationsPlugin::class.java)
    }


    @TaskAction
    protected fun checkTag() {
        val tagName = tagName
        logDebug("Checking if '{}' tag exists", tagName)
        val vcsOperations = project[VcsOperationsExtension::class.java]
        if (tagName in vcsOperations.getAllTagNames()) {
            throw VcsTagExistsException("Tag '$tagName' already exists")
        }
        didWork = true
    }

}
