package name.remal.gradle_plugins.plugins.vcs

import name.remal.Ordered
import java.io.File

interface VcsOperationsFactory : Ordered<VcsOperationsFactory> {
    fun get(dir: File): VcsOperations?
}
