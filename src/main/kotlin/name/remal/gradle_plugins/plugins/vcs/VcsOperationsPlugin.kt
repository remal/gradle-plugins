package name.remal.gradle_plugins.plugins.vcs

import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.CreateExtensionsPluginAction
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.tasks.TaskContainer

@Plugin(
    id = "name.remal.vcs-operations",
    description = "Plugin that provides 'vcsOperations' extension.",
    tags = ["vcs"]
)
class VcsOperationsPlugin : BaseReflectiveProjectPlugin() {

    @CreateExtensionsPluginAction
    fun ExtensionContainer.`Create 'vcsOperations' extension`(project: Project): VcsOperationsExtension {
        return create("vcsOperations", VcsOperationsExtension::class.java, project)
    }

    @PluginAction
    fun TaskContainer.`Create 'failIfProjectVersionTagExists' task`() {
        create("failIfProjectVersionTagExists", FailIfProjectVersionTagExists::class.java)
    }

}
