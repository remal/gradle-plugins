package name.remal.gradle_plugins.plugins.vcs

import name.remal.default
import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.dsl.GradleEnumVersion.GRADLE_VERSION_4_6
import name.remal.gradle_plugins.dsl.extensions.compareTo
import name.remal.gradle_plugins.dsl.extensions.isPluginApplied
import name.remal.gradle_plugins.dsl.extensions.logLifecycle
import name.remal.gradle_plugins.dsl.extensions.logWarn
import name.remal.gradle_plugins.dsl.extensions.sourceSets
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.sha256
import org.gradle.api.artifacts.component.ModuleComponentIdentifier
import org.gradle.api.artifacts.component.ProjectComponentIdentifier
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.SourceSet.MAIN_SOURCE_SET_NAME
import org.gradle.util.GradleVersion

@BuildTask
class CreateDependenciesHashTag : BaseCreateTagTask() {

    @get:Input
    @get:Optional
    var tagNamePrefix: String = "dependencies-"

    private val dependenciesHash: String by lazy {
        val deps = project.rootProject.allprojects.asSequence()
            .filter { it.isPluginApplied(JavaPluginId) }
            .flatMap { project ->
                val mainSourceSet = project.sourceSets.findByName(MAIN_SOURCE_SET_NAME) ?: return@flatMap emptySequence<String>()
                val configurationNames = mutableListOf<String>()
                configurationNames.add(mainSourceSet.compileClasspathConfigurationName)
                if (GradleVersion.current() >= GRADLE_VERSION_4_6) {
                    configurationNames.add(mainSourceSet.annotationProcessorConfigurationName)
                }
                return@flatMap configurationNames.asSequence()
                    .plus(sequenceOf("dependenciesToHash"))
                    .filterNotNull()
                    .mapNotNull(project.configurations::findByName)
                    .flatMap { it.resolvedConfiguration.resolvedArtifacts.asSequence() }
                    .filter { it.id.componentIdentifier !is ProjectComponentIdentifier }
                    .filter { it.id.componentIdentifier is ModuleComponentIdentifier }
                    .map {
                        val id = it.id.componentIdentifier
                        id as ModuleComponentIdentifier
                        return@map arrayOf(
                            id.group,
                            id.module,
                            id.version,
                            it.classifier,
                            it.type
                        ).joinToString(":", transform = { it.default() })
                    }
            }
            .toSortedSet()

        if (deps.isNotEmpty()) {
            logLifecycle("Dependencies to hash:")
            deps.forEach { logLifecycle("  {}", it) }
        } else {
            logWarn("No dependencies to hash have been found")
        }

        return@lazy sha256(deps.joinToString("\n"))
    }

    override val tagName: String? get() = tagNamePrefix + dependenciesHash

}
