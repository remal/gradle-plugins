package name.remal.gradle_plugins.plugins.testing

import name.remal.Services.loadServicesList
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.PluginActionsGroup
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.doSetup
import name.remal.gradle_plugins.dsl.extensions.isTestFrameworkSet
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.testing.AbstractTestTask
import org.gradle.api.tasks.testing.Test
import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.gradle.api.tasks.testing.logging.TestLogEvent
import org.gradle.api.tasks.testing.logging.TestLogEvent.FAILED
import org.gradle.api.tasks.testing.logging.TestLogEvent.STANDARD_ERROR
import org.gradle.api.tasks.testing.logging.TestStackTraceFilter

@Plugin(
    id = "name.remal.test-settings",
    description = "Plugin that configures test tasks.",
    tags = ["test"]
)
class TestSettingsPlugin : BaseReflectiveProjectPlugin() {

    companion object {
        private val testFrameworkConfigurerDetectors: List<TestFrameworkConfigurerDetector> by lazy { loadServicesList(TestFrameworkConfigurerDetector::class.java) }
    }

    @PluginActionsGroup(order = 1)
    inner class `For all AbstractTestTask tasks` {

        @PluginAction
        fun TaskContainer.`Turn ON all reports`() {
            all(AbstractTestTask::class.java) { task ->
                task.reports.all {
                    it.required.set(true)
                }
            }
        }

        @PluginAction
        fun TaskContainer.`Show exceptions in logs`() {
            all(AbstractTestTask::class.java) {
                it.testLogging {
                    it.showExceptions = true
                }
            }
        }

        @PluginAction
        fun TaskContainer.`Show causes in logs`() {
            all(AbstractTestTask::class.java) {
                it.testLogging {
                    it.showCauses = true
                }
            }
        }

        @PluginAction
        fun TaskContainer.`Show stacktraces in logs`() {
            all(AbstractTestTask::class.java) {
                it.testLogging {
                    it.showStackTraces = true
                }
            }
        }

        @PluginAction
        fun TaskContainer.`Use FULL exception format`() {
            all(AbstractTestTask::class.java) {
                it.testLogging {
                    it.exceptionFormat = TestExceptionFormat.FULL
                }
            }
        }

        @PluginAction
        fun TaskContainer.`Use only GROOVY stacktrace filter`() {
            all(AbstractTestTask::class.java) {
                it.testLogging {
                    it.stackTraceFilters = setOf(TestStackTraceFilter.GROOVY)
                }
            }
        }

        @PluginAction(order = 1)
        fun TaskContainer.`Log only FAILED and STANDARD_ERROR events by default`() {
            all(AbstractTestTask::class.java) {
                it.testLogging {
                    it.events(FAILED, STANDARD_ERROR)
                }
            }
        }

        @PluginAction(order = 2)
        fun TaskContainer.`Log all events for INFO log level`() {
            all(AbstractTestTask::class.java) {
                it.testLogging {
                    it.info.events(*TestLogEvent.values())
                }
            }
        }

    }

    @PluginActionsGroup(order = 2)
    inner class `For all Test tasks` {

        @PluginAction
        fun TaskContainer.`Enable assertions`() {
            all(Test::class.java) {
                it.enableAssertions = true
            }
        }

        @PluginAction("Set 'junit.jupiter.extensions.autodetecpation.enabled' system property to 'true'")
        fun TaskContainer.enableJupiterExtensionsAutodetection() {
            all(Test::class.java) {
                it.systemProperty("junit.jupiter.extensions.autodetection.enabled", "true")
            }
        }

        @PluginAction(order = Int.MAX_VALUE)
        fun TaskContainer.`Setup test framework based on classpath`() {
            all(Test::class.java) {
                it.doSetup { task ->
                    if (task.isTestFrameworkSet) return@doSetup
                    if (task.candidateClassFiles.isEmpty) return@doSetup

                    val classpath = task.classpath
                    for (detector in testFrameworkConfigurerDetectors) {
                        val configurer = detector.detect(classpath) ?: continue
                        configurer.configure(task)
                        break
                    }
                }
            }
        }

    }

}
