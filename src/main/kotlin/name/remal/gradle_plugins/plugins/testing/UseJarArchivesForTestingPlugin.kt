package name.remal.gradle_plugins.plugins.testing

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.archivePathCompatible
import name.remal.gradle_plugins.dsl.extensions.doSetup
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.test_source_sets.TestSourceSetContainer
import name.remal.gradle_plugins.test_source_sets.TestSourceSetsPlugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.plugins.JavaPlugin.JAR_TASK_NAME
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.testing.Test
import org.gradle.jvm.tasks.Jar

@Plugin(
    id = "name.remal.use-jar-archives-for-testing",
    description = "Plugin that configurers using Jar archives for tests's classpath instead of main source-set output.",
    tags = ["test"]
)
@WithPlugins(JavaPluginId::class)
@ApplyPluginClasses(TestSourceSetsPlugin::class)
class UseJarArchivesForTestingPlugin : BaseReflectiveProjectPlugin() {

    @PluginAction
    fun TestSourceSetContainer.setup(tasks: TaskContainer, project: Project) {
        all { testSourceSet ->
            tasks.all(Test::class.java, testSourceSet.testTaskName) {
                val jarTask: Jar by lazy { tasks[Jar::class.java, JAR_TASK_NAME] }
                it.dependsOn(project.provider<Task> { jarTask })

                it.doSetup { test ->
                    val outputFiles = testSourceSet.output.toList()
                    if (outputFiles.isEmpty()) return@doSetup
                    val classpath = test.classpath.toMutableList()
                    if (outputFiles.all { it in classpath }) {
                        val pos = outputFiles.asSequence()
                            .map(classpath::indexOf)
                            .minOrNull()!!
                        classpath.removeAll(outputFiles)
                        classpath.add(pos, jarTask.archivePathCompatible)
                        test.classpath = project.files(classpath)
                    }
                }
            }
        }
    }

}
