package name.remal.gradle_plugins.plugins.testing

import name.remal.Ordered
import org.gradle.api.file.FileCollection

interface TestFrameworkConfigurerDetector : Ordered<TestFrameworkConfigurerDetector> {

    fun detect(classpath: FileCollection): TestFrameworkConfigurer?

}


