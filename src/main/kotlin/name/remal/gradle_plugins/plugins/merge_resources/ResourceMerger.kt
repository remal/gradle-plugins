package name.remal.gradle_plugins.plugins.merge_resources

import name.remal.Ordered
import name.remal.gradle_plugins.dsl.utils.PathMatcher
import org.gradle.api.file.RelativePath
import java.io.File

interface ResourceMerger : Ordered<ResourceMerger> {

    val pattern: String

    fun getPatternMatcher(isCaseSensitive: Boolean = true): PathMatcher

    fun mergeFiles(relativePath: RelativePath, files: List<File>, mergedFilesDir: File)

}
