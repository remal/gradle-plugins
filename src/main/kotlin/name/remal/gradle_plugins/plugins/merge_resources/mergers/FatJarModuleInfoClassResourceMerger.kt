package name.remal.gradle_plugins.plugins.merge_resources.mergers

import com.google.common.collect.MultimapBuilder
import name.remal.ASM_API
import name.remal.accept
import name.remal.createParentDirectories
import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.dsl.extensions.isPluginApplied
import name.remal.gradle_plugins.dsl.extensions.isPluginAppliedAndNotDisabled
import name.remal.gradle_plugins.dsl.extensions.java
import name.remal.gradle_plugins.plugins.fatjar.FatJarPlugin
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.gradle_plugins.plugins.merge_resources.BaseResourceMerger
import name.remal.gradle_plugins.plugins.merge_resources.ResourceMerger
import name.remal.gradle_plugins.plugins.merge_resources.ResourceMergerFactory
import name.remal.subList
import org.gradle.api.Task
import org.gradle.api.file.RelativePath
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Opcodes.ACC_OPEN
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.ModuleNode
import org.objectweb.asm.tree.ModuleProvideNode
import java.io.File

class FatJarModuleInfoClassResourceMerger : BaseResourceMerger("module-info.class") {

    override fun mergeFiles(relativePath: RelativePath, files: List<File>, mergedFilesDir: File) {
        val baseFile = files.first()
        val baseClassNode = ClassNode().also { ClassReader(baseFile.readBytes()).accept(it) }
        val baseModuleNode: ModuleNode = baseClassNode.module

        val services = MultimapBuilder.linkedHashKeys().linkedHashSetValues().build<String, String>()

        baseModuleNode.packages = baseModuleNode.packages?.toMutableList() ?: mutableListOf()
        baseModuleNode.requires = baseModuleNode.requires?.toMutableList() ?: mutableListOf()
        baseModuleNode.exports = baseModuleNode.exports?.toMutableList() ?: mutableListOf()
        baseModuleNode.opens = baseModuleNode.opens?.toMutableList() ?: mutableListOf()
        baseModuleNode.uses = baseModuleNode.uses?.toMutableList() ?: mutableListOf()
        baseModuleNode.provides?.forEach { provide ->
            services.putAll(provide.service, provide.providers)
        }

        files.subList(1).forEach { file ->
            val moduleNode: ModuleNode = ClassNode().also { ClassReader(file.readBytes()).accept(it) }.module
            if (0 != (moduleNode.access and ACC_OPEN)) baseModuleNode.access = baseModuleNode.access and ACC_OPEN
            moduleNode.packages?.let { baseModuleNode.packages.addAll(it) }
            moduleNode.requires?.let { baseModuleNode.requires.addAll(it) }
            moduleNode.exports?.let { baseModuleNode.exports.addAll(it) }
            moduleNode.opens?.let { baseModuleNode.opens.addAll(it) }
            moduleNode.uses?.let { baseModuleNode.uses.addAll(it) }
            moduleNode.provides?.forEach { provide ->
                services.putAll(provide.service, provide.providers)
            }
        }

        baseModuleNode.packages = baseModuleNode.packages.distinct()
        baseModuleNode.requires = baseModuleNode.requires.distinctBy { listOf(it.module, it.access, it.version) }
        baseModuleNode.exports = baseModuleNode.exports.distinctBy { listOf(it.packaze, it.access, it.modules) }
        baseModuleNode.opens = baseModuleNode.opens.distinctBy { listOf(it.packaze, it.access, it.modules) }
        baseModuleNode.uses = baseModuleNode.uses.distinct()
        baseModuleNode.provides = services.asMap().map { (service, providers) -> ModuleProvideNode(service, providers.toMutableList()) }

        val classWriter = ClassWriter(ASM_API)
        baseClassNode.accept(classWriter)
        val targetFile = relativePath.getFile(mergedFilesDir)
        targetFile.createParentDirectories().writeBytes(classWriter.toByteArray())
    }

}

@AutoService
class ModuleInfoClassResourceMergerFactory : ResourceMergerFactory {
    override fun createResourceMerger(task: Task): List<ResourceMerger> {
        if (!task.project.isPluginApplied(JavaPluginId)) return emptyList()
        if (!task.project.isPluginAppliedAndNotDisabled(FatJarPlugin::class.java)) return emptyList()
        if (!task.project.java.sourceCompatibility.isJava9Compatible) return emptyList()
        return listOf(FatJarModuleInfoClassResourceMerger())
    }
}
