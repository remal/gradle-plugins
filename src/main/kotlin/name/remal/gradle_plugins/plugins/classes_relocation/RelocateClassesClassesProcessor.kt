package name.remal.gradle_plugins.plugins.classes_relocation

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings
import name.remal.ASM_API
import name.remal.CLASS_FILE_NAME_SUFFIX
import name.remal.SERVICE_FILE_BASE_PATH
import name.remal.accept
import name.remal.concurrentSetOf
import name.remal.dequeOf
import name.remal.emptyStream
import name.remal.get
import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.api.BuildTimeConstants.getClassDescriptor
import name.remal.gradle_plugins.api.RelocateClasses
import name.remal.gradle_plugins.api.RelocatePackages
import name.remal.gradle_plugins.api.classes_processing.BytecodeModifier
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor.RELOCATION_STAGE
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessorsGradleTaskFactory
import name.remal.gradle_plugins.api.classes_processing.ProcessContext
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.artifact.ArtifactEntryNotFoundException
import name.remal.gradle_plugins.dsl.artifact.CachedArtifactsCollection
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.getOrNull
import name.remal.gradle_plugins.dsl.extensions.isCompilingSourceSet
import name.remal.gradle_plugins.dsl.extensions.isPluginAppliedAndNotDisabled
import name.remal.gradle_plugins.dsl.extensions.toHasEntries
import name.remal.gradle_plugins.dsl.internal.Generated
import name.remal.gradle_plugins.dsl.internal.RelocatedClass
import name.remal.gradle_plugins.dsl.utils.ClassInternalName
import name.remal.gradle_plugins.dsl.utils.ClassName
import name.remal.gradle_plugins.dsl.utils.classInternalNameToClassName
import name.remal.gradle_plugins.dsl.utils.classNameToClassInternalName
import name.remal.gradle_plugins.dsl.utils.getGradleLogger
import name.remal.nullIf
import name.remal.plus
import name.remal.toSet
import org.gradle.api.artifacts.Configuration
import org.gradle.api.tasks.SourceSet.MAIN_SOURCE_SET_NAME
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.compile.AbstractCompile
import org.jetbrains.annotations.ApiStatus.Internal
import org.objectweb.asm.AnnotationVisitor
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassReader.SKIP_CODE
import org.objectweb.asm.ClassReader.SKIP_DEBUG
import org.objectweb.asm.ClassReader.SKIP_FRAMES
import org.objectweb.asm.ClassVisitor
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.commons.ClassRemapper
import org.objectweb.asm.commons.Method
import org.objectweb.asm.commons.MethodRemapper
import org.objectweb.asm.commons.Remapper
import org.objectweb.asm.tree.AnnotationNode
import java.util.stream.Stream
import kotlin.text.Charsets.UTF_8

private val relocateClassesDesc = getClassDescriptor(RelocateClasses::class.java)
private val relocatePackagesDesc = getClassDescriptor(RelocatePackages::class.java)

class RelocateClassesClassesProcessor(
    private val classesRelocation: ClassesRelocationExtension,
    private val classpathArtifacts: CachedArtifactsCollection,
    private val relocateClassesConf: Configuration,
    private val excludeFromClassesRelocationConf: Configuration,
    private val excludeFromForcedClassesRelocationConf: Configuration,
    private val isRelocateClassesAnnotationInClasspath: Boolean
) : ClassesProcessor {

    companion object {
        private val logger = getGradleLogger(RelocateClassesClassesProcessor::class.java)

        private val internalDesc = getClassDescriptor(Internal::class.java)
        private val generatedDesc = getClassDescriptor(Generated::class.java)
        private val relocatedClassDesc = getClassDescriptor(RelocatedClass::class.java)
        private val suppressFBWarningsDesc = getClassDescriptor(SuppressFBWarnings::class.java)
        private val additionalAnnotationDescrs = setOf(
            internalDesc,
            generatedDesc,
            relocatedClassDesc,
            suppressFBWarningsDesc
        )

        private val outerClassInternalNameRegex = Regex("\\\$[^\$/]*\$")
    }

    private val relocatedClassNamePrefix = classesRelocation.relocatedClassesPackageName + '.'
    private val relocatedClassInternalNamePrefix = classNameToClassInternalName(relocatedClassNamePrefix)


    private val classInternalNamesExcludedFromForcedRelocation: Set<ClassInternalName> by lazy {
        return@lazy CachedArtifactsCollection(excludeFromForcedClassesRelocationConf)
            .classNames.asSequence()
            .map(::classNameToClassInternalName)
            .toSet()
    }

    private val possibleClassInternalNamesForForcedRelocation: Set<ClassInternalName> by lazy {
        return@lazy classpathArtifacts.classNames.asSequence()
            .map(::classNameToClassInternalName)
            .filter { it !in classInternalNamesExcludedFromForcedRelocation }
            .toSet()
    }

    private val resourcesExcludedFromForcedRelocation: Set<String> by lazy {
        return@lazy CachedArtifactsCollection(excludeFromForcedClassesRelocationConf)
            .entryNames
    }

    private val possibleResourcesForForcedRelocation: Set<String> by lazy {
        return@lazy classpathArtifacts.entryNames.asSequence()
            .filter { it !in resourcesExcludedFromForcedRelocation }
            .toSet()
    }


    private val classInternalNamesExcludedFromRelocation: Set<ClassInternalName> by lazy {
        return@lazy CachedArtifactsCollection(excludeFromClassesRelocationConf)
            .classNames.asSequence()
            .map(::classNameToClassInternalName)
            .toSet()
    }

    private val possibleClassInternalNamesForRelocation: Set<ClassInternalName> by lazy {
        return@lazy CachedArtifactsCollection(relocateClassesConf)
            .classNames.asSequence()
            .map(::classNameToClassInternalName)
            .filter { it !in classInternalNamesExcludedFromRelocation }
            .toSet()
    }

    private val resourcesExcludedFromRelocation: Set<String> by lazy {
        return@lazy CachedArtifactsCollection(excludeFromClassesRelocationConf)
            .entryNames
    }

    private val possibleResourcesForRelocation: Set<String> by lazy {
        return@lazy CachedArtifactsCollection(relocateClassesConf)
            .entryNames.asSequence()
            .filter { it !in resourcesExcludedFromRelocation }
            .toSet()
    }


    private val alreadyRelocatedClassInternalNames = concurrentSetOf<ClassInternalName>()


    @Suppress("kotlin:S3776")
    override fun process(bytecode: ByteArray, bytecodeModifier: BytecodeModifier, className: ClassName, resourceName: String, context: ProcessContext) {
        var isChanged = false

        val classInternalNamesForForcedRelocation = mutableSetOf<ClassInternalName>()
        val classInternalNamesForRelocation = mutableSetOf<ClassInternalName>()

        val relocationInfo = readRelocationInfo(bytecode, context)
        val rootRemapper = object : RelocationRemapper() {
            override fun map(classInternalName: ClassInternalName): ClassInternalName? {
                if (classInternalName in relocationInfo.classInternalNamesForRelocation) {
                    isChanged = true
                    if (classInternalNamesForForcedRelocation.add(classInternalName)) {
                        logger.info("Relocation for {}: {} (forced)", className, classInternalName)
                    }
                    return relocatedClassInternalNamePrefix + classInternalName
                }
                if (classInternalName in possibleClassInternalNamesForRelocation) {
                    isChanged = true
                    if (classInternalNamesForRelocation.add(classInternalName)) {
                        logger.info("Relocation for {}: {}", className, classInternalName)
                    }
                    return relocatedClassInternalNamePrefix + classInternalName
                }
                return null
            }
        }

        val classReader = ClassReader(bytecode)
        val classWriter = ClassWriter(classReader, 0)
        val classRemapper = object : ClassRemapper(classWriter, rootRemapper) {
            override fun visitMethod(access: Int, name: ClassInternalName, desc: String, signature: String?, exceptions: Array<ClassInternalName>?): MethodVisitor? {
                val method = Method(name, desc)
                val methodRemapper = if (method in relocationInfo.methodsClassInternalNamesForRelocation) {
                    object : RelocationRemapper() {
                        private val methodClassInternalNamesForRelocation = relocationInfo.methodsClassInternalNamesForRelocation[method]!!
                        override fun map(classInternalName: ClassInternalName): ClassInternalName? {
                            if (classInternalName in methodClassInternalNamesForRelocation) {
                                isChanged = true
                                if (classInternalNamesForForcedRelocation.add(classInternalName)) {
                                    logger.info("Relocation for {}: {} (forced)", className, classInternalName)
                                }
                                return relocatedClassInternalNamePrefix + classInternalName
                            }
                            return remapper.map(classInternalName)
                        }
                    }
                } else {
                    remapper
                }

                val mv = super.visitMethod(
                    access,
                    methodRemapper.mapMethodName(className, name, desc),
                    methodRemapper.mapMethodDesc(desc),
                    methodRemapper.mapSignature(signature, false),
                    exceptions?.let(methodRemapper::mapTypes)
                )
                return if (mv == null) null else MethodRemapper(mv, methodRemapper)
            }
        }

        classReader.accept(classRemapper)

        if (isChanged) {
            bytecodeModifier.modify(classWriter.toByteArray())
        }


        val classInternalName = classNameToClassInternalName(className)
        classInternalNamesForForcedRelocation
            .filter { alreadyRelocatedClassInternalNames.add(it) }
            .onEach { classesRelocation._relocatedClassSources[it] = classInternalName }
            .forEach { executeRelocateRecursiveAction(it, classpathArtifacts, context, true) }
        classInternalNamesForRelocation
            .filter { alreadyRelocatedClassInternalNames.add(it) }
            .onEach { classesRelocation._relocatedClassSources[it] = classInternalName }
            .forEach { executeRelocateRecursiveAction(it, classpathArtifacts, context, false) }
    }


    private data class RelocationInfo(
        val classInternalNamesForRelocation: Set<ClassInternalName>,
        val methodsClassInternalNamesForRelocation: Map<Method, Set<ClassInternalName>> = emptyMap()
    ) {
        companion object {
            val EMPTY = RelocationInfo(emptySet(), emptyMap())
        }
    }

    @Suppress("kotlin:S3776")
    private fun readRelocationInfo(bytecode: ByteArray, context: ProcessContext): RelocationInfo {
        if (!isRelocateClassesAnnotationInClasspath) return RelocationInfo.EMPTY

        var classInternalName: ClassInternalName? = null
        val classAnnotations = mutableListOf<AnnotationNode>()
        val methodAnnotations = mutableMapOf<Method, MutableList<AnnotationNode>>()
        ClassReader(bytecode).accept(
            object : ClassVisitor(ASM_API) {
                override fun visit(version: Int, access: Int, name: ClassInternalName?, signature: String?, superName: ClassInternalName?, interfaces: Array<ClassInternalName>?) {
                    classInternalName = name
                    super.visit(version, access, name, signature, superName, interfaces)
                }

                override fun visitAnnotation(desc: String?, visible: Boolean): AnnotationVisitor? {
                    if (relocateClassesDesc == desc || relocatePackagesDesc == desc) {
                        return AnnotationNode(desc).also { classAnnotations.add(it) }
                    }
                    return null
                }

                override fun visitMethod(access: Int, name: ClassInternalName, desc: String, signature: String?, exceptions: Array<ClassInternalName>?): MethodVisitor {
                    return object : MethodVisitor(api) {
                        override fun visitAnnotation(annotationDesc: String?, annotationVisible: Boolean): AnnotationVisitor? {
                            if (relocateClassesDesc == annotationDesc || relocatePackagesDesc == desc) {
                                return AnnotationNode(annotationDesc).also {
                                    methodAnnotations.computeIfAbsent(Method(name, desc), { mutableListOf() }).add(it)
                                }
                            }
                            return null
                        }
                    }
                }
            },
            SKIP_DEBUG or SKIP_FRAMES or SKIP_CODE
        )

        val outerClassRelocationInfo: RelocationInfo = run {
            if (classInternalName == null) return@run RelocationInfo.EMPTY
            val outerClassInternalName = outerClassInternalNameRegex.replace(classInternalName!!, "")
            if (outerClassInternalName != classInternalName) {
                val outerClassBytecode = context.readBinaryResource(outerClassInternalName + CLASS_FILE_NAME_SUFFIX)
                if (outerClassBytecode != null) {
                    return@run readRelocationInfo(outerClassBytecode, context)

                } else {
                    return@run RelocationInfo.EMPTY
                }
            } else {
                return@run RelocationInfo.EMPTY
            }
        }

        return RelocationInfo(
            classAnnotations.stream()
                .flatMap { getClassInternalNamesFromRelocateAnnotation(classInternalName, it).stream() }
                .plus(outerClassRelocationInfo.classInternalNamesForRelocation.stream())
                .toSet(),
            methodAnnotations.mapValues {
                it.value.stream()
                    .flatMap { getClassInternalNamesFromRelocateAnnotation(classInternalName, it).stream() }
                    .toSet()
            }
        )
    }

    private fun getClassInternalNamesFromRelocateAnnotation(classInternalName: ClassInternalName?, annotationNode: AnnotationNode): Set<ClassInternalName> {
        if (relocateClassesDesc == annotationNode.desc) {
            return annotationNode[RelocateClasses::value]?.stream()
                ?.map { it.internalName }
                ?.filter {
                    if (it in classInternalNamesExcludedFromForcedRelocation) {
                        logger.error("${classInternalName?.let(::classInternalNameToClassName)}: $it is excluded from forced relocation")
                        return@filter false
                    } else {
                        return@filter true
                    }
                }
                ?.toSet()
                ?: emptySet()

        } else if (relocatePackagesDesc == annotationNode.desc) {
            return Stream.concat(
                annotationNode[RelocatePackages::value]?.stream()
                    ?.map(::classNameToClassInternalName)
                    ?: emptyStream(),
                annotationNode[RelocatePackages::basePackageClasses]?.stream()
                    ?.map { it.internalName.substringBeforeLast('/', "") }
                    ?: emptyStream()
            )
                .filter(String::isNotEmpty)
                .distinct()
                .flatMap { packageInternalName ->
                    val prefix = "$packageInternalName/"
                    possibleClassInternalNamesForForcedRelocation.stream()
                        .filter { it.startsWith(prefix) }
                }
                .toSet()

        } else {
            throw UnsupportedOperationException("Unsupported annotation: ${annotationNode.desc}")
        }
    }


    private fun executeRelocateRecursiveAction(
        actionClassInternalName: ClassInternalName,
        classpathArtifacts: CachedArtifactsCollection,
        context: ProcessContext,
        force: Boolean
    ) = executeRelocateRecursiveAction(
        RelocateRecursiveAction(
            actionClassInternalName = actionClassInternalName,
            classpathArtifacts = classpathArtifacts,
            context = context,
            force = force
        )
    )


    private val relocateRecursiveActionsQueue = dequeOf<RelocateRecursiveAction>()

    private fun executeRelocateRecursiveAction(action: RelocateRecursiveAction) {
        relocateRecursiveActionsQueue.addLast(action)

        while (true) {
            val currentAction = relocateRecursiveActionsQueue.pollFirst()
            if (currentAction == null) break

            currentAction.execute()
        }
    }

    private inner class RelocateRecursiveAction(
        private val actionClassInternalName: ClassInternalName,
        private val classpathArtifacts: CachedArtifactsCollection,
        private val context: ProcessContext,
        private val force: Boolean
    ) {
        @Suppress("kotlin:S3776")
        fun execute() {
            if (actionClassInternalName == "module-info") return

            val actionInternalPackageName = actionClassInternalName.substringBeforeLast('/')
            val actionInternalPackagePrefix = if (actionInternalPackageName.isNotEmpty()) {
                actionInternalPackageName + '/'
            } else {
                ""
            }

            val actionClassResourceName = actionClassInternalName + CLASS_FILE_NAME_SUFFIX
            val actionClassArtifact = classpathArtifacts.entryMapping[actionClassResourceName]
            if (actionClassArtifact == null) {
                logger.warn("Artifact containing class can't be found: {}", actionClassInternalName)
                return
            }
            val bytecode = actionClassArtifact.readBytes(actionClassResourceName)

            logger.debug(
                "Relocating class {} from artifact {}",
                actionClassInternalName,
                actionClassArtifact
            )

            val classReader = ClassReader(bytecode)
            val classWriter = ClassWriter(classReader, 0)

            val addedAdditionalAnnotationDescrs = mutableSetOf<String>()
            val relocatedClassAnnotationAdder = object : ClassVisitor(ASM_API, classWriter) {
                override fun visit(version: Int, access: Int, name: ClassInternalName?, signature: String?, superName: ClassInternalName?, interfaces: Array<ClassInternalName>?) {
                    super.visit(version, access, name, signature, superName, interfaces)
                    visitAnnotation(internalDesc, false)?.visitEnd()
                    visitAnnotation(generatedDesc, false)?.visitEnd()
                    visitAnnotation(relocatedClassDesc, false)?.visitEnd()
                    visitAnnotation(suppressFBWarningsDesc, false)?.visitEnd()
                }

                override fun visitAnnotation(descriptor: String, visible: Boolean): AnnotationVisitor? {
                    if (descriptor in additionalAnnotationDescrs) {
                        if (!addedAdditionalAnnotationDescrs.add(descriptor)) {
                            return null
                        }
                    }
                    return super.visitAnnotation(descriptor, visible)
                }
            }

            val classInternalNamesForRelocation = mutableSetOf<ClassInternalName>()
            val resourcesForRelocation = mutableSetOf<String>()
            val classRemapper = ClassRemapper(relocatedClassAnnotationAdder, object : RelocationRemapper() {
                override fun map(classInternalName: ClassInternalName): ClassInternalName? {
                    if (classInternalName in possibleClassInternalNamesForRelocation
                        || (force && classInternalName in possibleClassInternalNamesForForcedRelocation)
                    ) {
                        classInternalNamesForRelocation.add(classInternalName)
                        return relocatedClassInternalNamePrefix + classInternalName
                    }
                    return null
                }

                override fun mapValue(value: Any?): Any? {
                    if (value is String) {
                        if (value.isBlank()) {
                            return value
                        }

                        super.mapValue(value)
                            .nullIf { this == value }
                            ?.let { return it }

                        val isAbsolutePath = value.startsWith('/')
                        val absoluteResourceName = if (isAbsolutePath) {
                            value.substring(1)
                        } else {
                            value
                        }
                        mapAbsoluteResourceName(absoluteResourceName)?.let {
                            return if (isAbsolutePath) {
                                "/$it"
                            } else {
                                it
                            }
                        }

                        if (!isAbsolutePath) {
                            mapRelativeResourceName(value)?.let { return it }
                        }
                    }
                    return super.mapValue(value)
                }

                private fun mapAbsoluteResourceName(absoluteResourceName: String): String? {
                    if (absoluteResourceName in possibleResourcesForRelocation
                        || (force && absoluteResourceName in possibleResourcesForForcedRelocation)
                    ) {
                        resourcesForRelocation.add(absoluteResourceName)
                        return relocatedClassInternalNamePrefix + absoluteResourceName
                    }
                    return null
                }

                private fun mapRelativeResourceName(resourceName: String): String? {
                    val absoluteResourceName = actionInternalPackagePrefix + resourceName
                    if (absoluteResourceName in possibleResourcesForRelocation
                        || (force && absoluteResourceName in possibleResourcesForForcedRelocation)
                    ) {
                        resourcesForRelocation.add(absoluteResourceName)
                        return resourceName
                    }
                    return null
                }
            })

            classReader.accept(classRemapper)

            val resultClassInternalName = relocatedClassInternalNamePrefix + actionClassInternalName
            val resultClassResourceName = resultClassInternalName + CLASS_FILE_NAME_SUFFIX
            context.writeBinaryResource(resultClassResourceName, classWriter.toByteArray())

            classesRelocation._relocatedClassNames[classInternalNameToClassName(actionClassInternalName)] = classInternalNameToClassName(resultClassInternalName)


            val serviceResourceName = SERVICE_FILE_BASE_PATH + '/' + classInternalNameToClassName(actionClassInternalName)
            classpathArtifacts.artifacts.forEach { artifact ->
                if (artifact.contains(serviceResourceName)) {
                    logger.debug(
                        "Relocating service file {} from artifact {}",
                        serviceResourceName,
                        artifact
                    )

                    val content = artifact.readBytes(serviceResourceName).toString(UTF_8)
                    val serviceClassNames = content.splitToSequence('\n')
                        .map { it.substringBefore('#') }
                        .map(String::trim)
                        .filter(String::isNotEmpty)
                        .toSet()
                    if (serviceClassNames.isNotEmpty()) {
                        val resultClassName = classInternalNameToClassName(resultClassInternalName)
                        serviceClassNames.forEach { serviceClassName ->
                            val serviceClassInternalName = classNameToClassInternalName(serviceClassName)
                            if (serviceClassInternalName in possibleClassInternalNamesForRelocation
                                || (force && serviceClassInternalName in possibleClassInternalNamesForForcedRelocation)
                            ) {
                                classInternalNamesForRelocation.add(serviceClassInternalName)
                                context.writeService(resultClassName, relocatedClassNamePrefix + serviceClassName)

                            } else {
                                context.writeService(resultClassName, serviceClassName)
                            }
                        }
                    }
                }
            }


            classInternalNamesForRelocation.toList().forEach { classInternalName ->
                val prefix: String
                val lastSlashPos = classInternalName.lastIndexOf('/')
                if (lastSlashPos >= 0) {
                    prefix = classInternalName.substring(0, lastSlashPos + 1)
                } else {
                    prefix = ""
                }
                val packageInfoClassInternalName = prefix + "package-info"
                if (packageInfoClassInternalName in possibleClassInternalNamesForRelocation
                    || (force && packageInfoClassInternalName in possibleClassInternalNamesForForcedRelocation)
                ) {
                    classInternalNamesForRelocation.add(packageInfoClassInternalName)
                }
            }


            classInternalNamesForRelocation
                .filter { alreadyRelocatedClassInternalNames.add(it) }
                .onEach { classesRelocation._relocatedClassSources[it] = actionClassInternalName }
                .let { classInternalNames ->
                    classInternalNames.forEach { executeRelocateRecursiveAction(it, classpathArtifacts, context, force) }
                }


            resourcesForRelocation.forEach forEachResource@{ resourceName ->
                val content = try {
                    actionClassArtifact.readBytes(resourceName)
                } catch (ignored: ArtifactEntryNotFoundException) {
                    logger.debug(
                        "Resource {} can't be found for relocating",
                        resourceName
                    )
                    return@forEachResource
                }

                logger.debug(
                    "Relocating resource {} from artifact {}",
                    resourceName,
                    actionClassArtifact
                )

                val resultResourceName = relocatedClassInternalNamePrefix + resourceName
                context.writeBinaryResource(resultResourceName, content)
            }
        }
    }


    override fun getStage() = RELOCATION_STAGE

}


private open class RelocationRemapper : Remapper() {
    override fun mapValue(value: Any?): Any? {
        if (value is String) {
            if (value.isBlank()) {
                return value
            }

            map(classNameToClassInternalName(value))
                .nullIf { this == value }
                ?.let { return classInternalNameToClassName(it) }

            map(value)
                .nullIf { this == value }
                ?.let { return it }
        }
        return super.mapValue(value)
    }
}


@AutoService
class RelocateClassesClassesProcessorFactory : ClassesProcessorsGradleTaskFactory {

    override fun createClassesProcessors(compileTask: AbstractCompile): List<ClassesProcessor> {
        if (!compileTask.project.isPluginAppliedAndNotDisabled(ClassesRelocationPlugin::class.java)) return emptyList()

        val mainSourceSet = compileTask.project.getOrNull(SourceSetContainer::class.java)?.findByName(MAIN_SOURCE_SET_NAME)
        if (mainSourceSet != null && !compileTask.isCompilingSourceSet(mainSourceSet)) return emptyList()

        val isRelocateClassesAnnotationInClasspath = compileTask.classpath.toHasEntries().containsClass(RelocateClasses::class.java)
        if (compileTask.project.configurations.relocateClasses.allDependencies.isEmpty() && !isRelocateClassesAnnotationInClasspath) return emptyList()

        val classesRelocation = compileTask.project[ClassesRelocationExtension::class.java]
        return listOf(
            RelocateClassesClassesProcessor(
                classesRelocation,
                CachedArtifactsCollection(compileTask.classpath),
                compileTask.project.configurations.relocateClasses,
                compileTask.project.configurations.excludeFromClassesRelocation,
                compileTask.project.configurations.excludeFromForcedClassesRelocation,
                isRelocateClassesAnnotationInClasspath
            )
        )
    }

}


@AutoService
class DisabledRelocateClassesClassesProcessorFactory : ClassesProcessorsGradleTaskFactory {

    companion object {
        @JvmStatic
        private val logger = getGradleLogger(DisabledRelocateClassesClassesProcessorFactory::class.java)
    }

    override fun createClassesProcessors(compileTask: AbstractCompile): List<ClassesProcessor> {
        val mainSourceSet = compileTask.project.getOrNull(SourceSetContainer::class.java)?.findByName(MAIN_SOURCE_SET_NAME)
        val isCompilingMainSourceSet = mainSourceSet != null && compileTask.isCompilingSourceSet(mainSourceSet)

        val isClassesRelocationPluginApplied = compileTask.project.isPluginAppliedAndNotDisabled(ClassesRelocationPlugin::class.java)
        if (isCompilingMainSourceSet && isClassesRelocationPluginApplied) return emptyList()

        val isRelocateClassesAnnotationInClasspath = compileTask.classpath.toHasEntries().containsClass(RelocateClasses::class.java)
        if (!isRelocateClassesAnnotationInClasspath) return emptyList()

        return listOf(ClassesProcessor { bytecode, _, className, _, _ ->
            var isAnnotatedBy = false
            val visitor = object : ClassVisitor(ASM_API) {
                override fun visitAnnotation(desc: String?, visible: Boolean): AnnotationVisitor? {
                    if (relocateClassesDesc == desc || relocatePackagesDesc == desc) isAnnotatedBy = true
                    return null
                }

                override fun visitMethod(
                    access: Int,
                    name: ClassInternalName?,
                    desc: String?,
                    signature: String?,
                    exceptions: Array<ClassInternalName>?
                ): MethodVisitor {
                    return object : MethodVisitor(api) {
                        override fun visitAnnotation(desc: String?, visible: Boolean): AnnotationVisitor? {
                            if (relocateClassesDesc == desc || relocatePackagesDesc == desc) isAnnotatedBy = true
                            return null
                        }
                    }
                }
            }
            ClassReader(bytecode).accept(visitor, SKIP_DEBUG or SKIP_FRAMES or SKIP_CODE)

            if (isAnnotatedBy) {
                if (!isCompilingMainSourceSet) {
                    logger.error(
                        "{}: Class/constructor/method is annotated by {}/{}. Classes relocation can be used only for {} source-set.",
                        className,
                        RelocateClasses::class.java.name,
                        RelocatePackages::class.java.name,
                        MAIN_SOURCE_SET_NAME
                    )

                } else if (!isClassesRelocationPluginApplied) {
                    logger.error(
                        "{}: Class/constructor/method is annotated by {}/{}. Apply '{}' Gradle plugin to process these annotations.",
                        className,
                        RelocateClasses::class.java.name,
                        RelocatePackages::class.java.name,
                        ClassesRelocationPlugin::class.java.getAnnotation(Plugin::class.java)?.id
                    )
                }
            }
        })
    }

}
