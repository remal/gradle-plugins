package name.remal.gradle_plugins.plugins.classes_relocation

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.api.BuildTimeConstants.getClassDescriptor
import name.remal.gradle_plugins.dsl.internal.RelocatedClass
import name.remal.gradle_plugins.dsl.utils.isAnnotatedBy
import name.remal.gradle_plugins.plugins.classes_processing.ClassesProcessingFilter

@AutoService
class RelocatedClassClassesProcessingFilter : ClassesProcessingFilter {

    companion object {
        private val relocatedClassDesc: String = getClassDescriptor(RelocatedClass::class.java)
    }

    override fun canBytecodeBeProcessed(bytecode: ByteArray) = !isAnnotatedBy(bytecode, relocatedClassDesc)

}
