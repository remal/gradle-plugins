package name.remal.gradle_plugins.plugins.ci

import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.CreateExtensionsPluginAction
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.extensions.addPlugin
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.isRootProject
import org.gradle.StartParameter
import org.gradle.api.Project
import org.gradle.api.logging.configuration.ShowStacktrace.ALWAYS
import org.gradle.api.logging.configuration.ShowStacktrace.INTERNAL_EXCEPTIONS
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.reflect.TypeOf.typeOf

@Plugin(
    id = "name.remal.common-ci",
    description = "Plugin that helps to build project on CI servers.",
    tags = ["ci"]
)
class CommonCIPlugin : BaseReflectiveProjectPlugin() {

    @CreateExtensionsPluginAction
    fun ExtensionContainer.`Create extensions`(project: Project) {
        val ci: CIExtension
        if (project.isRootProject) {
            ci = create(typeOf(CIExtension::class.java), "ci", CIExtensionImpl::class.java)

        } else {
            val rootProject = project.rootProject
            rootProject.applyPlugin(CommonCIPlugin::class.java)
            val rootCI = rootProject[CIExtension::class.java]
            ci = object : CIExtension by rootCI {}
            add(typeOf(CIExtension::class.java), "ci", ci)
        }

        project.convention.addPlugin(CIMethodsExtension(ci))
    }

    @PluginAction
    fun StartParameter.`Show Gradle stacktrace`(project: Project) {
        project[CIExtension::class.java].forBuildOnCI {
            if (showStacktrace == INTERNAL_EXCEPTIONS) {
                showStacktrace = ALWAYS
            }
        }
    }

}
