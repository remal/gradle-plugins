package name.remal.gradle_plugins.plugins.fatjar

import name.remal.gradle_plugins.dsl.Extension
import org.gradle.api.file.FileTreeElement
import org.gradle.api.specs.Spec

@Extension
class FatJarSettingsExtension {

    val excludePatterns: MutableSet<String> = sortedSetOf()

    fun exclude(vararg excludes: String) {
        excludePatterns.addAll(excludes)
    }

    fun exclude(excludes: Iterable<String>) {
        excludePatterns.addAll(excludes)
    }


    val excludeSpecs = mutableListOf<Spec<FileTreeElement>>()

    fun exclude(excludeSpec: Spec<FileTreeElement>) {
        excludeSpecs.add(excludeSpec)
    }

}
