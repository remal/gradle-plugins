package name.remal.gradle_plugins.plugins.fatjar

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.dsl.extensions.isPluginApplied
import name.remal.gradle_plugins.plugins.dependencies.TransitiveDependenciesConfigurationMatcher
import org.gradle.api.Project
import org.gradle.api.artifacts.Configuration

@AutoService
class FatJarPluginTransitiveDependenciesConfigurationMatcher : TransitiveDependenciesConfigurationMatcher {
    override fun matches(project: Project, configuration: Configuration): Boolean {
        if (!project.isPluginApplied(FatJarPlugin::class.java)) return false
        if (FAT_JAR_CONFIGURATION_NAME == configuration.name) return true
        return false
    }
}
