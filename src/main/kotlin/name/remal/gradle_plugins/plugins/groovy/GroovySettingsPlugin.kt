package name.remal.gradle_plugins.plugins.groovy

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.PluginActionsGroup
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.dsl.extensions.compileGroovyTaskName
import name.remal.gradle_plugins.dsl.extensions.compileKotlinTaskName
import name.remal.gradle_plugins.dsl.extensions.doSetupIfAndAfterEvaluate
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.isTargetJava8Compatible
import name.remal.gradle_plugins.dsl.extensions.logDebug
import name.remal.gradle_plugins.dsl.extensions.parameters
import name.remal.gradle_plugins.plugins.common.CommonSettingsPlugin
import name.remal.gradle_plugins.plugins.java.JavaSettingsPlugin
import name.remal.gradle_plugins.plugins.kotlin.KotlinJvmPluginId
import org.gradle.api.Project
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.compile.AbstractCompile
import org.gradle.api.tasks.compile.GroovyCompile
import java.nio.charset.StandardCharsets.UTF_8

@Plugin(
    id = "name.remal.groovy-settings",
    description = "Plugin that configures groovy plugin if it applied.",
    tags = ["groovy"]
)
@WithPlugins(GroovyPluginId::class)
@ApplyPluginClasses(CommonSettingsPlugin::class, JavaSettingsPlugin::class)
class GroovySettingsPlugin : BaseReflectiveProjectPlugin() {

    @PluginAction
    @WithPlugins(KotlinJvmPluginId::class)
    fun SourceSetContainer.`GroovyCompile tasks should have Kotlin compiled classes in classpath`(tasks: TaskContainer, project: Project) {
        all { sourceSet ->
            val compileTasks = tasks[AbstractCompile::class.java]
            val compileGroovy = compileTasks.findByName(sourceSet.compileGroovyTaskName) ?: return@all
            val compileKotlin = compileTasks.findByName(sourceSet.compileKotlinTaskName) ?: return@all
            compileGroovy.classpath = compileGroovy.classpath + project.files(compileKotlin.destinationDirectory.asFile.get())
        }
    }

    @PluginActionsGroup
    inner class `For all GroovyCompile tasks` {

        @PluginAction
        fun TaskContainer.`Set default encoding to UTF-8`() {
            all(GroovyCompile::class.java) {
                if (null == it.options.encoding) {
                    it.logDebug("Setting Java encoding to UTF-8")
                    it.options.encoding = UTF_8.name()
                }
                if (null == it.groovyOptions.encoding) {
                    it.logDebug("Setting Groovy encoding to UTF-8")
                    it.groovyOptions.encoding = UTF_8.name()
                }
            }
        }

        @PluginAction
        fun TaskContainer.`Enable displaying deprecation warnings`() {
            all(GroovyCompile::class.java) {
                it.options.isDeprecation = true
            }
        }

        @PluginAction("Add '--parameters' compiler option if Groovy version is equal to 2.5 or above and targetting Java 8 or above")
        fun TaskContainer.addParametersCompilerOption() {
            all(GroovyCompile::class.java) {
                it.doSetupIfAndAfterEvaluate(AbstractCompile::isTargetJava8Compatible) { task ->
                    task.options.parameters = true
                    task.groovyOptions.parameters = true
                }
            }
        }

    }

}
