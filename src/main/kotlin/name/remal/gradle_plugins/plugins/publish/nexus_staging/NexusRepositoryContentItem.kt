package name.remal.gradle_plugins.plugins.publish.nexus_staging

import java.net.URI
import java.time.ZonedDateTime

data class NexusRepositoryContentItem(
    val resourceURI: URI,
    val relativePath: String,
    val leaf: Boolean,
    val lastModified: ZonedDateTime
)
