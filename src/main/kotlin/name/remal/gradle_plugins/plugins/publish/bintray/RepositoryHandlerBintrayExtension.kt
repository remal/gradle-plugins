package name.remal.gradle_plugins.plugins.publish.bintray

import groovy.lang.Closure
import groovy.lang.Closure.DELEGATE_FIRST
import groovy.lang.DelegatesTo
import name.remal.gradle_plugins.dsl.EnvironmentVariable
import name.remal.gradle_plugins.dsl.EnvironmentVariable.EnvironmentVariables
import name.remal.gradle_plugins.dsl.extensions.toConfigureAction
import name.remal.nullIfEmpty
import org.gradle.api.Action
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.artifacts.repositories.MavenArtifactRepository
import java.lang.System.getenv

@EnvironmentVariables(
    value = [
        EnvironmentVariable("BINTRAY_USER", description = "Bintray user"),
        EnvironmentVariable("BINTRAY_API_KEY", description = "Bintray API key")
    ],
    pluginClass = MavenPublishBintrayPlugin::class
)
class RepositoryHandlerBintrayExtension(private val repositories: RepositoryHandler) {

    @JvmOverloads
    fun bintray(configurer: Action<BintrayMavenArtifactRepository> = Action {}): MavenArtifactRepository {
        return repositories.maven { repo ->
            repo.name = "bintray"

            val user = getenv("BINTRAY_USER").nullIfEmpty()
            val password = getenv("BINTRAY_API_KEY").nullIfEmpty()
            if (user != null && password != null) {
                repo.credentials.username = user
                repo.credentials.password = password
            }

            configurer.execute(BintrayMavenArtifactRepositoryDefault(repo))
        }
    }

    fun bintray(@DelegatesTo(BintrayMavenArtifactRepository::class, strategy = DELEGATE_FIRST) configurer: Closure<*>) = bintray(configurer.toConfigureAction())

}
