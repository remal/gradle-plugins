package name.remal.gradle_plugins.plugins.publish.ossrh

import groovy.lang.Closure
import groovy.lang.Closure.DELEGATE_FIRST
import groovy.lang.DelegatesTo
import name.remal.gradle_plugins.dsl.EnvironmentVariable
import name.remal.gradle_plugins.dsl.EnvironmentVariable.EnvironmentVariables
import name.remal.gradle_plugins.dsl.extensions.applyPlugin
import name.remal.gradle_plugins.dsl.extensions.isNotSnapshotVersion
import name.remal.gradle_plugins.dsl.extensions.toConfigureAction
import name.remal.gradle_plugins.plugins.publish.nexus_staging.MavenPublishNexusStagingPlugin
import name.remal.gradle_plugins.plugins.signing.SigningPluginId
import name.remal.gradle_plugins.plugins.signing.SigningSettingsPlugin
import name.remal.nullIfEmpty
import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.artifacts.repositories.MavenArtifactRepository
import java.lang.System.getenv

@EnvironmentVariables(
    value = [
        EnvironmentVariable("OSS_USER", description = "OSS Repository Hosting user"),
        EnvironmentVariable("OSS_PASSWORD", description = "OSS Repository Hosting password"),
        EnvironmentVariable("OSSRH_USER", description = "OSS Repository Hosting user"),
        EnvironmentVariable("OSSRH_PASSWORD", description = "OSS Repository Hosting password")
    ],
    pluginClass = MavenPublishOssrhPlugin::class
)
class RepositoryHandlerOssrhExtension(private val project: Project, private val repositories: RepositoryHandler) {

    @JvmOverloads
    fun ossrh(configurer: Action<MavenArtifactRepository> = Action {}): MavenArtifactRepository {
        project.applyPlugin(SigningPluginId)
        project.applyPlugin(SigningSettingsPlugin::class.java)
        project.applyPlugin(MavenPublishNexusStagingPlugin::class.java)

        return repositories.maven { repo ->
            repo.name = "ossrh"
            repo.setUrl(
                if (project.isNotSnapshotVersion) {
                    "https://oss.sonatype.org/service/local/staging/deploy/maven2/"
                } else {
                    "https://oss.sonatype.org/content/repositories/snapshots/"
                }
            )

            val user = getenv("OSSRH_USER").nullIfEmpty() ?: getenv("OSS_USER").nullIfEmpty()
            val password = getenv("OSSRH_PASSWORD").nullIfEmpty() ?: getenv("OSS_PASSWORD").nullIfEmpty()
            if (user != null && password != null) {
                repo.credentials.username = user
                repo.credentials.password = password
            }

            configurer.execute(repo)
        }
    }

    fun ossrh(@DelegatesTo(MavenArtifactRepository::class, strategy = DELEGATE_FIRST) configurer: Closure<*>) = ossrh(configurer.toConfigureAction())

}
