package name.remal.gradle_plugins.plugins.publish.nexus_staging

import java.net.URI

data class NexusStagingRepository(
    val profileId: String,
    val repositoryId: String,
    val repositoryURI: URI,
    val type: String,
    val transitioning: Boolean
)
