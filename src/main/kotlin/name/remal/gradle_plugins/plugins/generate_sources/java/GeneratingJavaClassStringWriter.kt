package name.remal.gradle_plugins.plugins.generate_sources.java

import name.remal.gradle_plugins.plugins.generate_sources.BaseGeneratingJavaClassStringWriter
import java.io.File

class GeneratingJavaClassStringWriter(
    packageName: String = "",
    simpleName: String = "",
    classpath: Iterable<File> = emptyList(),
    wrapDepth: Int = 0
) : BaseGeneratingJavaClassStringWriter<GeneratingJavaClassStringWriter>(packageName, simpleName, classpath, wrapDepth), GeneratingJavaClassWriterInterface<GeneratingJavaClassStringWriter> {

    override val classFileExtension: String get() = "java"

    override fun newSubWriter() = GeneratingJavaClassStringWriter(packageName, simpleName, classpath, wrapDepth + 1)

}
