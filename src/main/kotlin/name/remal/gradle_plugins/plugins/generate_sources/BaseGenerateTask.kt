package name.remal.gradle_plugins.plugins.generate_sources

import name.remal.createParentDirectories
import name.remal.forceDeleteRecursively
import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.dsl.extensions.generatedSourcesDir
import name.remal.gradle_plugins.dsl.extensions.parents
import name.remal.gradle_plugins.dsl.extensions.requirePlugin
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.file.FileCollection
import org.gradle.api.tasks.CacheableTask
import org.gradle.api.tasks.Classpath
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFiles
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.PathSensitive
import org.gradle.api.tasks.PathSensitivity.ABSOLUTE
import org.gradle.api.tasks.TaskAction
import org.intellij.lang.annotations.Language
import java.io.File
import java.nio.charset.StandardCharsets.UTF_8

@BuildTask
@CacheableTask
abstract class BaseGenerateTask : DefaultTask() {

    @Input
    @Language("encoding-reference")
    var charset: String = UTF_8.name()

    @Classpath
    @InputFiles
    var classpath: FileCollection = project.files()

    @OutputDirectory
    var outputDir: File = project.generatedSourcesDir.resolve(name)

    private val generateActions: MutableMap<String, (File) -> Unit> = sortedMapOf()

    init {
        requirePlugin(JavaPluginId)
        requirePlugin(GenerateSourcesPlugin::class.java)
        onlyIf { generateActions.isNotEmpty() }
    }

    @TaskAction
    protected final fun executeGenerateActions() {
        outputDir.forceDeleteRecursively()
        generateActions.forEach { relativePath, generateAction ->
            val targetFile = File(outputDir, relativePath).absoluteFile
            targetFile.createParentDirectories()
            generateAction(targetFile)
        }
        didWork = true
    }

    protected final fun addGenerateAction(relativePath: String, generateAction: (file: File) -> Unit) {
        if (relativePath in generateActions) logger.warn("Redefining generation of {}", relativePath)
        generateActions[relativePath] = generateAction
    }

    @get:Input
    @get:Optional
    protected val objectsToCacheBy = mutableSetOf<Any>().apply {
        add(generateActions.keys)
    }

    @get:Optional
    @get:InputFiles
    @get:PathSensitive(ABSOLUTE)
    protected val filesToCacheBy = mutableSetOf<File>()

    @get:Optional
    @get:InputFiles
    @get:PathSensitive(ABSOLUTE)
    protected val buildFiles: Set<File> by lazy {
        (sequenceOf(project) + project.parents.asSequence())
            .map(Project::getBuildFile)
            .flatMap {
                sequenceOf(
                    it,
                    it.resolveSibling("settings.gradle"),
                    it.resolveSibling("settings.gradle.kts"),
                    it.resolveSibling("gradle.properties")
                )
            }
            .filter(File::isFile)
            .toSet()
    }

}
