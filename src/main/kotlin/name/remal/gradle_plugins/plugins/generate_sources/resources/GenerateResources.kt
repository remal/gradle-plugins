package name.remal.gradle_plugins.plugins.generate_sources.resources

import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.plugins.generate_sources.BaseGenerateTask
import name.remal.gradle_plugins.plugins.generate_sources.GeneratingOutputStream
import name.remal.gradle_plugins.plugins.generate_sources.GeneratingWriter
import org.gradle.api.tasks.CacheableTask
import java.nio.charset.Charset

@BuildTask
@CacheableTask
class GenerateResources : BaseGenerateTask() {

    fun textFile(relativePath: String, action: (writer: GeneratingWriter) -> Unit) {
        addGenerateAction(relativePath) { file ->
            GeneratingWriter(file, relativePath, this, file.writer(Charset.forName(charset))).use(action)
        }
    }

    fun binaryFile(relativePath: String, action: (outputStream: GeneratingOutputStream) -> Unit) {
        addGenerateAction(relativePath) { file ->
            GeneratingOutputStream(file, relativePath, this, file.outputStream()).use(action)
        }
    }

    private var services = mutableMapOf<String, MutableSet<String>>().also { objectsToCacheBy.add(it) }

    fun service(serviceName: String, implementationClassName: String) {
        val implementations = services.computeIfAbsent(serviceName) {
            val implementations = mutableSetOf<String>()
            textFile("META-INF/services/$serviceName") { writer ->
                implementations.forEach { writer.append(it).append('\n') }
            }
            return@computeIfAbsent implementations
        }
        implementations.add(implementationClassName)
    }

    fun service(serviceClass: Class<*>, implementationClassName: String) = service(serviceClass.name, implementationClassName)
    fun service(serviceName: String, implementationClass: Class<*>) = service(serviceName, implementationClass.name)
    fun <T> service(service: Class<T>, implementationClass: Class<out T>) = service(service.name, implementationClass.name)

}
