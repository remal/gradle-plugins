package name.remal.gradle_plugins.plugins.generate_sources.java

import name.remal.gradle_plugins.plugins.generate_sources.BaseGenerateTask
import name.remal.gradle_plugins.plugins.generate_sources.BaseGeneratingJavaClassWriter
import java.io.File
import java.io.StringWriter
import java.io.Writer

class GeneratingJavaClassWriter(
    packageName: String,
    simpleName: String,
    targetFile: File,
    relativePath: String,
    generateTask: BaseGenerateTask,
    delegate: Writer,
    wrapDepth: Int = 0
) : BaseGeneratingJavaClassWriter<GeneratingJavaClassWriter>(packageName, simpleName, targetFile, relativePath, generateTask, delegate, wrapDepth),
    GeneratingJavaClassWriterInterface<GeneratingJavaClassWriter> {

    override fun wrapStringWriter(stringWriter: StringWriter) = GeneratingJavaClassWriter(packageName, simpleName, targetFile, relativePath, generateTask, stringWriter, wrapDepth + 1)

}
