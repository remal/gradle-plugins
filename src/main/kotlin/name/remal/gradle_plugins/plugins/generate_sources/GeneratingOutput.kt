package name.remal.gradle_plugins.plugins.generate_sources

import name.remal.gradle_plugins.dsl.artifact.CachedArtifactsCollection
import java.io.File

interface GeneratingOutput : GeneratingWithClasspath {

    val targetFile: File
    val relativePath: String
    val generateTask: BaseGenerateTask

    override fun isClassInClasspath(className: String) = CachedArtifactsCollection(generateTask.classpath).containsClass(className)

}
