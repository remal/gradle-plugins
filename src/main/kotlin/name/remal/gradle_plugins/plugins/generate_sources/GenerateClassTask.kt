package name.remal.gradle_plugins.plugins.generate_sources

import groovy.lang.Closure
import groovy.lang.Closure.DELEGATE_FIRST
import groovy.lang.DelegatesTo
import name.remal.gradle_plugins.dsl.extensions.afterEvaluateOrNow
import name.remal.gradle_plugins.dsl.extensions.javaPackageName
import name.remal.gradle_plugins.dsl.extensions.toConfigureKotlinFunction
import org.gradle.api.Task

interface GenerateClassTask<WriterType : BaseGeneratingClassWriter<WriterType>> : Task {

    fun classFile(packageName: String, simpleName: String, action: (writer: WriterType) -> Unit)

    fun classFile(packageName: String, simpleName: String, @DelegatesTo(strategy = DELEGATE_FIRST) action: Closure<*>) = classFile(packageName, simpleName, action.toConfigureKotlinFunction())


    fun classFile(simpleName: String, action: (writer: WriterType) -> Unit) {
        project.afterEvaluateOrNow(Int.MIN_VALUE) { _ ->
            classFile(project.javaPackageName, simpleName, action)
        }
    }

    fun classFile(simpleName: String, @DelegatesTo(strategy = DELEGATE_FIRST) action: Closure<*>) = classFile(simpleName, action.toConfigureKotlinFunction())

}
