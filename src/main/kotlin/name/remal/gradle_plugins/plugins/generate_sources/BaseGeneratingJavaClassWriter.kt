package name.remal.gradle_plugins.plugins.generate_sources

import java.io.File
import java.io.Writer

abstract class BaseGeneratingJavaClassWriter<Self : BaseGeneratingJavaClassWriter<Self>>(
    packageName: String,
    simpleName: String,
    targetFile: File,
    relativePath: String,
    generateTask: BaseGenerateTask,
    delegate: Writer,
    wrapDepth: Int = 0
) : BaseGeneratingClassWriter<Self>(packageName, simpleName, targetFile, relativePath, generateTask, delegate, wrapDepth), BaseGeneratingJavaClassWriterInterface<Self>
