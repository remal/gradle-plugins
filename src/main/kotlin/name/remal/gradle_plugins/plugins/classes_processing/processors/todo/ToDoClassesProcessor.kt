package name.remal.gradle_plugins.plugins.classes_processing.processors.todo

import name.remal.accept
import name.remal.get
import name.remal.gradle_plugins.api.classes_processing.BytecodeModifier
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor.COLLECTION_STAGE
import name.remal.gradle_plugins.api.classes_processing.ProcessContext
import name.remal.gradle_plugins.dsl.utils.forEachAnnotationNode
import name.remal.gradle_plugins.dsl.utils.getGradleLogger
import org.objectweb.asm.ClassReader
import org.objectweb.asm.tree.AnnotationNode
import org.objectweb.asm.tree.ClassNode
import name.remal.gradle_plugins.api.todo.FIXME as FixMe
import name.remal.gradle_plugins.api.todo.FIXME.FIXMEs as FixMes
import name.remal.gradle_plugins.api.todo.TODO as ToDo
import name.remal.gradle_plugins.api.todo.TODO.TODOs as ToDos

class ToDoClassesProcessor : ClassesProcessor {

    companion object {
        private val logger = getGradleLogger(ToDoClassesProcessor::class.java)
    }

    override fun process(bytecode: ByteArray, bytecodeModifier: BytecodeModifier, className: String, resourceName: String, context: ProcessContext) {
        val classNode = ClassNode().also { ClassReader(bytecode).accept(it) }

        fun AnnotationNode.processFixMeAnnotationNode() {
            get(FixMe::value)?.let { value ->
                logger.error("{}({}:1): {}: {}", className, classNode.sourceFile ?: "(Unknown Source)", FixMe::class.java.simpleName, value)
            }
        }

        forEachAnnotationNode(classNode, FixMe::class.java, AnnotationNode::processFixMeAnnotationNode)

        fun AnnotationNode.processFixMesAnnotationNode() {
            get(FixMes::value)?.forEach(AnnotationNode::processFixMeAnnotationNode)
        }

        forEachAnnotationNode(classNode, FixMes::class.java, AnnotationNode::processFixMesAnnotationNode)

        fun AnnotationNode.processToDoAnnotationNode() {
            get(ToDo::value)?.let { value ->
                logger.warn("{}({}:1): {}: {}", className, classNode.sourceFile ?: "(Unknown Source)", ToDo::class.java.simpleName, value)
            }
        }

        forEachAnnotationNode(classNode, ToDo::class.java, AnnotationNode::processToDoAnnotationNode)

        fun AnnotationNode.processToDosAnnotationNode() {
            get(ToDos::value)?.forEach(AnnotationNode::processToDoAnnotationNode)
        }

        forEachAnnotationNode(classNode, ToDos::class.java, AnnotationNode::processToDosAnnotationNode)
    }

    override fun getStage() = COLLECTION_STAGE

}
