package name.remal.gradle_plugins.plugins.classes_processing.processors.auto_service

import name.remal.ASM_API
import name.remal.filterNotNull
import name.remal.get
import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.api.BuildTimeConstants.getClassDescriptor
import name.remal.gradle_plugins.api.classes_processing.BytecodeModifier
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor.COLLECTION_STAGE
import name.remal.gradle_plugins.api.classes_processing.ProcessContext
import name.remal.plus
import name.remal.toSet
import org.objectweb.asm.AnnotationVisitor
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassReader.SKIP_CODE
import org.objectweb.asm.ClassReader.SKIP_DEBUG
import org.objectweb.asm.ClassReader.SKIP_FRAMES
import org.objectweb.asm.ClassVisitor
import org.objectweb.asm.Type
import org.objectweb.asm.tree.AnnotationNode
import java.util.stream.Stream

class AutoServiceClassesProcessor : ClassesProcessor {

    companion object {
        private val autoServiceDesc: String = getClassDescriptor(AutoService::class.java)
    }

    override fun process(bytecode: ByteArray, bytecodeModifier: BytecodeModifier, className: String, resourceName: String, context: ProcessContext) {
        var superClassName: String? = null
        val interfaceClassNames = mutableSetOf<String>()
        val annotationNodes = mutableListOf<AnnotationNode>()
        ClassReader(bytecode).accept(
            object : ClassVisitor(ASM_API) {
                override fun visit(version: Int, access: Int, name: String?, signature: String?, superName: String?, interfaces: Array<String>?) {
                    superClassName = superName?.replace('/', '.')
                    interfaces?.forEach { interfaceClassNames.add(it.replace('/', '.')) }
                }

                override fun visitAnnotation(desc: String?, visible: Boolean): AnnotationVisitor? {
                    if (autoServiceDesc == desc) {
                        return AnnotationNode(desc).also { annotationNodes.add(it) }
                    }
                    return null
                }
            },
            SKIP_DEBUG or SKIP_FRAMES or SKIP_CODE
        )

        val serviceNames = annotationNodes.stream()
            .map { it[AutoService::value] }
            .flatMap {
                if (it != null && it.isNotEmpty()) {
                    it.stream()
                } else {
                    (Stream.of(superClassName) + interfaceClassNames.stream())
                        .filterNotNull()
                        .filter { !it.startsWith("java.") }
                        .filter { !it.startsWith("kotlin.") }
                        .filter { !it.startsWith("groovy.") }
                        .filter { !it.startsWith("scala.") }
                }
            }
            .map { if (it is Type) it.className else it.toString() }
            .toSet()

        serviceNames.forEach { serviceName ->
            context.writeService(serviceName, className)
        }
    }

    override fun getStage() = COLLECTION_STAGE

}
