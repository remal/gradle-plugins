package name.remal.gradle_plugins.plugins.assertj.internal

import org.gradle.api.logging.Logger
import java.io.File

internal interface AssertionGeneratorInvoker {
    fun invoke(classesDirs: Iterable<File>, classNames: List<String>, outputDir: File, classpath: Iterable<File>, logger: Logger)
}
