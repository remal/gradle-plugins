package name.remal.gradle_plugins.plugins.kotlin

import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.PluginActionsGroup
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.all
import name.remal.gradle_plugins.plugins.common.CommonSettingsPlugin
import name.remal.gradle_plugins.plugins.java.JavaSettingsPlugin
import org.gradle.api.tasks.TaskContainer
import org.jetbrains.kotlin.gradle.tasks.Kotlin2JsCompile

@Plugin(
    id = "name.remal.kotlin-js-settings",
    description = "Plugin that configures Kotlin JS plugins if some of them are applied.",
    tags = ["kotlin"]
)
@WithPlugins(KotlinJsPluginId::class)
@ApplyPluginClasses(CommonSettingsPlugin::class, JavaSettingsPlugin::class)
class KotlinJsSettingsPlugin : BaseReflectiveProjectPlugin() {

    @PluginActionsGroup(order = 10)
    inner class `Setup Kotlin for JavaScript` {
        @PluginActionsGroup
        inner class `For all Kotlin2JsCompile tasks` {

            @PluginAction
            fun TaskContainer.`Set metaInfo = true`() {
                all(Kotlin2JsCompile::class.java) {
                    it.kotlinOptions.metaInfo = true
                }
            }

            @PluginAction
            fun TaskContainer.`Set sourceMap = true`() {
                all(Kotlin2JsCompile::class.java) {
                    it.kotlinOptions.sourceMap = true
                }
            }

            @PluginAction
            fun TaskContainer.`Set moduleKind = 'umd'`() {
                all(Kotlin2JsCompile::class.java) {
                    it.kotlinOptions.moduleKind = "umd"
                }
            }

            @PluginAction
            fun TaskContainer.`Set noStdlib = false`() {
                all(Kotlin2JsCompile::class.java) {
                    it.kotlinOptions.noStdlib = false
                }
            }

        }
    }


}
