package name.remal.gradle_plugins.plugins.kotlin_classes_processing

import name.remal.InstructionNodeFilter
import name.remal.accept
import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.api.classes_processing.BytecodeModifier
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessor.POST_PROCESSING_STAGE
import name.remal.gradle_plugins.api.classes_processing.ClassesProcessorsGradleTaskFactory
import name.remal.gradle_plugins.api.classes_processing.ProcessContext
import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.WithPlugins
import name.remal.gradle_plugins.dsl.extensions.isPluginApplied
import name.remal.gradle_plugins.dsl.extensions.isPluginAppliedAndNotDisabled
import name.remal.gradle_plugins.plugins.classes_processing.ClassesProcessingPlugin
import name.remal.gradle_plugins.plugins.kotlin.KotlinJvmPluginId
import name.remal.replaceInstructions
import org.gradle.api.tasks.compile.AbstractCompile
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.ClassWriter.COMPUTE_MAXS
import org.objectweb.asm.Opcodes.ALOAD
import org.objectweb.asm.Opcodes.ATHROW
import org.objectweb.asm.Opcodes.DUP
import org.objectweb.asm.Opcodes.IFNONNULL
import org.objectweb.asm.Opcodes.INVOKESPECIAL
import org.objectweb.asm.Opcodes.INVOKESTATIC
import org.objectweb.asm.Opcodes.NEW
import org.objectweb.asm.Type.getArgumentTypes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.InsnList
import org.objectweb.asm.tree.InsnNode
import org.objectweb.asm.tree.JumpInsnNode
import org.objectweb.asm.tree.LabelNode
import org.objectweb.asm.tree.LdcInsnNode
import org.objectweb.asm.tree.MethodInsnNode
import org.objectweb.asm.tree.TypeInsnNode
import org.objectweb.asm.tree.VarInsnNode

@Plugin(
    id = "name.remal.inline-kotlin-intrinsics",
    description = "Process compiled classes and inline Kotlin intrinsics",
    tags = ["kotlin"],
    isHidden = true
)
@WithPlugins(KotlinJvmPluginId::class)
@ApplyPluginClasses(ClassesProcessingPlugin::class)
class InlineKotlinIntrinsicsPlugin : BaseReflectiveProjectPlugin()


class InlineKotlinIntrinsicsClassesProcessor : ClassesProcessor {

    override fun process(bytecode: ByteArray, bytecodeModifier: BytecodeModifier, className: String, resourceName: String, context: ProcessContext) {
        var isChanged = false
        val classReader = ClassReader(bytecode)
        val classNode = ClassNode()
        classReader.accept(classNode)

        classNode.methods?.forEach forEachMethodNode@{ methodNode ->
            if (getArgumentTypes(methodNode.desc).any { it.className.startsWith("kotlin.") }) {
                return@forEachMethodNode
            }

            methodNode.replaceInstructions(
                InstructionNodeFilter(VarInsnNode::class.java, { it.node.opcode == ALOAD }),
                InstructionNodeFilter(LdcInsnNode::class.java, { it.node.cst is String }),
                InstructionNodeFilter(MethodInsnNode::class.java, {
                    it.node.opcode == INVOKESTATIC
                        && it.node.owner == "kotlin/jvm/internal/Intrinsics"
                        && it.node.name == "checkParameterIsNotNull"
                        && it.node.desc == "(Ljava/lang/Object;Ljava/lang/String;)V"
                })
            ) { varInsnNode, ldcInsnNode, _ ->
                isChanged = true
                return@replaceInstructions InsnList().apply {
                    add(varInsnNode.clone(emptyMap()))
                    val endIfLabel = LabelNode()
                    add(JumpInsnNode(IFNONNULL, endIfLabel))
                    add(TypeInsnNode(NEW, "java/lang/IllegalArgumentException"))
                    add(InsnNode(DUP))
                    add(LdcInsnNode("Parameter specified as non-null is null: method $className.${methodNode.name}, parameter ${ldcInsnNode.cst}"))
                    add(MethodInsnNode(INVOKESPECIAL, "java/lang/IllegalArgumentException", "<init>", "(Ljava/lang/String;)V", false))
                    add(InsnNode(ATHROW))
                    add(endIfLabel)
                }
            }
        }

        if (isChanged) {
            val classWriter = ClassWriter(classReader, COMPUTE_MAXS)
            classNode.accept(classWriter)
            bytecodeModifier.modify(classWriter.toByteArray())
        }
    }

    override fun getStage() = POST_PROCESSING_STAGE

}

@AutoService
class InlineKotlinIntrinsicsClassesProcessorFactory : ClassesProcessorsGradleTaskFactory {
    override fun createClassesProcessors(compileTask: AbstractCompile): List<ClassesProcessor> {
        if (!compileTask.project.isPluginApplied(KotlinJvmPluginId)) return emptyList()
        if (!compileTask.project.isPluginAppliedAndNotDisabled(InlineKotlinIntrinsicsPlugin::class.java)) return emptyList()
        return listOf(InlineKotlinIntrinsicsClassesProcessor())
    }
}
