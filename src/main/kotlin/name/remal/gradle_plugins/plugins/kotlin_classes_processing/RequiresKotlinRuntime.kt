package name.remal.gradle_plugins.plugins.kotlin_classes_processing

import kotlin.annotation.AnnotationRetention.BINARY
import kotlin.annotation.AnnotationTarget.ANNOTATION_CLASS
import kotlin.annotation.AnnotationTarget.CLASS
import kotlin.annotation.AnnotationTarget.FILE
import kotlin.reflect.KClass

@Target(CLASS, ANNOTATION_CLASS, FILE)
@Retention(BINARY)
internal annotation class RequiresKotlinRuntime(vararg val value: KClass<*> = [])
