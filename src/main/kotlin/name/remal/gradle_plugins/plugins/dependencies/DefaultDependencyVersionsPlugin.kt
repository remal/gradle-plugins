package name.remal.gradle_plugins.plugins.dependencies

import name.remal.Services.loadServicesList
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.BuildTask
import name.remal.gradle_plugins.dsl.CreateConfigurationsPluginAction
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.PluginActionsGroup
import name.remal.gradle_plugins.dsl.extensions.onlyIfFirstTaskWithTheNameInGraph
import name.remal.gradle_plugins.dsl.reflective_project_plugin.mixin.CurrentProjectIsARootProjectMixin
import org.gradle.api.DefaultTask
import org.gradle.api.Project
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.plugins.HelpTasksPlugin.HELP_GROUP
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.TaskContainer

private val defaultDependencyVersionFactories = loadServicesList(DefaultDependencyVersionFactory::class.java)
private val defaultDependencyVersions = loadServicesList(DefaultDependencyVersion::class.java)

@Plugin(
    id = "name.remal.default-dependency-version",
    description = "Plugin provides resolution strategy that sets dependency version if it's not set. Please run 'defaultDependencyVersionsHelp' to get help.",
    tags = ["common", "dependencies"]
)
class DefaultDependencyVersionsPlugin : BaseReflectiveProjectPlugin() {

    @CreateConfigurationsPluginAction
    fun ConfigurationContainer.`Setup resolution strategy for all configurations`(project: Project) {
        all { conf ->
            conf.resolutionStrategy.eachDependency { details ->
                with(details) {
                    with(details.target) {
                        if (!version.isNullOrBlank()) {
                            return@eachDependency
                        }

                        val defaultDependencyVersionsFull = sortedSetOf<DefaultDependencyVersion>().apply {
                            defaultDependencyVersionFactories.forEach { addAll(it.create(project)) }
                            addAll(defaultDependencyVersions)
                        }
                        for (defaultDependencyVersion in defaultDependencyVersionsFull) {
                            if (defaultDependencyVersion.matches(group, name)) {
                                useVersion(defaultDependencyVersion.version)
                                break
                            }
                        }
                    }
                }
            }
        }
    }

    @PluginActionsGroup
    inner class `For root project` : CurrentProjectIsARootProjectMixin {

        @PluginAction
        fun TaskContainer.`Create 'defaultDependencyVersionsHelp' task`() {
            create("defaultDependencyVersionsHelp", DefaultDependencyVersionsHelp::class.java)
        }

    }

}


@BuildTask
class DefaultDependencyVersionsHelp : DefaultTask() {

    init {
        group = HELP_GROUP
        onlyIfFirstTaskWithTheNameInGraph()
    }

    @TaskAction
    protected fun displayHelp() {
        val defaultDependencyVersionsFull = sortedSetOf<DefaultDependencyVersion>().apply {
            defaultDependencyVersionFactories.forEach { addAll(it.create(project)) }
            addAll(defaultDependencyVersions)
        }

        if (defaultDependencyVersionsFull.isEmpty()) {
            logger.lifecycle("No rules are registered for dependencies with empty version.")
            return
        }

        logger.lifecycle("These rules are registered for dependencies with empty version:")
        defaultDependencyVersionsFull.forEach { rule ->
            logger.lifecycle("    ${rule.notation} -> ${rule.version}")
        }
    }

}
