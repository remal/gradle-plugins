package name.remal.gradle_plugins.plugins.dependencies.component_metadata

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.plugins.dependencies.AbstractComponentMetadata
import name.remal.gradle_plugins.plugins.dependencies.NebulaResolutionRules
import org.gradle.api.Project
import org.gradle.api.artifacts.ComponentMetadataDetails
import org.gradle.api.artifacts.ModuleVersionIdentifier
import javax.inject.Inject

@AutoService
class NebulaResolutionRulesMetadata @Inject constructor(project: Project) : AbstractComponentMetadata(project) {

    override fun ModuleVersionIdentifier.process(details: ComponentMetadataDetails) {
        NebulaResolutionRules.addComponentMetadata(this, details)
    }

}
