package name.remal.gradle_plugins.plugins.dependencies

import name.remal.default
import name.remal.escapeRegex
import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.version.Version

interface DefaultDependencyVersion : Comparable<DefaultDependencyVersion> {

    val notation: String
    val version: String

    fun matches(dependencyNotation: String): Boolean {
        if (!notation.contains('*')) return dependencyNotation == notation
        val regex = Regex(notation.split('*').joinToString(".*", transform = ::escapeRegex))
        return regex.matches(dependencyNotation)
    }

    fun matches(group: String?, name: String?): Boolean {
        return matches("${group.default()}:${name.default()}")
    }

    override fun compareTo(other: DefaultDependencyVersion): Int {
        compareNotations(notation, other.notation).let { if (it != 0) return it }
        compareVersions(version, other.version).let { if (it != 0) return it }
        return 0
    }

}


fun defaultDependencyVersionFromDependencyNotation(dependencyNotation: DependencyNotation): DefaultDependencyVersion {
    return object : DefaultDependencyVersion {
        override val notation: String = dependencyNotation.withOnlyGroupAndModule().toString()
        override val version: String = dependencyNotation.version
    }
}


private val MAX_CHAR = Character.MAX_CODE_POINT.toChar()

private fun compareNotations(notation1: String, notation2: String): Int {
    if (notation1 == notation2) return 0
    return notation1.replace('*', MAX_CHAR).compareTo(notation2.replace('*', MAX_CHAR))
}

private fun compareVersions(version1: String, version2: String): Int {
    if (version1 == version2) return 0
    Version.parseOrNull(version1)?.let { v1 ->
        Version.parseOrNull(version2)?.let { v2 ->
            return v1.compareTo(v2)
        }
    }
    return version1.compareTo(version2)
}
