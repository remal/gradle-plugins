package name.remal.gradle_plugins.plugins.dependencies

import name.remal.gradle_plugins.dsl.Extension
import name.remal.gradle_plugins.plugins.gradle_plugins.CrossVersionGradleLibrary.CORRESPONDING_KOTLIN_PLUGIN
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.dsl.DependencyHandler

@Extension
class DependencyHandlerCorrespondingKotlinGradlePluginExtension(
    private val dependencies: DependencyHandler
) {

    fun correspondingKotlinGradlePlugin(): Dependency {
        return CORRESPONDING_KOTLIN_PLUGIN.dependencyFactory(dependencies)!!
    }

}
