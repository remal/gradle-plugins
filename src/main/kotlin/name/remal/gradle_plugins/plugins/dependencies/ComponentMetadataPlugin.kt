package name.remal.gradle_plugins.plugins.dependencies

import name.remal.Services.loadImplementationClasses
import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.GradleEnumVersion.GRADLE_VERSION_4_10
import name.remal.gradle_plugins.dsl.MinGradleVersion
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.utils.wrapWithInjectedConstructorParams
import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.DependencyHandler

@Plugin(
    id = "name.remal.component-metadata",
    description = "Plugin that configures some default component metadata.",
    tags = ["component-metadata"]
)
@MinGradleVersion(GRADLE_VERSION_4_10)
@ApplyPluginClasses(ComponentCapabilitiesPlugin::class)
class ComponentMetadataPlugin : BaseReflectiveProjectPlugin() {

    companion object {
        private val metadataClasses: List<Class<AbstractComponentMetadata>> by lazy {
            loadImplementationClasses(AbstractComponentMetadata::class.java).toList()
        }
    }

    @PluginAction(isHidden = true)
    protected fun DependencyHandler.registerComponentMetadata(project: Project) {
        metadataClasses.forEach { metadataClass ->
            val wrappedMetadataClass = wrapWithInjectedConstructorParams(metadataClass, project)
            components.all(wrappedMetadataClass)
        }
    }

}
