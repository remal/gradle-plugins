package name.remal.gradle_plugins.plugins.dependencies

import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.extensions.addPlugin
import name.remal.gradle_plugins.dsl.extensions.convention
import org.gradle.api.artifacts.ConfigurationContainer

@Plugin(
    id = "name.remal.configuration-extensions",
    description = "Plugin that adds some useful extensions to all configurations.",
    tags = ["configuration"]
)
class ConfigurationExtensionsPlugin : BaseReflectiveProjectPlugin() {

    @PluginAction
    fun ConfigurationContainer.addExtensions() {
        all { conf ->
            conf.convention.addPlugin(ConfigurationExtension(conf))
        }
    }

}
