package name.remal.gradle_plugins.plugins.gradle_plugins

import name.remal.concurrentMapOf
import name.remal.createParentDirectories
import name.remal.debug
import name.remal.findMethod
import name.remal.gradle_plugins.api.BuildTimeConstants.getStringProperty
import name.remal.gradle_plugins.dsl.Extension
import name.remal.gradle_plugins.dsl.GradleEnumVersion.GRADLE_VERSION_5_0
import name.remal.gradle_plugins.dsl.extensions.cacheDynamicForever
import name.remal.gradle_plugins.dsl.extensions.compareTo
import name.remal.gradle_plugins.dsl.extensions.createFromNotation
import name.remal.gradle_plugins.dsl.extensions.excludeModule
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.includeModule
import name.remal.gradle_plugins.dsl.extensions.invokeForInstance
import name.remal.gradle_plugins.dsl.extensions.maven
import name.remal.gradle_plugins.dsl.extensions.minorVersion
import name.remal.gradle_plugins.dsl.extensions.notation
import name.remal.gradle_plugins.dsl.extensions.releasesOnly
import name.remal.gradle_plugins.dsl.extensions.snapshotsOnly
import name.remal.gradle_plugins.dsl.extensions.unwrapProviders
import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.dsl.utils.getGradleLogger
import name.remal.gradle_plugins.plugins.dependencies.DependencyVersionsExtension
import name.remal.gradle_plugins.plugins.dependencies.resolveAllVersions
import name.remal.gradle_plugins.plugins.dependencies.withoutFilters
import name.remal.nullIfEmpty
import org.gradle.api.Project
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.ExternalModuleDependency
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.artifacts.repositories.MavenArtifactRepository
import org.gradle.api.file.FileCollection
import org.gradle.util.GradleVersion
import java.io.File
import java.lang.Math.max
import java.nio.charset.StandardCharsets.UTF_8
import java.time.Duration
import java.util.concurrent.Callable
import java.util.concurrent.ConcurrentMap
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit.MILLISECONDS
import java.util.concurrent.TimeUnit.SECONDS
import java.util.concurrent.atomic.AtomicReference

@Extension
class CrossGradleVersionsChecksExtension(
    private val dependencies: DependencyHandler,
    private val configurations: ConfigurationContainer,
    private val repositories: RepositoryHandler,
    private val project: Project
) {

    companion object {
        private val logger = getGradleLogger(CrossGradleVersionsChecksExtension::class.java)
        private val libraryMarkerClasspathItemsCache: ConcurrentMap<CrossVersionGradleLibrary, Collection<ClasspathItem>> = concurrentMapOf()
        private val notationFilesCache: ConcurrentMap<DependencyNotation, Collection<File>> = concurrentMapOf()
        private val libraryVersionsCache: ConcurrentMap<CrossVersionGradleLibrary, Collection<GradleVersion>> = concurrentMapOf()

        private val resolutionThreadPool = ThreadPoolExecutor(0, max(16, 4 * Runtime.getRuntime().availableProcessors()), 1, SECONDS, LinkedBlockingQueue<Runnable>())
        private val resolutionTimeout = Duration.ofMinutes(5)
    }


    private val cacheRootDir = project.rootProject.buildDir.resolve("cross-gradle-versions")


    private val withLenientStateFunc: ((runnable: Runnable) -> Unit)? = run {
        val serviceRegistry: Any = project.javaClass.findMethod("getServices")?.invokeForInstance(project) ?: return@run null

        val projectStateRegistryClass: Class<*> = try {
            Class.forName("org.gradle.api.internal.project.ProjectStateRegistry", false, CrossGradleVersionsChecksExtension::class.java.classLoader)
        } catch (ignored: Throwable) {
            return@run null
        }

        val projectStateRegistry: Any = serviceRegistry.javaClass.findMethod("get", Class::class.java)?.invokeForInstance(serviceRegistry, projectStateRegistryClass) ?: return@run null

        val withLenientStateMethod = projectStateRegistry.javaClass.findMethod("withLenientState", Runnable::class.java) ?: return@run null
        val func: (Runnable) -> Unit = { runnable ->
            withLenientStateMethod.invokeForInstance(projectStateRegistry, runnable)
        }
        return@run func
    }

    private fun <T> withLenientState(action: () -> T): T {
        val withLenientStateFunc = this.withLenientStateFunc
        if (withLenientStateFunc != null) {
            val reference = AtomicReference<T>()
            withLenientStateFunc(Runnable {
                val result = action()
                reference.set(result)
            })
            return reference.get()

        } else {
            return action()
        }
    }


    private val dependencyVersions = project[DependencyVersionsExtension::class.java]


    val useSnapshotVersions: Boolean = run {
        val property: String = project.findProperty("cross-gradle-versions.use-snapshot-versions").unwrapProviders()?.toString().nullIfEmpty()
            ?: getStringProperty("cross-gradle-versions.use-snapshot-versions")
        return@run property.toBoolean()
    }

    val repositoryUrl: String = project.findProperty("cross-gradle-versions.repository").unwrapProviders()?.toString().nullIfEmpty()
        ?: getStringProperty("cross-gradle-versions.repository")

    private val repository = repositories.maven("Cross Gradle versions", repositoryUrl) { repo ->
        CrossVersionGradleLibrary.values().forEach { lib ->
            repo.includeModule(lib.notation)
        }
        if (useSnapshotVersions) {
            repo.snapshotsOnly()
        } else {
            repo.releasesOnly()
        }
    }

    init {
        repositories.matching { it != repository }.withType(MavenArtifactRepository::class.java) { repo ->
            CrossVersionGradleLibrary.values().forEach { lib ->
                try {
                    repo.excludeModule(lib.notation)
                } catch (e: Throwable) {
                    logger.debug(e)
                }
            }
        }
    }

    var minGradleVersion: String? = getMinGradleVersion(project) ?: GradleVersion.current().version
        set(value) {
            field = value?.let(GradleVersion::version)?.version
        }


    var maxGradleVersion: String? = getMaxGradleVersion(project)
        set(value) {
            field = value?.let(GradleVersion::version)?.version
        }


    var onlyLatestPatchVersions: Boolean = true


    val availableVersions: List<GradleVersion>
        get() {
            val minGradleVersion = minGradleVersion?.let(GradleVersion::version)
            val maxGradleVersion = maxGradleVersion?.let(GradleVersion::version)
            val versions = CrossVersionGradleLibrary.values().asSequence()
                .flatMap { getLibraryVersions(it).asSequence() }
                .filter(GradleVersion::isValid)
                .distinct()
                .filter { it != GradleVersion.current() }
                .filter { minGradleVersion == null || minGradleVersion <= it }
                .filter { maxGradleVersion == null || it <= maxGradleVersion }
                .let {
                    if (onlyLatestPatchVersions) {
                        it.groupBy(GradleVersion::minorVersion)
                            .values.asSequence()
                            .mapNotNull { it.maxOrNull() }
                    } else {
                        it
                    }
                }
                .sorted()
                .toList()

            val number = "gradle.cross-versions.number".let { prop ->
                project.findProperty(prop).unwrapProviders()?.toString()?.toIntOrNull()
                    ?.also { logger.info("{} = {}", prop, it) }
            }
            val total = "gradle.cross-versions.total".let { prop ->
                project.findProperty(prop).unwrapProviders()?.toString()?.toIntOrNull()
                    ?.also { logger.info("{} = {}", prop, it) }
            }
            if (number != null && total != null) {
                val chunk = versions.size.toDouble() / total
                val revertNumber = total - number + 1
                val fromIndex = (chunk * (revertNumber - 1) + 0.000000001).toInt()
                val toIndex = (chunk * revertNumber + 0.000000001).toInt()
                return versions.subList(fromIndex, toIndex)
            }

            return versions
        }

    @JvmOverloads
    fun getClasspathItems(classpathFiles: Iterable<File>, version: GradleVersion = GradleVersion.current()): List<ClasspathItem> {
        if (!classpathFiles.iterator().hasNext()) return emptyList()
        val result: MutableList<ClasspathItem> = classpathFiles.asSequence()
            .distinct()
            .map(::FileClasspathItem)
            .toMutableList()

        CrossVersionGradleLibrary.values().asSequence()
            .filter { version in getLibraryVersions(it) }
            .map { it to getLibraryMarkerClasspathItems(it) }
            .filter { it.second.isNotEmpty() }
            .sortedByDescending { it.second.size }
            .forEach forEachLibrary@{ (lib, classpathItems) ->
                val firstFileIndex = classpathItems.asSequence()
                    .map { result.indexOf(it) }
                    .filter { 0 <= it }
                    .minOrNull() ?: return@forEachLibrary

                result.removeAll(classpathItems)

                val newClasspathItem = NotationClasspathItem(lib.notation.withVersion(version.version))
                result.add(firstFileIndex, newClasspathItem)
            }

        return result
    }

    fun replaceClasspathItems(fileCollection: FileCollection, version: GradleVersion): FileCollection {
        return project.files(project.provider<Collection<File>> provider@{
            val classpathItems = getClasspathItems(fileCollection, version)

            if (GradleVersion.current() < GRADLE_VERSION_5_0 || withLenientStateFunc != null) {
                val resolutionsFutures = classpathItems.map { classpathItem ->
                    resolutionThreadPool.submit(Callable<Collection<File>> {
                        withLenientState {
                            when (classpathItem) {
                                is FileClasspathItem -> listOf(classpathItem.file)
                                is NotationClasspathItem -> getNotationFiles(classpathItem.notation)
                            }
                        }
                    })
                }
                val resolvedFiles = resolutionsFutures.flatMap {
                    it.get(resolutionTimeout.toMillis(), MILLISECONDS)
                }
                return@provider resolvedFiles

            } else {
                return@provider classpathItems.asSequence()
                    .flatMap { classpathItem ->
                        when (classpathItem) {
                            is FileClasspathItem -> sequenceOf(classpathItem.file)
                            is NotationClasspathItem -> getNotationFiles(classpathItem.notation).asSequence()
                        }
                    }
                    .toList()
            }
        })
    }


    private fun getLibraryVersions(library: CrossVersionGradleLibrary): Collection<GradleVersion> {
        return libraryVersionsCache.computeIfAbsent(library) { lib ->
            val notation = lib.notation.withoutVersion()
            val cacheFile = cacheRootDir
                .resolve(notation.toString()
                    .splitToSequence(':')
                    .map { it.trim('.') }
                    .filter(String::isNotBlank)
                    .joinToString("/")
                )
                .resolve(
                    if (useSnapshotVersions) {
                        "snapshots.txt"
                    } else {
                        "releases.txt"
                    }
                )
            if (cacheFile.isFile) {
                logger.debug("Reading cached versions of {} from cache file {}", notation, cacheFile)
                val cachedVersions = cacheFile.readText(UTF_8).splitToSequence('\n')
                    .map(String::trim)
                    .filter(String::isNotEmpty)
                    .map(GradleVersion::version)
                    .toList()
                logger.debug("Cached versions of {}: ", notation, cachedVersions)
                return@computeIfAbsent processLibraryVersions(cachedVersions)
            }

            val versions = dependencyVersions.withoutFilters {
                resolveAllVersions(notation).asSequence()
                    .filter {
                        if (useSnapshotVersions) {
                            return@filter it.endsWith("-SNAPSHOT")
                        } else {
                            return@filter !it.endsWith("-SNAPSHOT")
                        }
                    }
                    .mapNotNull {
                        try {
                            GradleVersion.version(it)
                        } catch (e: Exception) {
                            logger.debug(e)
                            null
                        }
                    }
                    .filter(GradleVersion::isValid)
                    .distinct()
                    .sorted()
                    .toList()
            }

            cacheFile.createParentDirectories().writeText(
                versions.asSequence()
                    .map(GradleVersion::getVersion)
                    .joinToString("\n"),
                UTF_8
            )

            return@computeIfAbsent processLibraryVersions(versions)
        }
    }

    private fun processLibraryVersions(versions: Collection<GradleVersion>): Collection<GradleVersion> {
        return versions.asSequence()
            .map {
                if (it.isSnapshot) {
                    return@map GradleVersion.version(it.version.substringBefore("-SNAPSHOT"))
                } else {
                    return@map it
                }
            }
            .distinct()
            .toList()
    }


    private fun getLibraryMarkerClasspathItems(library: CrossVersionGradleLibrary): Collection<ClasspathItem> {
        return libraryMarkerClasspathItemsCache.computeIfAbsent(library, compute@{
            val dependency = it.dependencyFactory(dependencies) ?: return@compute emptyList()
            return@compute resolveDependency(dependency).map(::FileClasspathItem)
        })
    }

    private fun getNotationFiles(dependencyNotation: DependencyNotation): Collection<File> {
        return notationFilesCache.computeIfAbsent(dependencyNotation) compute@{ notation ->
            val dependency = dependencies.createFromNotation(notation)
            return@compute resolveDependency(dependency)
        }
    }

    private fun resolveDependency(dependency: Dependency): Set<File> {
        dependencyVersions.withoutFilters {
            if (dependency is ExternalModuleDependency) {
                val configuration = configurations.detachedConfiguration()
                    .cacheDynamicForever()

                if (useSnapshotVersions) {
                    val notation = dependency.notation
                    val snapshotNotation = notation.withSnapshotVersion()
                    val snapshotDependency = dependencies.createFromNotation(snapshotNotation)
                    configuration.dependencies.add(snapshotDependency)
                    return configuration.files

                } else {
                    configuration.dependencies.add(dependency)
                    return configuration.files
                }

            } else {
                return configurations.detachedConfiguration(dependency).files
            }
        }
    }


    private fun getMinGradleVersion(project: Project) = getGradleVersion(project, "gradle.min-version")
    private fun getMaxGradleVersion(project: Project) = getGradleVersion(project, "gradle.max-version")

    private fun getGradleVersion(project: Project, propName: String): String? {
        val propValue = project.findProperty(propName).unwrapProviders()?.toString().nullIfEmpty() ?: return null
        try {
            GradleVersion.version(propValue)
        } catch (ignored: Exception) {
            logger.warn("Value of property '{}' is not a valid Gradle version: {}", propName, propValue)
            return null
        }
        return propValue
    }

}
