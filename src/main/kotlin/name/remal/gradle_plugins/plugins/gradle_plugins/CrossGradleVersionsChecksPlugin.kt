package name.remal.gradle_plugins.plugins.gradle_plugins

import name.remal.default
import name.remal.gradle_plugins.dsl.ApplyPluginClasses
import name.remal.gradle_plugins.dsl.ApplyPlugins
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin
import name.remal.gradle_plugins.dsl.CreateExtensionsPluginAction
import name.remal.gradle_plugins.dsl.Plugin
import name.remal.gradle_plugins.dsl.PluginAction
import name.remal.gradle_plugins.dsl.extensions.get
import name.remal.gradle_plugins.dsl.extensions.setupTasksAfterEvaluateOrNow
import name.remal.gradle_plugins.plugins.common.CommonSettingsPlugin
import name.remal.gradle_plugins.plugins.dependencies.DependencyVersionsPlugin
import name.remal.gradle_plugins.plugins.java.JavaPluginId
import name.remal.nullIfEmpty
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.artifacts.ConfigurationContainer
import org.gradle.api.artifacts.dsl.DependencyHandler
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.plugins.JavaBasePlugin.CHECK_TASK_NAME
import org.gradle.api.plugins.JavaBasePlugin.VERIFICATION_GROUP
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.testing.Test
import org.gradle.util.GradleVersion

@Plugin(
    id = "name.remal.cross-gradle-versions-checks",
    description = "Plugin that allows to check plugins against different Gradle versions.",
    tags = ["gradle", "plugins"]
)
@ApplyPlugins(JavaPluginId::class)
@ApplyPluginClasses(CommonSettingsPlugin::class, DependencyVersionsPlugin::class)
class CrossGradleVersionsChecksPlugin : BaseReflectiveProjectPlugin() {

    companion object {
        private val versionPattern = Regex("\\d+(?:\\.\\d+)*")
        private val versionEscaperStart1 = Regex("^(\\d)(\\.|$)")
        private val versionEscaper2 = Regex("\\.(\\d{2,})")
        private val versionEscaper1 = Regex("\\.(\\d)")
        private val genericVersionEscaper = Regex("\\W")
    }

    @CreateExtensionsPluginAction
    fun ExtensionContainer.`Create 'crossGradleVersionsChecks' extension`(dependencies: DependencyHandler, configurations: ConfigurationContainer, repositories: RepositoryHandler, project: Project) {
        create("crossGradleVersionsChecks", CrossGradleVersionsChecksExtension::class.java, dependencies, configurations, repositories, project)
    }

    @PluginAction(order = -1000)
    fun TaskContainer.`Create 'crossGradleVersionsChecks' tasks`() {
        create("crossGradleVersionsCheck") {
            it.group = VERIFICATION_GROUP
            it.dependsOn(CHECK_TASK_NAME)
        }
    }

    @PluginAction(isHidden = true)
    fun TaskContainer.`Clone tasks`(extensions: ExtensionContainer, project: Project) {
        val crossGradleVersionsChecksExtension = extensions[CrossGradleVersionsChecksExtension::class.java]
        val crossGradleVersionsChecks = getByName("crossGradleVersionsCheck")

        project.setupTasksAfterEvaluateOrNow { _ ->
            toList().forEach forEachTask@{ task ->
                val groupTask: Task by lazy {
                    create("crossGradleVersions" + task.name.capitalize()) { group ->
                        group.dependsOn(task)
                        crossGradleVersionsChecks.dependsOn(group)

                        group.group = task.group
                        group.description = (task.description.nullIfEmpty().default(task.name) + ": Run cross Gradle version tasks").trimStart(':', ' ')
                    }
                }

                if (task is Test) {
                    crossGradleVersionsChecksExtension.availableVersions.forEach { gradleVersion ->
                        create(task.name + '-' + escapeVersion(gradleVersion), Test::class.java) { clone ->
                            clone.dependsOn(task.taskDependencies)
                            clone.shouldRunAfter(task)
                            groupTask.dependsOn(clone)

                            clone.classpath = crossGradleVersionsChecksExtension.replaceClasspathItems(task.classpath, gradleVersion)

                            clone.group = task.group
                            clone.description = (task.description.nullIfEmpty().default(task.name).trim('.') + ": $gradleVersion").trimStart(':', ' ')
                            clone.testLogging.events = task.testLogging.events
                            clone.testLogging.minGranularity = task.testLogging.minGranularity
                            clone.testLogging.maxGranularity = task.testLogging.maxGranularity
                            clone.testLogging.displayGranularity = task.testLogging.displayGranularity
                            clone.testLogging.showExceptions = task.testLogging.showExceptions
                            clone.testLogging.showCauses = task.testLogging.showCauses
                            clone.testLogging.showStackTraces = task.testLogging.showStackTraces
                            clone.testLogging.exceptionFormat = task.testLogging.exceptionFormat
                            clone.testLogging.stackTraceFilters = task.testLogging.stackTraceFilters
                            clone.testLogging.showStandardStreams = task.testLogging.showStandardStreams
                            clone.testLogging.debug = task.testLogging.debug
                            clone.testLogging.info = task.testLogging.info
                            clone.testLogging.lifecycle = task.testLogging.lifecycle
                            clone.testLogging.warn = task.testLogging.warn
                            clone.testLogging.quiet = task.testLogging.quiet
                            clone.testLogging.error = task.testLogging.error

                            clone.testClassesDirs = task.testClassesDirs
                            clone.maxParallelForks = task.maxParallelForks

                            clone.systemProperties = task.systemProperties
                            clone.defaultCharacterEncoding = task.defaultCharacterEncoding
                            clone.minHeapSize = task.minHeapSize
                            clone.maxHeapSize = task.maxHeapSize
                            clone.enableAssertions = task.enableAssertions
                            clone.debug = task.debug

                            clone.environment = task.environment
                        }
                    }
                }
            }
        }
    }


    private fun escapeVersion(gradleVersion: GradleVersion) = escapeVersion(gradleVersion.version)

    private fun escapeVersion(version: String): String {
        if (versionPattern.matches(version)) {
            var result = version
            result = versionEscaperStart1.replace(result, "-$1$2")
            result = versionEscaper2.replace(result, "-$1")
            result = versionEscaper1.replace(result, "--$1")
            return result

        } else {
            return genericVersionEscaper.replace(version, "-")
        }
    }

}
