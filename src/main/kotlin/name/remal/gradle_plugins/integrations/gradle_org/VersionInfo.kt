package name.remal.gradle_plugins.integrations.gradle_org

data class VersionInfo(
    val version: String,
    val current: Boolean,
    val snapshot: Boolean,
    val nightly: Boolean,
    val activeRc: Boolean,
    val rcFor: String?,
    val milestoneFor: String?,
    val broken: Boolean,
    val downloadUrl: String
) {

    val isRelease: Boolean
        get() {
            if (snapshot) return false
            if (nightly) return false
            if (activeRc) return false
            if (!rcFor.isNullOrEmpty()) return false
            if (!milestoneFor.isNullOrEmpty()) return false
            if (broken) return false
            return true
        }

}
