package name.remal.gradle_plugins.dsl.extensions

import name.remal.findCompatibleMethod
import name.remal.uncheckedCast
import org.gradle.api.provider.Property
import org.gradle.api.tasks.bundling.AbstractArchiveTask
import java.io.File

var AbstractArchiveTask.archiveNameCompatible: String
    get() {
        this.javaClass.findCompatibleMethod(Property::class.java, "getArchiveFileName")?.let { method ->
            val property = method.invoke(this).uncheckedCast<Property<Any>>()
            return property.get().toString()
        }

        this.javaClass.findCompatibleMethod(String::class.java, "getArchiveName")?.let { method ->
            return method.invoke(this).uncheckedCast()
        }

        throw IllegalStateException("Getter can't be found")
    }
    set(value) {
        this.javaClass.findCompatibleMethod(Property::class.java, "getArchiveFileName")?.let { method ->
            val property = method.invoke(this).uncheckedCast<Property<Any>>()
            property.set(value)
            return
        }

        this.javaClass.findCompatibleMethod("setArchiveName", String::class.java)?.let { method ->
            method.invoke(this, value)
            return
        }

        throw IllegalStateException("Setter can't be found")
    }


var AbstractArchiveTask.baseNameCompatible: String
    get() {
        this.javaClass.findCompatibleMethod(Property::class.java, "getArchiveBaseName")?.let { method ->
            val property = method.invoke(this).uncheckedCast<Property<Any>>()
            return property.get().toString()
        }

        this.javaClass.findCompatibleMethod(String::class.java, "getBaseName")?.let { method ->
            return method.invoke(this).uncheckedCast()
        }

        throw IllegalStateException("Getter can't be found")
    }
    set(value) {
        this.javaClass.findCompatibleMethod(Property::class.java, "getArchiveBaseName")?.let { method ->
            val property = method.invoke(this).uncheckedCast<Property<Any>>()
            property.set(value)
            return
        }

        this.javaClass.findCompatibleMethod("setBaseName", String::class.java)?.let { method ->
            method.invoke(this, value)
            return
        }

        throw IllegalStateException("Setter can't be found")
    }


var AbstractArchiveTask.destinationDirCompatible: File
    get() {
        this.javaClass.findCompatibleMethod(Property::class.java, "getDestinationDirectory")?.let { method ->
            val property = method.invoke(this).uncheckedCast<Property<Any>>()
            return property.getAsFileSmart()
        }

        this.javaClass.findCompatibleMethod(File::class.java, "getDestinationDir")?.let { method ->
            method.isAccessible = true
            return method.invoke(this).uncheckedCast()
        }

        throw IllegalStateException("Getter can't be found")
    }
    set(value) {
        this.javaClass.findCompatibleMethod(Property::class.java, "getDestinationDirectory")?.let { method ->
            val property = method.invoke(this).uncheckedCast<Property<Any>>()
            val setter = property.javaClass.findCompatibleMethod("set", File::class.java)
            if (setter != null) {
                setter.isAccessible = true
                setter.invoke(property, value)
                return
            }
        }

        this.javaClass.findCompatibleMethod("setDestinationDir", File::class.java)?.let { method ->
            method.invoke(this, value)
            return
        }

        throw IllegalStateException("Setter can't be found")
    }


val AbstractArchiveTask.archivePathCompatible: File
    get() {
        return this.archiveFile.get().asFile
    }


var AbstractArchiveTask.extensionCompatible: String?
    get() {
        this.javaClass.findCompatibleMethod(Property::class.java, "getArchiveExtension")?.let { method ->
            val property = method.invoke(this).uncheckedCast<Property<Any>>()
            return property.orNull?.toString()
        }

        this.javaClass.findCompatibleMethod(String::class.java, "getExtension")?.let { method ->
            return method.invoke(this).uncheckedCast()
        }

        throw IllegalStateException("Getter can't be found")
    }
    set(value) {
        this.javaClass.findCompatibleMethod(Property::class.java, "getArchiveExtension")?.let { method ->
            val property = method.invoke(this).uncheckedCast<Property<Any>>()
            property.set(value)
            return
        }

        this.javaClass.findCompatibleMethod("setExtension", String::class.java)?.let { method ->
            method.invoke(this, value)
            return
        }

        throw IllegalStateException("Setter can't be found")
    }


var AbstractArchiveTask.classifierCompatible: String?
    get() {
        this.javaClass.findCompatibleMethod(Property::class.java, "getArchiveClassifier")?.let { method ->
            val property = method.invoke(this).uncheckedCast<Property<Any>>()
            return property.orNull?.toString()
        }

        this.javaClass.findCompatibleMethod(String::class.java, "getClassifier")?.let { method ->
            return method.invoke(this).uncheckedCast()
        }

        throw IllegalStateException("Getter can't be found")
    }
    set(value) {
        this.javaClass.findCompatibleMethod(Property::class.java, "getArchiveClassifier")?.let { method ->
            val property = method.invoke(this).uncheckedCast<Property<Any>>()
            property.set(value)
            return
        }

        this.javaClass.findCompatibleMethod("setClassifier", String::class.java)?.let { method ->
            method.invoke(this, value)
            return
        }

        throw IllegalStateException("Setter can't be found")
    }


private fun Property<*>.getAsFileSmart(): File {
    val propertyValue = get()
    if (propertyValue is File) {
        return propertyValue
    }

    val getAsFile = propertyValue.javaClass.findCompatibleMethod(File::class.java, "getAsFile")
    if (getAsFile != null) {
        getAsFile.isAccessible = true
        val fileValue = getAsFile.invoke(propertyValue)
        if (fileValue is File) {
            return fileValue
        } else if (fileValue is Property<*>) {
            return fileValue.getAsFileSmart()
        }
    }

    throw UnsupportedOperationException("Not a file value: $propertyValue")
}
