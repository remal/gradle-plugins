package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.ResolvedConfiguration
import org.gradle.api.artifacts.ResolvedDependency

val ResolvedConfiguration.moduleDependencies: Set<ResolvedDependency>
    get() = mutableSetOf<ResolvedDependency>().apply {
        firstLevelModuleDependencies.forEach {
            if (add(it)) {
                addAll(it.allChildren)
            }
        }
    }


fun ResolvedConfiguration.getModuleDependencies(dependencySpec: (dependency: Dependency) -> Boolean): Set<ResolvedDependency> {
    return mutableSetOf<ResolvedDependency>().apply {
        getFirstLevelModuleDependencies(dependencySpec).forEach {
            if (add(it)) {
                addAll(it.allChildren)
            }
        }
    }
}

fun ResolvedConfiguration.getFirstLevelModuleDependencies(dependency: Dependency): Set<ResolvedDependency> = getFirstLevelModuleDependencies { it == dependency }
fun ResolvedConfiguration.getModuleDependencies(dependency: Dependency) = getModuleDependencies { it == dependency }
