package name.remal.gradle_plugins.dsl.utils.code_quality

import name.remal.gradle_plugins.dsl.utils.canonizeText
import name.remal.gradle_plugins.dsl.utils.htmlToText
import name.remal.nullIfEmpty
import java.io.File
import java.lang.Math.max
import java.lang.Math.min
import java.time.Instant
import java.util.Comparator.naturalOrder
import java.util.Comparator.nullsLast

@DslMarker
private annotation class FindBugsReportDslMarker

@FindBugsReportDslMarker
data class FindBugsReport(
    var tool: String? = null,
    var version: String? = null,
    var analysisTimestamp: Long? = null,
    var project: FindBugsProject? = null,
    val bugs: MutableList<FindBugsBug> = mutableListOf(),
    val types: MutableMap<String, FindBugsType> = sortedMapOf(),
    val categories: MutableMap<String, FindBugsCategory> = sortedMapOf()
) {

    val sortedBugs: List<FindBugsBug> get() = bugs.sorted()


    fun tool(value: String?) {
        tool = value.nullIfEmpty()
    }


    fun version(value: String?) {
        version = value.nullIfEmpty()
    }


    fun analysisTimestamp(value: Long?) {
        analysisTimestamp = value
    }

    fun analysisTimestamp(value: Instant?) = analysisTimestamp(value?.toEpochMilli())


    fun project(init: FindBugsProject.() -> Unit) {
        project = FindBugsProject().apply(init)
    }


    fun bug(init: FindBugsBug.() -> Unit) {
        bugs.add(FindBugsBug().apply(init))
    }


    fun type(type: String, init: FindBugsType.() -> Unit) {
        types[type] = FindBugsType().apply(init)
    }


    fun category(type: String, init: FindBugsCategory.() -> Unit) {
        categories[type] = FindBugsCategory().apply(init)
    }

}

@FindBugsReportDslMarker
data class FindBugsProject(
    var name: String? = null,
    val srcDirs: MutableSet<String> = mutableSetOf()
) {

    fun name(value: String?) {
        name = value.nullIfEmpty()
    }


    fun srcDir(value: File) {
        srcDirs.add(value.absolutePath)
    }

    fun srcDirs(vararg values: File) {
        values.forEach(::srcDir)
    }

    fun srcDir(value: String) {
        srcDirs.add(value)
    }

    fun srcDirs(vararg values: String) {
        srcDirs.addAll(values)
    }

    fun srcDirs(values: Iterable<String>) {
        srcDirs.addAll(values)
    }

}

@FindBugsReportDslMarker
data class FindBugsBug(
    var category: String? = null,
    var type: String? = null,
    var rank: Int? = null,
    var confidence: Int? = null,
    var message: String? = null,
    var shortMessage: String? = null,
    var location: FindBugsLocation? = null
) : Comparable<FindBugsBug> {

    fun category(value: String?) {
        category = value.nullIfEmpty()
    }


    fun type(value: String?) {
        type = value.nullIfEmpty()
    }


    fun rank(value: Int?) {
        rank = value
    }


    private fun rank(min: Int, max: Int, increment: Int) {
        rank = max(min, min(min + increment, max))
    }

    fun rankScariest(increment: Int = 0) = rank(1, 4, increment)

    fun rankScary(increment: Int = 0) = rank(5, 9, increment)

    fun rankTroubling(increment: Int = 0) = rank(10, 14, increment)

    fun rankOfConcern(increment: Int = 0) = rank(15, 20, increment)


    fun rankBlocker() = rankScariest(0)

    fun rankCritical() = rankScariest(2)

    fun rankMajor() = rankScary(0)

    fun rankMinor() = rankTroubling(0)

    fun rankInfo() = rankOfConcern(0)


    fun confidence(value: Int?) {
        confidence = value
    }


    fun message(value: String?) {
        message = canonizeText(value)
    }


    fun shortMessage(value: String?) {
        shortMessage = canonizeText(value)
    }


    fun location(init: FindBugsLocation.() -> Unit) {
        location = FindBugsLocation().apply(init)
    }


    @Suppress("ComplexMethod")
    override fun compareTo(other: FindBugsBug): Int {
        nullsLast(naturalOrder<Int>()).compare(rank, other.rank).let { if (it != 0) return it }
        nullsLast(naturalOrder<Int>()).compare(confidence, other.confidence).let { if (it != 0) return it }
        nullsLast(naturalOrder<String>()).compare(location?.className, other.location?.className).let { if (it != 0) return it }
        nullsLast(naturalOrder<String>()).compare(location?.sourceFile, other.location?.sourceFile).let { if (it != 0) return it }
        nullsLast(naturalOrder<Int>()).compare(location?.startLine, other.location?.startLine).let { if (it != 0) return it }
        nullsLast(naturalOrder<Int>()).compare(location?.startLineOffset, other.location?.startLineOffset).let { if (it != 0) return it }
        nullsLast(reverseOrder<Int>()).compare(location?.endLine, other.location?.endLine).let { if (it != 0) return it }
        nullsLast(reverseOrder<Int>()).compare(location?.endLineOffset, other.location?.endLineOffset).let { if (it != 0) return it }
        nullsLast(naturalOrder<String>()).compare(type, other.type).let { if (it != 0) return it }
        return 0
    }

}

@FindBugsReportDslMarker
data class FindBugsLocation(
    var className: String? = null,
    var sourceFile: String? = null,
    var startLine: Int? = null,
    var startLineOffset: Int? = null,
    var endLine: Int? = null,
    var endLineOffset: Int? = null
) {

    fun className(value: String?) {
        className = value.nullIfEmpty()
    }


    fun sourceFile(value: String?) {
        sourceFile = value.nullIfEmpty()
    }

    fun sourceFile(value: File?) {
        sourceFile = value?.path
    }


    fun startLine(value: Int?) {
        startLine = value
    }


    fun startLineOffset(value: Int?) {
        startLineOffset = value
    }


    fun endLine(value: Int?) {
        endLine = value
    }


    fun endLineOffset(value: Int?) {
        endLineOffset = value
    }

}

@FindBugsReportDslMarker
data class FindBugsType(
    override var textDescription: String? = null,
    override var htmlDescription: String? = null
) : FindBugsWithDescription

@FindBugsReportDslMarker
data class FindBugsCategory(
    override var textDescription: String? = null,
    override var htmlDescription: String? = null
) : FindBugsWithDescription

@FindBugsReportDslMarker
interface FindBugsWithDescription {

    var textDescription: String?

    var htmlDescription: String?

    fun textDescription(value: String?) {
        if (value != null) {
            canonizeText(value).let {
                textDescription = it
                htmlDescription = it?.replace("\n", "<br>\n")
            }

        } else {
            textDescription = null
            htmlDescription = null
        }
    }

    fun htmlDescription(value: String?) {
        if (value != null) {
            canonizeText(value).let {
                textDescription = it?.let { htmlToText(it) }
                htmlDescription = it
            }

        } else {
            textDescription = null
            htmlDescription = null
        }
    }

}
