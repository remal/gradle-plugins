package name.remal.gradle_plugins.dsl.extensions

import name.remal.ASM_API
import name.remal.concurrentMapOf
import name.remal.getCompatibleMethod
import name.remal.gradle_plugins.api.BuildTimeConstants.getClassDescriptor
import name.remal.gradle_plugins.api.ExcludeFromCodeCoverage
import name.remal.gradle_plugins.dsl.internal.RelocatedClass
import name.remal.newTempFile
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.file.FileCollection
import org.gradle.testing.jacoco.tasks.JacocoReportBase
import org.objectweb.asm.AnnotationVisitor
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassReader.SKIP_CODE
import org.objectweb.asm.ClassReader.SKIP_DEBUG
import org.objectweb.asm.ClassReader.SKIP_FRAMES
import org.objectweb.asm.ClassVisitor
import java.io.File
import java.lang.reflect.Method

private val jacocoReportBaseGetExecutionDataMethod: Method by lazy {
    JacocoReportBase::class.java.getCompatibleMethod(FileCollection::class.java, "getExecutionData")
}
private val jacocoReportBaseSetExecutionDataMethod: Method by lazy {
    JacocoReportBase::class.java.getCompatibleMethod("setExecutionData", FileCollection::class.java)
}
var JacocoReportBase.executionDataCompatible: FileCollection
    get() = jacocoReportBaseGetExecutionDataMethod.invokeForInstance(this) ?: project.files()
    set(value) {
        val executionData: FileCollection? = jacocoReportBaseGetExecutionDataMethod.invokeForInstance(this)
        if (executionData == value) return
        if (executionData is ConfigurableFileCollection) {
            executionData.setFrom(value as Any)
        } else {
            jacocoReportBaseSetExecutionDataMethod.invokeForInstance(this, value)
        }
    }


private val jacocoReportBaseGetSourceDirectoriesMethod: Method by lazy {
    JacocoReportBase::class.java.getCompatibleMethod(FileCollection::class.java, "getSourceDirectories")
}
private val jacocoReportBaseSetSourceDirectoriesMethod: Method by lazy {
    JacocoReportBase::class.java.getCompatibleMethod("setSourceDirectories", FileCollection::class.java)
}
var JacocoReportBase.sourceDirectoriesCompatible: FileCollection
    get() = jacocoReportBaseGetSourceDirectoriesMethod.invokeForInstance(this) ?: project.files()
    set(value) {
        val sourceDirectories: FileCollection? = jacocoReportBaseGetSourceDirectoriesMethod.invokeForInstance(this)
        if (sourceDirectories == value) return
        if (sourceDirectories is ConfigurableFileCollection) {
            sourceDirectories.setFrom(value as Any)
        } else {
            jacocoReportBaseSetSourceDirectoriesMethod.invokeForInstance(this, value)
        }
    }


private val jacocoReportBaseGetClassDirectoriesMethod: Method by lazy {
    JacocoReportBase::class.java.getCompatibleMethod(FileCollection::class.java, "getClassDirectories")
}
private val jacocoReportBaseSetClassDirectoriesMethod: Method by lazy {
    JacocoReportBase::class.java.getCompatibleMethod("setClassDirectories", FileCollection::class.java)
}
var JacocoReportBase.classDirectoriesCompatible: FileCollection
    get() = jacocoReportBaseGetClassDirectoriesMethod.invokeForInstance(this) ?: project.files()
    set(value) {
        val classDirectories: FileCollection? = jacocoReportBaseGetClassDirectoriesMethod.invokeForInstance(this)
        if (classDirectories == value) return
        if (classDirectories is ConfigurableFileCollection) {
            classDirectories.setFrom(value as Any)
        } else {
            jacocoReportBaseSetClassDirectoriesMethod.invokeForInstance(this, value)
        }
    }


private val jacocoReportBaseGetAdditionalClassDirsMethod: Method by lazy {
    JacocoReportBase::class.java.getCompatibleMethod(FileCollection::class.java, "getAdditionalClassDirs")
}
private val jacocoReportBaseSetAdditionalClassDirsMethod: Method by lazy {
    JacocoReportBase::class.java.getCompatibleMethod("setAdditionalClassDirs", FileCollection::class.java)
}
var JacocoReportBase.additionalClassDirsCompatible: FileCollection
    get() = jacocoReportBaseGetAdditionalClassDirsMethod.invokeForInstance(this) ?: project.files()
    set(value) {
        val additionalClassDirs: FileCollection? = jacocoReportBaseGetAdditionalClassDirsMethod.invokeForInstance(this)
        if (additionalClassDirs == value) return
        if (additionalClassDirs is ConfigurableFileCollection) {
            additionalClassDirs.setFrom(value as Any)
        } else {
            jacocoReportBaseSetAdditionalClassDirsMethod.invokeForInstance(this, value)
        }
    }


private val jacocoReportBaseGetAdditionalSourceDirsMethod: Method by lazy {
    JacocoReportBase::class.java.getCompatibleMethod(FileCollection::class.java, "getAdditionalSourceDirs")
}
private val jacocoReportBaseSetAdditionalSourceDirsMethod: Method by lazy {
    JacocoReportBase::class.java.getCompatibleMethod("setAdditionalSourceDirs", FileCollection::class.java)
}
var JacocoReportBase.additionalSourceDirsCompatible: FileCollection
    get() = jacocoReportBaseGetAdditionalSourceDirsMethod.invokeForInstance(this) ?: project.files()
    set(value) {
        val additionalSourceDirs: FileCollection? = jacocoReportBaseGetAdditionalSourceDirsMethod.invokeForInstance(this)
        if (additionalSourceDirs == value) return
        if (additionalSourceDirs is ConfigurableFileCollection) {
            additionalSourceDirs.setFrom(value as Any)
        } else {
            jacocoReportBaseSetAdditionalSourceDirsMethod.invokeForInstance(this, value)
        }
    }


fun JacocoReportBase.prepareExecutionData() {
    var result = executionDataCompatible
    result = project.files(*result.files.filter(File::exists).toTypedArray())
    if (result.isEmpty) result = project.files(newTempFile("jacoco-", ".exec"))
    executionDataCompatible = result
}


private val excludeFromCodeCoverageDescs = setOf(
    getClassDescriptor(ExcludeFromCodeCoverage::class.java),
    getClassDescriptor(RelocatedClass::class.java)
)

fun JacocoReportBase.applyExcludeFromCodeCoverage() {
    val cache = concurrentMapOf<File, Boolean>()
    fun isIncluded(file: File): Boolean {
        cache[file]?.let { return it }

        val result: Boolean = run compute@{
            if (file.extension != "class" || !file.isFile) return@compute true

            if (file.name != "package-info.class") {
                val packageInfoFile = file.resolveSibling("package-info.class")
                if (!isIncluded(packageInfoFile)) {
                    logDebug("Excluded by package {}: {}", packageInfoFile, file)
                    return@compute false
                }
            }

            var exclusionAnnotationDesc: String? = null
            lateinit var internalName: String
            val outerInternalNames = mutableSetOf<String>()
            ClassReader(file.readBytes()).accept(
                object : ClassVisitor(ASM_API) {
                    override fun visitAnnotation(desc: String?, visible: Boolean): AnnotationVisitor? {
                        if (desc != null && desc in excludeFromCodeCoverageDescs) {
                            exclusionAnnotationDesc = desc
                        }
                        return null
                    }

                    override fun visit(version: Int, access: Int, name: String, signature: String?, superName: String?, interfaces: Array<String>?) {
                        internalName = name
                    }

                    override fun visitInnerClass(name: String, outerName: String?, innerName: String?, access: Int) {
                        outerName?.let(outerInternalNames::add)
                    }

                    override fun visitOuterClass(owner: String, name: String?, descriptor: String?) {
                        outerInternalNames.add(owner)
                    }
                },
                SKIP_DEBUG or SKIP_FRAMES or SKIP_CODE
            )

            if (exclusionAnnotationDesc != null) {
                logDebug("Excluded by annotation {}: {}", exclusionAnnotationDesc, file)
                return@compute false
            }

            outerInternalNames.remove(internalName)
            if (internalName.contains('/')) {
                val internalNamePrefix = internalName.substringBeforeLast('/', "") + '/'
                outerInternalNames.removeIf { !it.startsWith(internalNamePrefix) }
                outerInternalNames.removeIf { it.substring(internalNamePrefix.length).contains('/') }
            }

            if (outerInternalNames.isNotEmpty()) {
                logDebug("{}: outerInternalNames: {}", internalName, outerInternalNames)
                val rootDir = run {
                    var dir = file
                    repeat(internalName.count { it == '/' } + 1) { _ ->
                        dir = dir.parentFile!!
                    }
                    return@run dir
                }
                val excludedOuterInternalName = outerInternalNames.firstOrNull { outerInternalName ->
                    val outerClassFile = rootDir.resolve(outerInternalName + ".class")
                    return@firstOrNull !isIncluded(outerClassFile)
                }
                if (excludedOuterInternalName != null) {
                    logDebug("Excluded by outer class {}: {}", excludedOuterInternalName, file)
                    return@compute false
                }
            }

            return@compute true
        }

        cache[file] = result

        return result
    }

    fun FileCollection.applyExcludeFromCodeCoverage(): FileCollection {
        return project.files(this.files).asFileTree.filter {
            if (isIncluded(it)) {
                return@filter true
            } else {
                logDebug("Exclude {}", it)
                return@filter false
            }
        }
    }

    classDirectoriesCompatible = classDirectoriesCompatible.applyExcludeFromCodeCoverage()
    additionalClassDirsCompatible = additionalClassDirsCompatible.applyExcludeFromCodeCoverage()
}
