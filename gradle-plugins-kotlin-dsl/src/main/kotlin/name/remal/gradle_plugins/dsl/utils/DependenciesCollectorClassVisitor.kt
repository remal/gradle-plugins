package name.remal.gradle_plugins.dsl.utils

import name.remal.uncheckedCast
import org.objectweb.asm.ClassVisitor
import org.objectweb.asm.commons.ClassRemapper
import org.objectweb.asm.commons.Remapper

class DependenciesCollectorClassVisitor(delegate: ClassVisitor?) : ClassRemapper(delegate, DependenciesCollectorRemapper()) {

    val dependencyClassInternalNames: Set<ClassInternalName>
        get() = remapper.uncheckedCast<DependenciesCollectorRemapper>()
            .dependencyClassInternalNames
            .toSortedSet()
            .apply { classInternalName?.let(this::remove) }

    private var classInternalName: ClassInternalName? = null
    override fun visit(version: Int, access: Int, name: ClassInternalName, signature: String?, superName: ClassInternalName?, interfaces: Array<ClassInternalName>?) {
        classInternalName = name
        super.visit(version, access, name, signature, superName, interfaces)
    }


    private class DependenciesCollectorRemapper : Remapper() {
        val dependencyClassInternalNames: MutableSet<ClassInternalName> = hashSetOf()
        override fun map(internalName: ClassInternalName?): ClassInternalName? {
            internalName?.let(dependencyClassInternalNames::add)
            return null
        }
    }

}
