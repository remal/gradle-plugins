package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.Task

val Task.jacocoReportTaskName get() = "jacoco${name.capitalize()}Report"
val Task.jacocoCoverageVerificationTaskName get() = "jacoco${name.capitalize()}CoverageVerification"
