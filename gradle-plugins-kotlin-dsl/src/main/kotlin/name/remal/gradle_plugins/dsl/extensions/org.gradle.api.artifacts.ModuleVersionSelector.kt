package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.dsl.utils.DependencyNotationMatcher
import org.gradle.api.artifacts.ModuleVersionSelector

val ModuleVersionSelector.notation
    get() = DependencyNotation(
        group = group,
        module = name,
        version = version
    )


fun DependencyNotationMatcher.matches(selector: ModuleVersionSelector) = matches(selector.notation)
fun DependencyNotationMatcher.notMatches(selector: ModuleVersionSelector) = !matches(selector)
