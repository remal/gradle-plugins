package name.remal.gradle_plugins.dsl.extensions

import name.remal.findCompatibleMethod
import name.remal.gradle_plugins.dsl.utils.getGradleGroovyVersion
import name.remal.uncheckedCast
import name.remal.version.Version
import org.gradle.api.tasks.compile.GroovyCompileOptions
import java.lang.reflect.Method

private val parametersMinGroovyVersion = Version.create(2, 5)
private val parametersGetter: Method? by lazy {
    GroovyCompileOptions::class.java.findCompatibleMethod(Boolean::class.java, "isParameters")
}
private val parametersSetter: Method? by lazy {
    GroovyCompileOptions::class.java.findCompatibleMethod("setParameters", Boolean::class.java)
}

var GroovyCompileOptions.parameters: Boolean?
    get() {
        if (getGradleGroovyVersion() >= parametersMinGroovyVersion) {
            parametersGetter?.let {
                return it.invokeForInstance(this)
            }
        }
        return null
    }
    set(value) {
        if (getGradleGroovyVersion() >= parametersMinGroovyVersion) {
            parametersSetter?.invokeForInstance<Any?>(this, value)
        }
    }
