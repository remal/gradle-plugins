package name.remal.gradle_plugins.dsl.extensions

import name.remal.fromLowerCamelToLowerHyphen
import org.gradle.api.reporting.Report
import org.gradle.api.reporting.Report.OutputType.FILE

val Report.destinationFileExtension: String
    get() {
        if (FILE != outputType) throw IllegalStateException("$name report has $outputType output type")
        return name.fromLowerCamelToLowerHyphen().substringAfterLast('-').substringAfterLast('_')
    }
