package name.remal.gradle_plugins.dsl.reflective_project_plugin.action_param_injector

import name.remal.gradle_plugins.api.AutoService
import org.gradle.api.Project
import org.gradle.api.resources.ResourceHandler

@AutoService
class ResourceHandlerActionParamInjector : ActionParamInjector<ResourceHandler>() {
    override fun createValue(project: Project): ResourceHandler = project.resources
}
