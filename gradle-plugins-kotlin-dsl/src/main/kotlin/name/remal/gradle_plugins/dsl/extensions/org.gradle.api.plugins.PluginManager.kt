package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.PluginId
import name.remal.gradle_plugins.dsl.ProjectPluginClass
import org.gradle.api.internal.plugins.PluginManagerInternal
import org.gradle.api.plugins.AppliedPlugin
import org.gradle.api.plugins.PluginContainer
import org.gradle.api.plugins.PluginManager
import org.gradle.api.plugins.UnknownPluginException
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger

val PluginManager.pluginContainer: PluginContainer get() = (this as PluginManagerInternal).pluginContainer


fun PluginManager.hasPlugin(pluginId: PluginId): Boolean {
    return hasPlugin(pluginId.id) || pluginId.alternateIds.any(this::hasPlugin)
}

fun PluginManager.hasPlugin(pluginClass: ProjectPluginClass): Boolean {
    return pluginContainer.hasPlugin(pluginClass)
}


fun PluginManager.withOneOfPlugin(ids: Collection<String>, action: (appliedPlugin: AppliedPlugin) -> Unit) {
    if (ids.isEmpty()) throw IllegalArgumentException("empty pluginIds")

    val wasExecuted = AtomicBoolean(false)
    ids.toSet().forEach { id ->
        withPlugin(id) { appliedPlugin ->
            if (wasExecuted.compareAndSet(false, true)) {
                action(appliedPlugin)
            }
        }
    }
}

fun PluginManager.withOneOfPlugin(vararg ids: String, action: (appliedPlugin: AppliedPlugin) -> Unit) = withOneOfPlugin(ids.toList(), action)

fun PluginManager.withOneOfPlugin(vararg ids: PluginId, action: (appliedPlugin: AppliedPlugin) -> Unit) = withOneOfPlugin(ids.flatMap { it.allIds }, action)

fun PluginManager.withPlugin(pluginId: PluginId, action: (appliedPlugin: AppliedPlugin) -> Unit) {
    val ids = setOf(pluginId.id) + pluginId.alternateIds
    if (1 == ids.size) {
        return withPlugin(ids.first(), action)
    } else {
        return withOneOfPlugin(ids, action)
    }
}


fun PluginManager.withPlugins(ids: Collection<String>, action: () -> Unit) {
    if (ids.isEmpty()) return action()

    val notAppliedPlugins = AtomicInteger(ids.size)
    ids.forEach {
        withPlugin(it) {
            if (0 == notAppliedPlugins.decrementAndGet()) {
                action()
            }
        }
    }
}

@JvmName("withPluginIds")
fun PluginManager.withPlugins(pluginIds: Collection<PluginId>, action: () -> Unit) {
    if (pluginIds.isEmpty()) return action()

    val notAppliedPlugins = AtomicInteger(pluginIds.size)
    pluginIds.forEach {
        withPlugin(it) {
            if (notAppliedPlugins.decrementAndGet() == 0) {
                action()
            }
        }
    }
}


fun PluginManager.applyFirstAvailable(ids: Collection<String>) {
    var exception: Exception? = null
    for (id in ids) {
        try {
            apply(id)
            return

        } catch (e: UnknownPluginException) {
            if (exception != null) e.addSuppressed(exception)
            exception = e
        }
    }

    if (exception != null) {
        throw exception
    }
}

fun PluginManager.applyFirstAvailable(vararg ids: String) = applyFirstAvailable(ids.toList())


fun PluginManager.apply(pluginId: PluginId) = applyFirstAvailable(setOf(pluginId.id) + pluginId.alternateIds)


fun PluginManager.tryApply(id: String) {
    try {
        apply(id)
    } catch (e: UnknownPluginException) {
        // do nothing
    }
}

fun PluginManager.tryApply(pluginId: PluginId) {
    try {
        apply(pluginId)
    } catch (e: UnknownPluginException) {
        // do nothing
    }
}
