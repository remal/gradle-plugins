package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.dsl.utils.DependencyNotationMatcher
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.ExternalDependency

val ExternalDependency.notation get() = (this as Dependency).notation


fun DependencyNotationMatcher.matches(dependency: ExternalDependency) = matches(dependency.notation)
fun DependencyNotationMatcher.notMatches(dependency: ExternalDependency) = !matches(dependency)
