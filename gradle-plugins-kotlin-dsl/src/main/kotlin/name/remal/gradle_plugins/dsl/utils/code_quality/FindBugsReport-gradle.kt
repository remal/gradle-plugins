package name.remal.gradle_plugins.dsl.utils.code_quality

import name.remal.gradle_plugins.dsl.extensions.unwrapGradleGenerated
import org.gradle.api.Task
import org.gradle.api.plugins.quality.CodeQualityExtension

fun FindBugsReport.tool(task: Task) = tool(task.findBugsReportToolName)
fun FindBugsReport.version(codeQualityExtension: CodeQualityExtension) = tool(codeQualityExtension.toolVersion)


internal val Task.findBugsReportToolName get() = javaClass.unwrapGradleGenerated().simpleName.substringBefore("Task")
