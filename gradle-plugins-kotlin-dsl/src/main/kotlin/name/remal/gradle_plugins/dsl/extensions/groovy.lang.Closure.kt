package name.remal.gradle_plugins.dsl.extensions

import groovy.lang.Closure
import org.gradle.api.Action
import org.gradle.api.specs.Spec
import org.gradle.api.specs.Specs.convertClosureToSpec
import java.lang.reflect.InvocationTargetException

fun Closure<*>.toRunnable(): Runnable = Runnable { this.call() }

fun <T> Closure<*>.toSpec(): Spec<T> = convertClosureToSpec(this)


fun <T> Closure<*>.toConfigureAction(): Action<T> = Action { configure(this, it) }
fun <T> Closure<*>.toConfigureKotlinFunction() = { arg: T -> configure(this, arg); Unit }

private val configureUtilClass: Class<*> = sequenceOf(
    "org.gradle.util.internal.ConfigureUtil",
    "org.gradle.util.ConfigureUtil",
)
    .map { className ->
        try {
            Class.forName(className)
        } catch (expected: ClassNotFoundException) {
            null
        }
    }
    .filterNotNull()
    .first()

private val configureMethod = configureUtilClass.getMethod("configure", Closure::class.java, Any::class.java)

private fun configure(closure: Closure<*>?, obj: Any?) {
    try {
        configureMethod.invoke(null, closure, obj)
    } catch (e: InvocationTargetException) {
        throw e.targetException
    } catch (e: Throwable) {
        throw e
    }
}
