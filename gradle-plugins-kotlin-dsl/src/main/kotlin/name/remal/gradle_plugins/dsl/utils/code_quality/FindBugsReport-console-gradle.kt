package name.remal.gradle_plugins.dsl.utils.code_quality

import org.gradle.api.Task

fun FindBugsReport.createConsoleMessages(task: Task) = createConsoleMessages(task.project.projectDir)
