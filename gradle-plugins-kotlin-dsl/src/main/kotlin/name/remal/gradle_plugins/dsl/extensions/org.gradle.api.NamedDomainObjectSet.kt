package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.NamedDomainObjectSet

operator fun <T, S : T> NamedDomainObjectSet<T>.get(type: Class<S>): NamedDomainObjectSet<S> = withType(type)
