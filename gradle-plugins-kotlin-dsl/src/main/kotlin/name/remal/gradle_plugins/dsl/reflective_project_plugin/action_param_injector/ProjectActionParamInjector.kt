package name.remal.gradle_plugins.dsl.reflective_project_plugin.action_param_injector

import name.remal.gradle_plugins.api.AutoService
import org.gradle.api.Project

@AutoService
class ProjectActionParamInjector : ActionParamInjector<Project>() {
    override fun createValue(project: Project): Project = project
}
