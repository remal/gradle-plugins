package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.DomainObjectSet

operator fun <T, S : T> DomainObjectSet<T>.get(type: Class<S>): DomainObjectSet<S> = withType(type)
