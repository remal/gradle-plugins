package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.Project
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.api.tasks.SourceSetContainer


private val javaPackageNameProhibitedChars = Regex("[^.\\w]")
private val javaPackageNameProhibitedCharsAfterDot = Regex("\\.([^A-Za-z_])")
val Project.javaPackageName: String
    get() {
        var result = this.id
        result = result.replace(javaPackageNameProhibitedChars, "_")
        result = result.replace(javaPackageNameProhibitedCharsAfterDot, "._$1")
        return result
    }

val Project.javaModuleName: String get() = javaPackageName


val Project.java: JavaPluginExtension get() = this[JavaPluginExtension::class.java]
val Project.sourceSets: SourceSetContainer get() = this[SourceSetContainer::class.java]
