package name.remal.gradle_plugins.dsl.reflective_project_plugin.action_param_injector

import name.remal.gradle_plugins.api.AutoService
import org.gradle.api.Project
import org.gradle.api.component.SoftwareComponentContainer

@AutoService
class SoftwareComponentContainerActionParamInjector : ActionParamInjector<SoftwareComponentContainer>() {
    override fun createValue(project: Project): SoftwareComponentContainer = project.components
}
