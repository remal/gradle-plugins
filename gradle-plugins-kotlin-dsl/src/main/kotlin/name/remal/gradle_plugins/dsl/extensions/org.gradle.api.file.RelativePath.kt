package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.file.RelativePath

fun RelativePath.startsWith(other: RelativePath): Boolean {
    val segments = this.segments
    val otherSegments = other.segments
    if (segments.size < otherSegments.size) return false

    if (isFile) {
        if (other.isFile) {
            return this == other
        } else if (segments.size == otherSegments.size) {
            return false
        }
    } else {
        if (other.isFile) {
            return false
        } else if (segments.size == otherSegments.size) {
            return this == other
        }
    }

    var i = 0
    while (i < otherSegments.size) {
        if (segments[i] != otherSegments[i]) return false
        ++i
    }
    return true
}

fun RelativePath.endsWith(other: RelativePath): Boolean {
    if (isFile != other.isFile) {
        return false
    }

    val segments = this.segments
    val otherSegments = other.segments
    if (segments.size < otherSegments.size) return false
    val delta = segments.size - otherSegments.size
    var i = otherSegments.size - 1
    while (i >= 0) {
        if (segments[i + delta] != otherSegments[i]) return false
        --i
    }
    return true
}
