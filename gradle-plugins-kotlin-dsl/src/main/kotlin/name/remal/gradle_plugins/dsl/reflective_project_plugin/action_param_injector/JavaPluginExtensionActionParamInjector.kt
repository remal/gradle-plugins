package name.remal.gradle_plugins.dsl.reflective_project_plugin.action_param_injector

import name.remal.gradle_plugins.api.AutoService
import name.remal.gradle_plugins.dsl.extensions.java
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPluginExtension

@AutoService
class JavaPluginExtensionActionParamInjector : ActionParamInjector<JavaPluginExtension>() {
    override fun createValue(project: Project): JavaPluginExtension = project.java
}
