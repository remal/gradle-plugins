package name.remal.gradle_plugins.dsl.extensions

import name.remal.default
import name.remal.gradle_plugins.dsl.utils.DependencyNotation
import name.remal.gradle_plugins.dsl.utils.DependencyNotationMatcher
import name.remal.nullIf
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.DependencyArtifact
import org.gradle.api.artifacts.ModuleDependency

val Dependency.classifier: String?
    get() {
        if (this !is ModuleDependency) return null
        return artifacts.singleOrNull()?.classifier
    }

val Dependency.extension: String?
    get() {
        if (this !is ModuleDependency) return null
        return artifacts.singleOrNull()?.extension
    }

val Dependency.notation
    get() = DependencyNotation(
        group = group.default(),
        module = name,
        version = version.default(),
        classifier = classifier,
        extension = extension.nullIf { this == DependencyArtifact.DEFAULT_TYPE }
    )


fun DependencyNotationMatcher.matches(dependency: Dependency) = matches(dependency.notation)
fun DependencyNotationMatcher.notMatches(dependency: Dependency) = !matches(dependency)
