package name.remal.gradle_plugins.dsl.utils

import name.remal.accept
import org.objectweb.asm.ClassReader
import org.objectweb.asm.Type
import org.objectweb.asm.tree.AnnotationNode
import org.objectweb.asm.tree.ClassNode

fun forEachAnnotationNode(classNode: ClassNode, filter: (AnnotationNode) -> Boolean, action: (AnnotationNode) -> Unit) {
    fun AnnotationNode?.acceptAnnotationNode() {
        if (this != null) {
            if (filter(this)) {
                action(this)
            }
        }
    }

    classNode.visibleAnnotations?.forEach(AnnotationNode::acceptAnnotationNode)
    classNode.invisibleAnnotations?.forEach(AnnotationNode::acceptAnnotationNode)
    classNode.visibleTypeAnnotations?.forEach(AnnotationNode::acceptAnnotationNode)
    classNode.invisibleTypeAnnotations?.forEach(AnnotationNode::acceptAnnotationNode)

    classNode.fields?.forEach { field ->
        field.visibleAnnotations?.forEach(AnnotationNode::acceptAnnotationNode)
        field.invisibleAnnotations?.forEach(AnnotationNode::acceptAnnotationNode)
        field.visibleTypeAnnotations?.forEach(AnnotationNode::acceptAnnotationNode)
        field.invisibleTypeAnnotations?.forEach(AnnotationNode::acceptAnnotationNode)
    }

    classNode.methods?.forEach { method ->
        method.visibleAnnotations?.forEach(AnnotationNode::acceptAnnotationNode)
        method.invisibleAnnotations?.forEach(AnnotationNode::acceptAnnotationNode)
        method.visibleTypeAnnotations?.forEach(AnnotationNode::acceptAnnotationNode)
        method.invisibleTypeAnnotations?.forEach(AnnotationNode::acceptAnnotationNode)
        method.visibleParameterAnnotations?.forEach { it?.forEach(AnnotationNode::acceptAnnotationNode) }
        method.invisibleParameterAnnotations?.forEach { it?.forEach(AnnotationNode::acceptAnnotationNode) }
        method.tryCatchBlocks?.forEach {
            it.visibleTypeAnnotations?.forEach(AnnotationNode::acceptAnnotationNode)
            it.invisibleTypeAnnotations?.forEach(AnnotationNode::acceptAnnotationNode)
        }
        method.visibleLocalVariableAnnotations?.forEach(AnnotationNode::acceptAnnotationNode)
        method.invisibleLocalVariableAnnotations?.forEach(AnnotationNode::acceptAnnotationNode)
    }
}

fun forEachAnnotationNode(classNode: ClassNode, annotationClass: Class<out Annotation>, action: (AnnotationNode) -> Unit) {
    val filterDesc = Type.getDescriptor(annotationClass)
    return forEachAnnotationNode(classNode, { it.desc == filterDesc }, action)
}

fun forEachAnnotationNode(classNode: ClassNode, action: (AnnotationNode) -> Unit) {
    return forEachAnnotationNode(classNode, { true }, action)
}


fun forEachAnnotationNode(bytecode: ByteArray, filter: (AnnotationNode) -> Boolean, action: (AnnotationNode) -> Unit) {
    return forEachAnnotationNode(ClassNode().also { ClassReader(bytecode).accept(it) }, filter, action)
}

fun forEachAnnotationNode(bytecode: ByteArray, annotationClass: Class<out Annotation>, action: (AnnotationNode) -> Unit) {
    val filterDesc = Type.getDescriptor(annotationClass)
    return forEachAnnotationNode(bytecode, { it.desc == filterDesc }, action)
}

fun forEachAnnotationNode(bytecode: ByteArray, action: (AnnotationNode) -> Unit) {
    return forEachAnnotationNode(bytecode, { true }, action)
}
