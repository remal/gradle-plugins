package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.Action

operator fun <T> Action<T>.invoke(obj: T) = execute(obj)
