package name.remal.gradle_plugins.dsl.extensions

import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import com.google.common.cache.LoadingCache
import com.google.common.reflect.TypeToken
import name.remal.accept
import name.remal.asClass
import name.remal.findConstructor
import name.remal.fromUpperCamelToLowerCamel
import name.remal.gradle_plugins.api.BuildTimeConstants.getClassDescriptor
import name.remal.toInsnNode
import name.remal.tryLoadClass
import name.remal.uncheckedCast
import org.gradle.api.Task
import org.gradle.api.reporting.Report
import org.gradle.api.reporting.ReportContainer
import org.gradle.api.reporting.SingleFileReport
import org.gradle.api.reporting.internal.TaskGeneratedSingleFileReport
import org.gradle.api.reporting.internal.TaskReportContainer
import org.objectweb.asm.ClassReader
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.ClassWriter.COMPUTE_FRAMES
import org.objectweb.asm.ClassWriter.COMPUTE_MAXS
import org.objectweb.asm.Opcodes.*
import org.objectweb.asm.Type.VOID_TYPE
import org.objectweb.asm.Type.getConstructorDescriptor
import org.objectweb.asm.Type.getDescriptor
import org.objectweb.asm.Type.getInternalName
import org.objectweb.asm.Type.getMethodDescriptor
import org.objectweb.asm.Type.getType
import org.objectweb.asm.tree.AnnotationNode
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldInsnNode
import org.objectweb.asm.tree.InsnList
import org.objectweb.asm.tree.InsnNode
import org.objectweb.asm.tree.LabelNode
import org.objectweb.asm.tree.MethodInsnNode
import org.objectweb.asm.tree.MethodNode
import org.objectweb.asm.tree.TypeInsnNode
import org.objectweb.asm.tree.VarInsnNode
import org.objectweb.asm.util.CheckClassAdapter
import java.lang.reflect.Method
import java.lang.reflect.ParameterizedType
import javax.inject.Inject

private val taskReportContainerClassCache: LoadingCache<Class<ReportContainer<*>>, Class<ReportContainer<*>>> = CacheBuilder.newBuilder()
    .weakValues()
    .build(object : CacheLoader<Class<ReportContainer<*>>, Class<ReportContainer<*>>>() {
        @Suppress("LongMethod", "kotlin:S3776")
        override fun load(containerClass: Class<ReportContainer<*>>): Class<ReportContainer<*>> {
            if (!containerClass.isInterface) throw IllegalArgumentException("$containerClass is not an interface")

            val reportSuperType: Class<out Report> = run {
                val type = TypeToken.of(containerClass).getSupertype(ReportContainer::class.java).type
                if (type !is ParameterizedType) throw IllegalStateException("$type is not instance of ParameterizedType")
                type.actualTypeArguments[0].asClass().uncheckedCast()
            }

            data class ReportInfo(
                val name: String,
                val type: Class<*>,
                val implType: Class<*>?,
                val method: Method
            )

            val reportInfos = containerClass.methods.asSequence()
                .filter { !it.declaringClass.isAssignableFrom(ReportContainer::class.java) }
                .filter { it.parameterCount == 0 }
                .filter { Report::class.java.isAssignableFrom(it.returnType) }
                .filter { it.name.run { length >= 4 && startsWith("get") && get(3) == get(3).toUpperCase() } }
                .map {
                    ReportInfo(
                        name = it.name.substring(3).fromUpperCamelToLowerCamel(),
                        type = it.returnType,
                        implType = when (it.returnType) {
                            SingleFileReport::class.java -> TaskGeneratedSingleFileReport::class.java
                            else -> null
                        },
                        method = it
                    )
                }
                .distinctBy { it.name }
                .toList()

            val classNode = ClassNode().apply classNode@{
                version = V1_8
                access = ACC_PUBLIC
                name = getInternalName(containerClass) + "\$\$TaskReportContainer"
                superName = getInternalName(TaskReportContainer::class.java)
                interfaces = mutableListOf(getInternalName(containerClass))

                methods = mutableListOf()
                methods.add(MethodNode(ACC_PUBLIC, "<init>", getMethodDescriptor(VOID_TYPE, getType(Task::class.java)), null, null).apply methodNode@{
                    visibleAnnotations = mutableListOf(AnnotationNode(getClassDescriptor(Inject::class.java)))
                    instructions = InsnList().apply instructions@{
                        add(LabelNode())

                        add(VarInsnNode(ALOAD, 0))
                        add(reportSuperType.toInsnNode())
                        add(VarInsnNode(ALOAD, 1))
                        val decoratorClass = TaskReportContainer::class.java.classLoader.tryLoadClass("org.gradle.api.internal.CollectionCallbackActionDecorator")
                        val decoratorCtor = decoratorClass?.let { TaskReportContainer::class.java.findConstructor(Class::class.java, Task::class.java, it) }
                        if (decoratorCtor != null) {
                            add(FieldInsnNode(GETSTATIC, getInternalName(decoratorClass), "NOOP", getDescriptor(decoratorClass)))
                            add(MethodInsnNode(INVOKESPECIAL, this@classNode.superName, this@methodNode.name, getConstructorDescriptor(decoratorCtor), false))
                        } else {
                            add(
                                MethodInsnNode(
                                    INVOKESPECIAL,
                                    this@classNode.superName,
                                    this@methodNode.name,
                                    getMethodDescriptor(VOID_TYPE, getType(Class::class.java), getType(Task::class.java)),
                                    false
                                )
                            )
                        }

                        reportInfos.forEach { info ->
                            if (info.implType == null) {
                                if (info.method.isDefault) {
                                    return@forEach
                                } else {
                                    throw IllegalStateException("Report implementation type can't be defined for ${info.method}")
                                }
                            }
                            add(VarInsnNode(ALOAD, 0))
                            add(info.implType.toInsnNode())
                            add(InsnNode(ICONST_2))
                            add(TypeInsnNode(ANEWARRAY, getInternalName(Any::class.java)))
                            add(InsnNode(DUP))
                            add(InsnNode(ICONST_0))
                            add(info.name.toInsnNode())
                            add(InsnNode(AASTORE))
                            add(InsnNode(DUP))
                            add(InsnNode(ICONST_1))
                            add(VarInsnNode(ALOAD, 1))
                            add(InsnNode(AASTORE))
                            add(
                                MethodInsnNode(
                                    INVOKEVIRTUAL,
                                    this@classNode.name,
                                    "add",
                                    getMethodDescriptor(getType(Report::class.java), getType(Class::class.java), getType(Array<Any>::class.java)),
                                    false
                                )
                            )
                            add(InsnNode(POP))
                        }

                        add(InsnNode(RETURN))
                    }
                    maxStack = 1
                    maxLocals = 1
                })

                reportInfos.forEach { info ->
                    if (info.implType == null) return@forEach

                    methods.add(MethodNode(ACC_PUBLIC, info.method.name, getMethodDescriptor(info.method), null, null).apply methodNode@{
                        instructions = InsnList().apply instructions@{
                            add(LabelNode())
                            add(VarInsnNode(ALOAD, 0))
                            add(info.name.toInsnNode())
                            add(MethodInsnNode(INVOKEVIRTUAL, this@classNode.name, "getByName", getMethodDescriptor(getType(Any::class.java), getType(String::class.java)), false))
                            add(TypeInsnNode(CHECKCAST, getInternalName(info.type)))
                            add(InsnNode(ARETURN))
                        }
                        maxStack = 1
                        maxLocals = 1
                    })
                }
            }

            val classWriter = ClassWriter(COMPUTE_MAXS or COMPUTE_FRAMES)
            classNode.accept(classWriter)
            val bytecode = classWriter.toByteArray()
            ClassReader(bytecode).accept(CheckClassAdapter(ClassWriter(0)))

            return containerClass.classLoader.defineClass(classNode.name.replace('/', '.'), bytecode)
        }
    })

val <T : ReportContainer<*>> Class<T>.taskReportContainerClass: Class<out T> get() = taskReportContainerClassCache[this.uncheckedCast()].uncheckedCast()
