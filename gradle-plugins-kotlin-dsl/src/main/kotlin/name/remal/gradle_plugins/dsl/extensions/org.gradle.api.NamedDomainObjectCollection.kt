package name.remal.gradle_plugins.dsl.extensions

import org.gradle.api.DomainObjectCollection
import org.gradle.api.NamedDomainObjectCollection

operator fun <T, S : T> NamedDomainObjectCollection<T>.get(type: Class<S>): NamedDomainObjectCollection<S> = withType(type)

operator fun NamedDomainObjectCollection<*>.contains(name: String): Boolean = findByName(name) != null
operator fun <T> NamedDomainObjectCollection<T>.get(name: String): T = getByName(name)
operator fun <T, S : T> NamedDomainObjectCollection<T>.get(type: Class<S>, name: String): S = withType(type).getByName(name)


fun <T> NamedDomainObjectCollection<T>.all(name: String, configureAction: (T) -> Unit) = all {
    if (name == namer.determineName(it)) {
        configureAction(it)
    }
}

fun <T, N> NamedDomainObjectCollection<T>.all(nameProviders: DomainObjectCollection<N>, nameGetter: (nameProvider: N) -> String, configureAction: (T) -> Unit) = all {
    nameProviders.all { nameProvider ->
        val name = nameGetter(nameProvider)
        if (name == namer.determineName(it)) {
            configureAction(it)
        }
    }
}

fun <T, S : T> NamedDomainObjectCollection<T>.all(type: Class<S>, name: String, configureAction: (S) -> Unit) = withType(type).all(name, configureAction)

fun <T, S : T, N> NamedDomainObjectCollection<T>.all(
    type: Class<S>,
    nameProviders: DomainObjectCollection<N>,
    nameGetter: (nameProvider: N) -> String,
    configureAction: (S) -> Unit
) = withType(type).all(nameProviders, nameGetter, configureAction)

fun <T> NamedDomainObjectCollection<T>.forEach(name: String, configureAction: (T) -> Unit) = forEach {
    if (name == namer.determineName(it)) {
        configureAction(it)
    }
}

fun <T, N> NamedDomainObjectCollection<T>.forEach(nameProviders: DomainObjectCollection<N>, nameGetter: (nameProvider: N) -> String, configureAction: (T) -> Unit) = forEach {
    nameProviders.forEach { nameProvider ->
        val name = nameGetter(nameProvider)
        if (name == namer.determineName(it)) {
            configureAction(it)
        }
    }
}

fun <T, S : T> NamedDomainObjectCollection<T>.forEach(type: Class<S>, name: String, configureAction: (S) -> Unit) = withType(type).forEach(name, configureAction)

@Suppress("MaxLineLength")
fun <T, S : T, N> NamedDomainObjectCollection<T>.forEach(type: Class<S>, nameProviders: DomainObjectCollection<N>, nameGetter: (nameProvider: N) -> String, configureAction: (S) -> Unit) = withType(
    type
).forEach(nameProviders, nameGetter, configureAction)
