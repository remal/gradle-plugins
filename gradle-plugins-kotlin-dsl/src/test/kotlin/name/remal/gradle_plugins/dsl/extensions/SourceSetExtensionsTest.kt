package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.io.File

class SourceSetExtensionsTest : BaseProjectTest() {

    @Test
    fun test() {
        val names = mutableSetOf<String>()
        val srcDirs = mutableSetOf<File>()

        project.applyPlugin("groovy")
        project.sourceSets.main.forEachSourceDirectorySet { name, directorySet ->
            names.add(name)
            srcDirs.addAll(directorySet.srcDirs)
        }

        assertThat(names)
            .containsAll(
                listOf(
                    "resources",
                    "java",
                    "groovy"
                )
            )

        assertThat(srcDirs)
            .containsAll(
                listOf(
                    "src/main/resources",
                    "src/main/java",
                    "src/main/groovy"
                ).map { project.file(it) }
            )
    }

}
