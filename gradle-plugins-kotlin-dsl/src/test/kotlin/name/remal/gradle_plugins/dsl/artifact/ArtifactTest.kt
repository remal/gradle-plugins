package name.remal.gradle_plugins.dsl.artifact

import name.remal.gradle_plugins.dsl.extensions.createWithUniqueName
import name.remal.gradle_plugins.dsl.extensions.makeNotTransitive
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import name.remal.newTempDir
import org.assertj.core.api.AssertionsForInterfaceTypes.assertThat
import org.gradle.api.artifacts.Configuration
import org.junit.After
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters.NAME_ASCENDING
import java.io.File

@FixMethodOrder(NAME_ASCENDING)
class ArtifactTest : BaseProjectTest() {

    lateinit var tempDir: File

    lateinit var configuration: Configuration

    lateinit var artifacts: CachedArtifactsCollection

    @Before
    fun tearUp() {
        ArtifactsCache.invalidateAll()

        tempDir = newTempDir(ArtifactTest::class.java.simpleName + '-')

        project.repositories.add(project.repositories.mavenCentral())
        configuration = project.configurations.createWithUniqueName { it.makeNotTransitive() }

        configuration.dependencies.add(project.dependencies.create("org.springframework:spring-context:5+"))

        val tempFiles = configuration.files.map { file ->
            File(tempDir, file.name).also { if (!it.exists()) file.copyTo(it, false) }
        }
        artifacts = CachedArtifactsCollection(tempFiles)
    }

    @After
    fun tearDown() {
        tempDir.deleteRecursively()
    }


    @Test
    fun forEachEntry() {
        val names = sortedSetOf<String>()
        artifacts.forEachEntry("org/springframework/stereotype/**/*.class") { names.add(it.name) }
        assertThat(names).contains("org/springframework/stereotype/Component.class")
    }

}
