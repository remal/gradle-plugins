package name.remal.gradle_plugins.dsl.extensions

import groovy.lang.Closure
import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.junit.Assert.assertEquals
import org.junit.Test

class ClosureExtensionsTest : BaseProjectTest() {

    @Test
    fun toConfigureAction() {
        val closure = object : Closure<Any?>(project.tasks) {
            override fun call(vararg args: Any?): Any? {
                this.delegate.extensions.extraProperties["applied"] = true
                return null
            }
        }

        closure.toConfigureAction<Any?>().execute(project)

        assertEquals(true, project.extensions.extraProperties["applied"])
    }

}
