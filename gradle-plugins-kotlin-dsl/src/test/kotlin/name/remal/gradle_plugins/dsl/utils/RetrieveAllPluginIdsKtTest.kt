package name.remal.gradle_plugins.dsl.utils

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class RetrieveAllPluginIdsKtTest {

    @Test
    fun test() {
        assertThat(retrieveAllPluginIds(RetrieveAllPluginIdsKtTest::class.java.classLoader))
            .doesNotContain("java")
            .contains("org.gradle.java")
    }

}
