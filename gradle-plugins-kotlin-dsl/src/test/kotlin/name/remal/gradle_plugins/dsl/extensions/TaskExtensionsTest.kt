package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.gradle.api.tasks.Copy
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test

class TaskExtensionsTest : BaseProjectTest() {

    @Test
    fun hasCustomActions() {
        val withoutCustomActions = project.tasks.create("withoutCustomActions", Copy::class.java)
        val withCustomActions = project.tasks.create("withCustomActions", Copy::class.java) {
            it.doFirst {}
        }

        assertFalse(withoutCustomActions.isHasCustomActions)
        assertTrue(withCustomActions.isHasCustomActions)
    }

}
