package name.remal.gradle_plugins.dsl.utils

import name.remal.newTempDir
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test
import java.nio.charset.StandardCharsets.UTF_8

class RetrieveClassNameFromJavaSourceTest {

    private val javaSources = """
        package pkg;

        interface AdditionalClass {}

        class MainClass {
            Map field = new HashMap() {
                {
                    put("key", "value");
                }
            };
            enum InnerEnum {
                ENUM
            }
        }
    """.trim('\n', '\r').trimIndent()

    @Test
    fun test() {
        val tempDir = newTempDir(javaClass.simpleName + '-', doDeleteOnExit = false)
        val sourceFile = tempDir.resolve("MainClass.java")
        sourceFile.writeText(javaSources, UTF_8)

        assertEquals("primary class name", "pkg.MainClass", retrieveClassNameFromJavaSource(sourceFile))

        assertNull("incorrect line - before", retrieveClassNameFromJavaSource(sourceFile, 2))

        assertEquals("additional class name", "pkg.AdditionalClass", retrieveClassNameFromJavaSource(sourceFile, 3))
        assertNull("additional class name - incorrect column", retrieveClassNameFromJavaSource(sourceFile, 3, 100))

        assertNull("incorrect line - after", retrieveClassNameFromJavaSource(sourceFile, 4))

        assertEquals("main class name (without column)", "pkg.MainClass", retrieveClassNameFromJavaSource(sourceFile, 5))
        assertEquals("main class name (with column)", "pkg.MainClass", retrieveClassNameFromJavaSource(sourceFile, 5, 1))

        assertEquals("local class name", "pkg.MainClass", retrieveClassNameFromJavaSource(sourceFile, 7))

        assertEquals("inner class name", "pkg.MainClass\$InnerEnum", retrieveClassNameFromJavaSource(sourceFile, 11))
        assertEquals("inner class name - incorrect column", "pkg.MainClass", retrieveClassNameFromJavaSource(sourceFile, 11, 1))
        assertEquals("inner class name - correct column", "pkg.MainClass\$InnerEnum", retrieveClassNameFromJavaSource(sourceFile, 11, 100))

        tempDir.deleteRecursively()
    }

}
