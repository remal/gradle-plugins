package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class SourceSetOutputExtensionsTest : BaseProjectTest() {

    @Test
    fun addClassesDirs() {
        project.applyPlugin("java")
        val sourceSetOutput = project.sourceSets.main.output
        sourceSetOutput.addClassesDirs(project.provider {
            project.file("build/classes/additional")
        })
        assertThat(sourceSetOutput.classesDirs.files)
            .contains(project.file("build/classes/additional"))
    }

}
