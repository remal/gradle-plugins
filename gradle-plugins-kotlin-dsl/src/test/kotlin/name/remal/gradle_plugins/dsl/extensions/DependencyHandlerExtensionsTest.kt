package name.remal.gradle_plugins.dsl.extensions

import name.remal.gradle_plugins.testing.dsl.BaseProjectTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test

class DependencyHandlerExtensionsTest : BaseProjectTest() {

    @Before
    fun applyJavaPlugin() {
        project.applyPlugin("java")
    }

    @Test
    fun testCreateString() {
        val dep = project.dependencies.create("name.remal:common:+") { it.isTransitive = false }
        assertThat(dep.group).`as`("dep.group").isEqualTo("name.remal")
        assertThat(dep.name).`as`("dep.name").isEqualTo("common")
        assertThat(dep.version).`as`("dep.version").isEqualTo("+")
        assertThat(dep.isTransitive).`as`("dep.isTransitive").isFalse()
    }

    @Test
    fun testAddString() {
        val conf = project.configurations.implementation

        val dep = project.dependencies.add(conf.name, "name.remal:common:+") { it.isTransitive = false }
        assertThat(dep.group).`as`("dep.group").isEqualTo("name.remal")
        assertThat(dep.name).`as`("dep.name").isEqualTo("common")
        assertThat(dep.version).`as`("dep.version").isEqualTo("+")
        assertThat(dep.isTransitive).`as`("dep.isTransitive").isFalse()

        assertThat(conf.dependencies).contains(dep)
    }

}
