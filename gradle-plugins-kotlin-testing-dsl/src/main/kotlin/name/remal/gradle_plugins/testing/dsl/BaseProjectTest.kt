package name.remal.gradle_plugins.testing.dsl

import name.remal.asSynchronized
import name.remal.default
import name.remal.findMethod
import name.remal.gradle_plugins.dsl.BaseReflectiveProjectPlugin.DisableOptionalPluginsMarker
import name.remal.gradle_plugins.dsl.GradleEnumVersion.GRADLE_VERSION_6_8
import name.remal.gradle_plugins.dsl.extensions.compareTo
import name.remal.gradle_plugins.dsl.extensions.createWithAutoName
import name.remal.gradle_plugins.dsl.extensions.invokeForInstance
import name.remal.mockito.StrictMockAnswer
import name.remal.newTempDir
import name.remal.uncheckedCast
import org.gradle.api.Project
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.internal.GradleInternal
import org.gradle.api.internal.SettingsInternal
import org.gradle.api.internal.project.ProjectStateInternal
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.util.GradleVersion
import org.junit.Rule
import org.junit.rules.TestWatcher
import org.junit.runner.Description
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import java.lang.System.identityHashCode

abstract class BaseProjectTest {

    protected val project: Project by lazy { newTempProject() }

    private val createdProjects = mutableListOf<Project>().asSynchronized()

    protected fun newTempProject(name: String): Project {
        val prefix = "gradle-test-project-"
        val projectDir = newTempDir("$prefix$name-", doDeleteOnExit = false)
        val project = ProjectBuilder.builder()
            .withProjectDir(projectDir)
            .withName(projectDir.name)
            .build()
            .also { createdProjects.add(it) }

        if (GradleVersion.current() >= GRADLE_VERSION_6_8) {
            injectGradleSettings(project)
        }

        configureProject(project)

        return project
    }

    protected fun newTempProject(): Project {
        val stackTraceElement = Throwable().stackTrace.firstOrNull {
            val className = it.className
            !className.isNullOrEmpty()
                && className != BaseProjectTest::class.java.name
                && !className.startsWith(BaseProjectTest::class.java.name + "$")
                && !className.startsWith("java.")
                && !className.startsWith("jdk.")
                && !className.startsWith("javax.")
                && !className.startsWith("kotlin.")
                && !className.startsWith("groovy.")
                && !className.startsWith("scala.")
        }
        val name = stackTraceElement
            ?.run { "${className.substringAfterLast('.')}.$methodName" }
            ?.replace(Regex("\\W"), "-")
            .default()
        return newTempProject(name)
    }

    @get:Rule
    val cleanerTestWatcher = object : TestWatcher() {
        override fun succeeded(description: Description?) {
            createdProjects.forEach {
                it.rootDir.deleteRecursively()
            }
        }
    }


    protected fun Project.newChildProject(name: String, configurer: Project.() -> Unit = {}): Project {
        val childProject = ProjectBuilder.builder()
            .withName(name)
            .withParent(this)
            .build()

        configureProject(childProject)

        configurer(childProject)

        return childProject
    }


    private fun configureProject(project: Project) {
        project.extensions.createWithAutoName(DisableOptionalPluginsMarker::class.java)

        val stateInternal = project.state.uncheckedCast<ProjectStateInternal>()
        stateInternal.toBeforeEvaluate()
    }


    private fun injectGradleSettings(project: Project) {
        val gradle = project.gradle as GradleInternal

        try {
            @Suppress("SENSELESS_COMPARISON")
            if (gradle.settings != null) {
                return
            }
        } catch (ignored: IllegalStateException) {
            // settings not set
        }

        val settings = strictMock(SettingsInternal::class.java) {
            tryLoadClass("org.gradle.internal.management.DependencyResolutionManagementInternal")?.let { dependencyResolutionManagementClass ->
                val dependencyResolutionManagement = strictMock(dependencyResolutionManagementClass) {
                    val repositoryHandler = strictMock(RepositoryHandler::class.java) {
                        `when`(isEmpty()).thenReturn(true)
                    }

                    javaClass.findMethod("getRepositories")?.let { getRepositories ->
                        `when`(getRepositories.invokeForInstance<Any>(this)).thenReturn(repositoryHandler)
                    }
                }

                javaClass.findMethod("getDependencyResolutionManagement")?.let { getDependencyResolutionManagement ->
                    `when`(getDependencyResolutionManagement.invokeForInstance<Any>(this)).thenReturn(dependencyResolutionManagement)
                }
            }
        }

        gradle.settings = settings
    }

    private fun tryLoadClass(className: String): Class<*>? {
        try {
            return Class.forName(className, false, BaseProjectTest::class.java.classLoader)
        } catch (ignored: Throwable) {
            return null
        }
    }

    private fun <T : Any> strictMock(mockClass: Class<T>, configurer: T.() -> Unit = {}): T {
        val defaultAnswer = StrictMockAnswer()
        val mock = mock(mockClass, defaultAnswer)
        `when`(mock.toString()).thenReturn(mockClass.simpleName + "@" + identityHashCode(mock))
        configurer(mock)
        defaultAnswer.isEnabled = true
        return mock
    }

}
