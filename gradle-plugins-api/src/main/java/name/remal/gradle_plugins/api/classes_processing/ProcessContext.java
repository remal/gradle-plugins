package name.remal.gradle_plugins.api.classes_processing;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.annotation.Nullable;

public interface ProcessContext extends Closeable {

    File getClassesDir();

    List<File> getClasspath();

    ClassLoader getClasspathClassLoader();

    default boolean doesResourceExist(String relativePath) {
        return readBinaryResource(relativePath) != null;
    }

    @Nullable
    byte[] readBinaryResource(String relativePath);

    @Nullable
    String readTextResource(String relativePath);

    void writeBinaryResource(String relativePath, byte[] content);

    void writeTextResource(String relativePath, String text);

    void appendTextResource(String relativePath, String text);

    default void writeService(String serviceName, String implementationName) {
        appendTextResource("META-INF/services/" + serviceName, "\n" + implementationName);
    }

    default void writeService(Class<?> serviceClass, String implementationName) {
        writeService(serviceClass.getName(), implementationName);
    }

    default void writeService(String serviceName, Class<?> implementationClass) {
        writeService(serviceName, implementationClass.getName());
    }

    default void writeService(Class<?> serviceClass, Class<?> implementationClass) {
        writeService(serviceClass.getName(), implementationClass.getName());
    }

    default boolean doesClasspathResourceExist(String relativePath) {
        return readClasspathBinaryResource(relativePath) != null;
    }

    @Nullable
    byte[] readClasspathBinaryResource(String relativePath);

    @Nullable
    String readClasspathTextResource(String relativePath);


    @Override
    default void close() throws IOException {
        ClassLoader classpathClassLoader = getClasspathClassLoader();
        if (classpathClassLoader instanceof Closeable) {
            ((Closeable) classpathClassLoader).close();
        }
    }

}
