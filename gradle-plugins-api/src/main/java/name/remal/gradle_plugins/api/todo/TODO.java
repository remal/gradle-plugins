package name.remal.gradle_plugins.api.todo;

import static java.lang.annotation.RetentionPolicy.CLASS;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;

@Documented
@Retention(CLASS)
@Repeatable(TODO.TODOs.class)
public @interface TODO {

    String value();


    @Documented
    @Retention(CLASS)
    @interface TODOs {
        TODO[] value() default {};
    }

}
