package name.remal.gradle_plugins.api.classes_processing;

@FunctionalInterface
public interface BytecodeModifier {

    void modify(byte[] modifiedBytecode);

}
